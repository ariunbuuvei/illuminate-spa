<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Status;

class Status extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'name',
    ];

    protected $dates = [
    	'deleted_at',
    ];

    public static function dataTable()
    {
    	// $settingsAttrValueObj = Status::whereNotNull('target')->leftJoin('notifications', function ($join) {
     //        $join->on('notifications.id', '=', 'notification_targets.notification_id');
     //    })->select('notifications.*', 'notification_targets.target_id', 'notification_targets.is_read')
     //    ->orderBy('notifications.id', 'DESC')
     //    ->limit($limit)
     //    ->get();

    	// if ($settingsAttrValueObj) {
    	// 	return $settingsAttrValueObj->content;
    	// }

    	// return null;
    }

}
