<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Settings extends Model
{
    use SoftDeletes;

    public function settingsAttrCollection()
    {
        return $this->hasMany('App\SettingsAttr', 'settings_id');
    }

    public function getTargetName()
	{
		// 'General' => 1,
        // 'Page' => 2,
        // 'Post' => 3,
        // 'User' => 4,
        // 'Settings' => 5,
		
		switch ($this->attributes['target']) {
			case 1:
				return 'General';
				break;

			case 2:
				return 'Page';
				break;

			case 3:
				return 'Post';
				break;

			case 4:
				return 'User';
				break;

			case 5:
				return 'Settings';
				break;
			
			default:
				return $this->attributes['target'];
				break;
		}
	}

}
