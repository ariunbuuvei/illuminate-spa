<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifyToken extends Model
{
    public function userObject()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
