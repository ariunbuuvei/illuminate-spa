<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class userMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailParams)
    {
        $this->template = $mailParams['template'];
        $this->mailParams = $mailParams;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        switch ($this->template) {
            case 'invoice':
                return $this->markdown('mail.userMail')->with([
                    'title' => $this->mailParams['title'],
                    'body' => $this->mailParams['body'],
                    'table' => $this->mailParams['table'],
                    'link' => $this->mailParams['link'],
                ]);

                break;
            
            default:
                return $this->markdown('mail.generalMail')->with([
                    'title' => $this->mailParams['title'],
                    'body' => $this->mailParams['body'],
                    'link' => $this->mailParams['link'],
                ]);
                break;
        }

    }
}
