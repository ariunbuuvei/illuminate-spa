<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

use App\Page;

class Page extends Model
{
    use SoftDeletes;

    protected $dates = [
    	'deleted_at',
    ];

    public function pageAttrCollection()
    {
        return $this->hasMany('App\ContentAttr', 'target_id')->where('target', 'page');
    }

    public function pageCommentCollection()
    {
        return $this->hasMany('App\ContentAttr', 'target_id')->where([
            'target' => Config::get('settings.target.Page'),
            'type_id' => Config::get('settings.type.content.Comment')
        ]);
    }

    public function authorObject()
    {
        return $this->belongsTo('App\User', 'author_id');
    }

    public function categoryObject()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function thumbObject()
    {
        return $this->hasOne('App\File', 'target_id')->where([
            'target' => Config::get('settings.target.Page'),
            'type_id' => Config::get('settings.type.file.Thumbnail')
        ]);
    }

    public function pageThumbCollection()
    {
        return $this->hasMany('App\File', 'target_id')->where([
            'target' => Config::get('settings.target.Page'),
            'type_id' => Config::get('settings.type.file.Thumbnail')
        ]);
    }

    public static function dataTable()
    {
        $settingsAttrValueObj = Page::leftJoin('statuses', function ($join) {
            $join->on('statuses.id', '=', 'pages.status_id');
        })->leftJoin('types', function ($join) {
            $join->on('types.id', '=', 'pages.type_id');
        })->leftJoin('categories', function ($join) {
            $join->on('categories.id', '=', 'pages.category_id');
        })->leftJoin('users', function ($join) {
            $join->on('users.id', '=', 'pages.author_id');
        })->select(
            'pages.id', 
            'pages.title', 
            'pages.created_at', 
            'statuses.name as statusName',
            'types.name as typeName',
            'categories.name as categoryName',
            'users.email as authorMail'
        )->get();

        if ($settingsAttrValueObj) {
            return $settingsAttrValueObj;
        }

        return null;
    }

    public function reviewCeilGroups()
    {
        $reviewCeilGroupCollection = ContentAttr::where([
            'target' => Config::get('settings.target.Page'),
            'target_id' => $this->attributes['id'],
            'type_id' => Config::get('settings.type.content.Comment')
        ])->whereNotNull('support_content')
        ->select(DB::raw('CEIL(support_content) as reviews'), DB::raw('COUNT(id) as review_count'))
        ->groupBy(DB::raw('CEIL(support_content)'))
        ->orderBy(DB::raw('CEIL(support_content)'), 'DESC')
        ->get();

        if ($reviewCeilGroupCollection) {
            return $reviewCeilGroupCollection;
        }

        return null;
    }

}
