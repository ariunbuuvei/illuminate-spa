<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

use App\Traits\GeneralTrait;
use App\Traits\UserTrait;

use App\Mail\UserMail;

use App\Page;
use App\ContentAttr;

use Nikolag\Square\Facades\Square;

use Validator;
use Auth;

class ContentController extends Controller
{
    use GeneralTrait, UserTrait;

    public function enquireEvent(Request $request)
    {
        // return back()->with('success', 'Appointment Date required');

        Mail::to('unlavab@gmail.com')->send(new UserMail([
            'template' => 'invoice',
            'title' => 'Thank you, NAME',
            'body' => 'We have confirmed your order M6T2KD22',
            'link' => null,
        ]));

        return back()->with('success', 'Appointment Date required');
        
        $validator = Validator::make($request->only('AppointmentFullName', 'AppointmentEmail', 'AppointmentContactNumber'), [
            'AppointmentFullName' => 'required',
            'AppointmentEmail' => 'required',
            'AppointmentContactNumber' => 'required',
        ]);

        if( !$request->input('AppointmentDate') or !$request->input('AppointmentMobileDate') ) {
            return back()->with('error', 'Appointment Date required');
        }
        

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        $newEnquireObj = new ContentAttr;
        $newEnquireObj->target_id = 1;
        $newEnquireObj->target = Config::get('settings.target.Page');
        $newEnquireObj->author = $request->input('AppointmentFullName');
        $newEnquireObj->user = $request->input('AppointmentEmail');
        $newEnquireObj->mobile = $request->input('AppointmentContactNumber');
        $newEnquireObj->content_date = $request->input('AppointmentMobileDate') ? $request->input('AppointmentMobileDate') : ( $request->input('AppointmentDate') ? $request->input('AppointmentDate') : null );
        $newEnquireObj->attr = 'enquire';
        $newEnquireObj->type_id = Config::get('settings.type.content.Comment');
        $newEnquireObj->content = $request->input('AppointmentMessage');
        
        if ($newEnquireObj->save()) {
            return back()->with('success', 'Thank you for booking appointment. Please check your email');
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function sendMessage(Request $request)
    {
        $validator = Validator::make($request->only('ContactFullName', 'ContactEmail', 'ContactNumber', 'ContactDescription'), [
            'ContactFullName' => 'required',
            'ContactEmail' => 'required',
            'ContactNumber' => 'required',
            'ContactDescription' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        $newEnquireObj = new ContentAttr;
        $newEnquireObj->target_id = 0;
        $newEnquireObj->target = Config::get('settings.target.General');
        $newEnquireObj->author = $request->input('ContactFullName');
        $newEnquireObj->user = $request->input('ContactEmail');
        $newEnquireObj->mobile = $request->input('ContactNumber');
        $newEnquireObj->attr = 'message';
        $newEnquireObj->type_id = Config::get('settings.type.content.Message');
        $newEnquireObj->content = $request->input('ContactDescription');
        
        if ($newEnquireObj->save()) {
            return back()->with('success', 'Message sent created');
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function purchaseGift(Request $request)
    {
        $amount = 1; //Is in USD currency and is in smallest denomination (cents). ($amount = 5000 == 50 Dollars)

        $formNonce = '9'; //nonce reference => https://docs.connect.squareup.com/articles/adding-payment-form

        $location_id = 'Z5707199T5WTX'; //$location_id is id of a location from Square

        $currency = 'USD'; //available currencies => https://docs.connect.squareup.com/api/connect/v2/?q=currency#type-currency

        $options = [
            'amount' => $amount,
            'card_nonce' => $formNonce,
            'location_id' => $location_id,
            'currency' => $currency
        ];

        dd(Square::charge($options));



        // return back()->with('success', 'Success');
    }
}
