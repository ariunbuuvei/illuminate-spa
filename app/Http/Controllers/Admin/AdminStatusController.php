<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use App\Traits\GeneralTrait;
use App\Traits\UserTrait;

use App\Status;

use Validator;
use Auth;

class AdminStatusController extends Controller
{
    use GeneralTrait, UserTrait;

	public function statusList()
    {
    	return view('admin/status/statusList');
    }

    public function statusTable(Request $request)
    {
    	$statusList = Status::all()
        ->toArray();

        $statusTable = [
            "pagination" => [
                'page' => $request->pagination['page'],
                'perpage' => $request->pagination['perpage'],
            ],
            "data" => $statusList
        ];

        return response()->json($statusTable);
    }

    public function statusCreate()
    {
    	$collection = collect(Config::get('settings.target'));
    	$targetList = $collection->flip();

    	return view('admin/status/statusCreate', [
            'targetList' => $targetList->all(),
        ]);
    }

    public function statusStore(Request $request)
    {
    	$validator = Validator::make($request->only('target', 'name'), [
            'target' => 'required',
          	'name' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        try {
            // prepaire and execute
            $newStatusObj = new Status;
            $newStatusObj->target = $request->input('target');
            $newStatusObj->name = $request->input('name');
            $newStatusObj->color = $request->input('color');
            $newStatusObj->css = $request->input('css');
            $newStatusObj->html_class = $request->input('html_class');
            $newStatusObj->description = $request->input('description');

            if($newStatusObj->save()) {

                $this->userActionLogger([
                    'user_id' => Auth::user()->id,
                    'status_id' => Config::get('settings.status.logger.Succesful'),
                    'type_id' => Config::get('settings.type.logger.Create'),
                    'controller' => 'AdminStatusController',
                    'class' => 'statusList',
                    'line' => 75,
                    'code' => 200,
                    'message' => 'User read statusList',
                    'target' => Config::get('settings.target.General'),
                    'target_id' => null,
                ], [
                    // 'email', 'notification'
                ]);

                return redirect()->route('adminStatusList')->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());

            $this->userActionLogger([
                'user_id' => Auth::user()->id,
                'status_id' => Config::get('settings.status.logger.Succesful'),
                'type_id' => Config::get('settings.type.logger.Create'),
                'controller' => 'AdminStatusController',
                'class' => 'statusList',
                'line' => 75,
                'code' => 200,
                'message' => $e->getMessage(),
                'target' => Config::get('settings.target.General'),
                'target_id' => null,
            ], [
                    // 'email', 'notification'
            ]);
            
        } catch (\Exception $e) {
            // deactivating newly inserted model
            if ($newStatusObj->exists) {
                // softdelete
                $newStatusObj->delete();
            }
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function statusEdit($status = null)
    {
    	$statusObject = Status::findOrFail($status);

    	$collection = collect(Config::get('settings.target'));
    	$targetList = $collection->flip();

    	return view('admin/status/statusEdit', [
            'targetList' => $targetList->all(),
            'statusObject' => $statusObject
        ]);
    }

    public function statusUpdate(Request $request, $status)
    {
    	$statusObject = Status::find($status);
    	if (!$statusObject) {
    		return back()->withErrors([
	            'error' => 'Object not found'
	        ]);
    	}

    	$validator = Validator::make($request->only('target', 'name'), [
            'target' => 'required',
          	'name' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        try {
            // prepaire and execute
            $statusObject->target = $request->input('target');
            $statusObject->name = $request->input('name');
            $statusObject->color = $request->input('color');
            $statusObject->css = $request->input('css');
            $statusObject->html_class = $request->input('html_class');
            $statusObject->description = $request->input('description');

            if($statusObject->save()) {
                return redirect()->route('adminStatusList')->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // deactivating newly inserted model
            if ($newStatusObj->exists) {
                // softdelete
                // $newStatusObj->delete();
            }
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function statusDelete(Request $request, $status)
    {
        $statusObject = Status::find($status);
        if (!$statusObject) {
            return back()->withErrors([
                'error' => 'Object not found'
            ]);
        }

        // delete
        try {
            // prepaire and execute
            if($statusObject->delete()) {
                return redirect()->route('adminStatusList')->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }
}
