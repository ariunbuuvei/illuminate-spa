<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

use App\Category;

use Validator;
use Auth;

class AdminCategoryController extends Controller
{
	public function categoryList()
    {
    	return view('admin/category/categoryList');
    }

    public function categoryTable(Request $request)
    {
        $categoryList = Category::all()
        ->toArray();

        $categoryTable = [
            "pagination" => [
                'page' => $request->pagination['page'],
                'perpage' => $request->pagination['perpage'],
            ],
            "data" => $categoryList
        ];

        return response()->json($categoryTable);
    }

    public function categoryCreate()
    {
    	$collection = collect(Config::get('settings.target'));
        $targetList = $collection->flip();

        return view('admin/category/categoryCreate', [
            'targetList' => $targetList->all(),
        ]);
    }

    public function categoryAutocomplete(Request $request)
    {
        $categoryList = Category::where('name', 'like', '%' . $request->input('autocomplete') . '%')
        ->select('name as text', 'id')
        ->get();

        $autoCompleteResponse = [
            'results' => $categoryList->toArray(),
            'pagination' => [
                'more' => false,
            ],
        ];

        return response()->json($autoCompleteResponse);
    }

    public function categoryStore(Request $request)
    {
        $validator = Validator::make($request->only('target', 'name'), [
            'target' => 'required',
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        try {
            // prepaire and execute
            $newCategoryObj = new Category;
            $newCategoryObj->target = $request->input('target');
            $newCategoryObj->name = $request->input('name');
            $newCategoryObj->parent_id = $request->input('parent');
            $newCategoryObj->color = $request->input('color');
            $newCategoryObj->css = $request->input('css');
            $newCategoryObj->html_class = $request->input('html_class');
            $newCategoryObj->description = $request->input('description');

            if($newCategoryObj->save()) {
                return redirect()->route('adminCategoryList')->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // deactivating newly inserted model
            if ($newCategoryObj->exists) {
                // softdelete
                $newCategoryObj->delete();
            }
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function categoryEdit($category = null)
    {
        $categoryObject = Category::findOrFail($category);

        $collection = collect(Config::get('settings.target'));
        $targetList = $collection->flip();

        return view('admin/category/categoryEdit', [
            'targetList' => $targetList->all(),
            'categoryObject' => $categoryObject
        ]);
    }

    public function categoryUpdate(Request $request, $category)
    {
        $categoryObject = Category::find($category);
        if (!$categoryObject) {
            return back()->withErrors([
                'error' => 'Object not found'
            ]);
        }

        $validator = Validator::make($request->only('target', 'name'), [
            'target' => 'required',
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        try {
            // prepaire and execute
            $categoryObject->target = $request->input('target');
            $categoryObject->name = $request->input('name');
            $categoryObject->parent_id = $request->input('parent');
            $categoryObject->color = $request->input('color');
            $categoryObject->css = $request->input('css');
            $categoryObject->html_class = $request->input('html_class');
            $categoryObject->description = $request->input('description');

            if($categoryObject->save()) {
                return redirect()->route('adminCategoryList')->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // deactivating newly inserted model
            if ($categoryObject->exists) {
                // softdelete
                // $categoryObject->delete();
            }
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function categoryDelete(Request $request, $category)
    {
        $categoryObject = Category::find($category);
        if (!$categoryObject) {
            return back()->withErrors([
                'error' => 'Object not found'
            ]);
        }

        // delete
        try {
            // prepaire and execute
            if($categoryObject->delete()) {
                return redirect()->route('adminCategoryList')->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }
}
