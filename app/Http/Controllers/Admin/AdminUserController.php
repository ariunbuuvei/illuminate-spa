<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

use App\Traits\GeneralTrait;

use App\User;
use App\UserAttr;
use App\Settings;
use App\ContentAttr;
use App\Subscription;

use Validator;
use Auth;

class AdminUserController extends Controller
{
    use GeneralTrait;

    public function userList()
    {
    	return view('admin/user/userList');
    }

    public function userTable(Request $request)
    {

    	$userList = User::all()
    	->toArray();

    	$userTable = [
    		"pagination" => [
    			'page' => $request->pagination['page'],
                'perpage' => $request->pagination['perpage'],
            ],
    		"data" => $userList
    	];

    	return response()->json($userTable);
    }

    public function userCreate()
    {
        $userSettingsCollection = Settings::where('type', Config::get('fixed.status.target.User'))->get();
        // foreach ($settingsCollections as $settings) {

        //     foreach ($settings->settingsAttrCollection as $settingsAttr) {
        //         dd( $settingsAttr->settingsAttrValue('user', 4, $settingsAttr->attr) );
        //     }
            
        // }
        
        return view('admin/user/userCreate', [
            'userSettingsCollection' => $userSettingsCollection
        ]);
    }

    public function userStore(Request $request)
    {
        $validator = Validator::make($request->only('email', 'password'), [
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        try {
            // prepaire and execute
            $newUserObj = new User;
            $newUserObj->name = $request->input('email');
            $newUserObj->email = $request->input('email');
            $newUserObj->password = bcrypt($request->input('password'));
            $newUserObj->verify_code = $this->randomDigit(6);
            $newUserObj->status_id = Config::get('settings.status.user.new');
            $newUserObj->type_id = Config::get('settings.type.user.regular');
            $newUserObj->firstname = $request->input('firstname');
            $newUserObj->lastname = $request->input('lastname');
            $newUserObj->mobile = $request->input('mobile');
            $newUserObj->address = $request->input('address');

            if($newUserObj->save()) {

                $userSettingsCollection = Settings::where('type', Config::get('fixed.status.target.User'))->get();
                foreach ($userSettingsCollection as $settings) {
                    foreach ($settings->settingsAttrCollection as $settingsAttr) {

                        if ($request->input($settingsAttr->attr)) {
                            ContentAttr::create([
                                'target' => 'user',
                                'target_id' => $newUserObj->id,
                                'attr' => $settingsAttr->attr,
                                'content' => $request->input($settingsAttr->attr),
                            ]);
                        }
                    }
                }

                return redirect()->route('adminUserIndex')->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // deactivating newly inserted model
            if ($newUserObj->exists) {
                // softdelete
                $newUserObj->delete();
            }
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function userEdit($user = null)
    {
        $userObject = User::findOrFail($user);
        $userSettingsCollection = Settings::where('type', Config::get('fixed.status.target.User'))->get();

        $userSubscriptionCollection = Subscription::where([
            'user_id' => $userObject->id,
        ])->get();

        return view('admin/user/userEdit', [
            'userObject' => $userObject,
            'userSettingsCollection' => $userSettingsCollection,
            'userSubscriptionCollection' => $userSubscriptionCollection
        ]);
    }

    public function userUpdate(Request $request, $user)
    {
        $userObject = User::findOrFail($user);

        try {
            // prepaire and execute
            $userObject->firstname = $request->input('firstname');
            $userObject->lastname = $request->input('lastname');
            $userObject->mobile = $request->input('mobile');
            $userObject->address = $request->input('address');

            if($userObject->save()) {
                return redirect()->route('adminUserIndex')->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // deactivating newly inserted model
            if ($userObject->exists) {
                // softdelete
                // $userObject->delete();
            }
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);

    }

    public function userDelete(Request $request, $user)
    {
        $userObject = User::find($user);
        if (!$userObject) {
            return back()->withErrors([
                'error' => 'Object not found'
            ]);
        }

        try {
            // prepaire and execute
            if($userObject->delete()) {
                return redirect()->route('adminUserIndex')->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);

    }
}
