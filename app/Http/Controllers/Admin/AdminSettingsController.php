<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

use App\Settings;
use App\SettingsAttr;
use App\SettingsAttrOption;

use Validator;
use Auth;

class AdminSettingsController extends Controller
{
    public function settingsGeneral()
    {
        $generalSettings = Settings::where('type', 'general')->get();
        $paymentSettings = Settings::where('type', 'payment')->get();

        return view('admin/settings/settingsGeneral', [
            'generalSettings' => $generalSettings,
            'paymentSettings' => $paymentSettings
        ]);
    }

    public function settingsGeneralApply(Request $request)
    {
        $validator = Validator::make($request->only('settings'), [
            'settings' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        $settingsObject = Settings::find($request->settings);
        if (!$settingsObject) {
            return back()->withErrors([
                'error' => 'unsuccessful attempt'
            ]);
        }

        foreach ($settingsObject->settingsAttrCollection as $key => $settings) {
            if ($request->input($settings->attr) !== $settings->value) {
                $settings->value = $request->input($settings->attr);
                $settings->save();
            }
        }
        
        return back()->with('success', $settingsObject->name . ' updated');
    }

    public function settingsList()
    {
    	$settingsCollection = Settings::where('type', 'form')->get();
        
    	return view('admin/settings/settingsList', [
    		'settingsCollection' => $settingsCollection,
    	]);
    }

    public function settingsCreate()
    {
    	$collection = collect(Config::get('fixed.status.target'));
    	$targetList = $collection->flip();

    	return view('admin/settings/settingsCreate', [
            'targetList' => $targetList->all(),
        ]);
    }

    public function settingsStore(Request $request)
    {
      	$validator = Validator::make($request->only('target', 'type', 'name'), [
            'target' => 'required',
            'type' => 'required',
          	'name' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        try {
            // prepaire and execute
            $newSettingsObj = new Settings;
            $newSettingsObj->target = $request->input('target');
            $newSettingsObj->type = $request->input('type');
            $newSettingsObj->name = $request->input('name');
            $newSettingsObj->description = $request->input('description');

            if($newSettingsObj->save()) {
                return redirect()->route('adminSettingsList')->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // deactivating newly inserted model
            if ($newSettingsObj->exists) {
                // softdelete
                $newSettingsObj->delete();
            }
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function settingsEdit($settings = null)
    {
    	$settingsObject = Settings::findOrFail($settings);

    	$collection = collect(Config::get('fixed.status.target'));
    	$targetList = $collection->flip();

        $collection = collect(Config::get('fixed.constant.form'));
        $settingsAttrType = $collection->flip();

    	return view('admin/settings/settingsEdit', [
            'targetList' => $targetList->all(),
            'settingsObject' => $settingsObject,
            'settingsAttrType' => $settingsAttrType,
        ]);
    }

    public function settingsUpdate(Request $request, $settings)
    {
    	$settingsObject = Settings::find($settings);
	    if(!$settingsObject) {
	        return redirect()->route('adminSettingsList')->withErrors([
	            'error' => 'unsuccessful attempt'
	        ]);
        }

    	$validator = Validator::make($request->only('target', 'name'), [
            'target' => 'required',
          	'name' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        try {
            // prepaire and execute
            $settingsObject->type = $request->input('target');
            $settingsObject->name = $request->input('name');
            $settingsObject->description = $request->input('description');

            if($settingsObject->save()) {
                return redirect()->route('adminSettingsList')->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // deactivating newly inserted model
            if ($settingsObject->exists) {
                // softdelete
                // $settingsObject->delete();
            }
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function settingsDelete(Request $request, $settings)
    {
        $settingsObject = Settings::find($settings);
        if (!$settingsObject) {
            return back()->withErrors([
                'error' => 'Object not found'
            ]);
        }

        // delete
        try {
            // prepaire and execute
            if($settingsObject->delete()) {
                return redirect()->route('adminSettingsList')->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function settingsAttrStore(Request $request, $settings)
    {
        $settingsObject = Settings::find($settings);
            if(!$settingsObject) {
                return back()->withErrors([
                'error' => 'unsuccessful attempt'
            ]);
        }

        // validate user input params
        $validator = Validator::make($request->only('attr', 'type', 'placeholder'), [
            'attr' => 'required',
            'type' => 'required',
            'placeholder' => 'required',
        ]);

        // Return back with errors
        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        try {
            // prepaire and execute
            $settingsAttrObj = new SettingsAttr;
            $settingsAttrObj->settings_id = $settingsObject->id;
            $settingsAttrObj->attr = $request->input('attr');
            $settingsAttrObj->type = $request->input('type');
            $settingsAttrObj->required = $request->input('required');
            $settingsAttrObj->placeholder = $request->input('placeholder');
            $settingsAttrObj->description = $request->input('description');

            if($settingsAttrObj->save()) {
                return redirect()->route('adminSettingsEdit', ['settings' => $settingsObject->id])->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // deactivating newly inserted model
            if ($settingsAttrObj->exists) {
                // softdelete
                // $settingsAttrObj->delete();
            }
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function settingsAttrEdit($settingsAttr)
    {
        $settingsAttrObject = SettingsAttr::findOrFail($settingsAttr);

        $collection = collect(Config::get('fixed.constant.form'));
        $settingsAttrType = $collection->flip();

        return view('admin/settings/settingsAttrView', [
            'settingsAttrObject' => $settingsAttrObject,
            'settingsAttrType' => $settingsAttrType,
        ]);
    }

    public function settingsAttrUpdate(Request $request, $settingsAttr)
    {
        $settingsAttrObject = SettingsAttr::find($settingsAttr);
            if(!$settingsAttrObject) {
                return back()->withErrors([
                'error' => 'unsuccessful attempt'
            ]);
        }

        // validate user input params
        $validator = Validator::make($request->only('attr', 'type', 'placeholder'), [
            'attr' => 'required',
            'type' => 'required',
            'placeholder' => 'required',
        ]);

        // Return back with errors
        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        try {
            // prepaire and execute
            $settingsAttrObject->attr = $request->input('attr');
            $settingsAttrObject->type = $request->input('type');
            $settingsAttrObject->placeholder = $request->input('placeholder');
            $settingsAttrObject->description = $request->input('description');
            $settingsAttrObject->required = $request->input('required');

            if($settingsAttrObject->save()) {
                return redirect()->route('adminSettingsEdit', ['settings' => $settingsAttrObject->settings_id])->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // deactivating newly inserted model
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function settingsAttrDelete(Request $request, $settingsAttr)
    {
        $settingsAttrObject = SettingsAttr::find($settingsAttr);
            if(!$settingsAttrObject) {
                return back()->withErrors([
                'error' => 'unsuccessful attempt'
            ]);
        }

        try {
            // prepaire and execute
            if($settingsAttrObject->delete()) {
                return redirect()->route('adminSettingsEdit', ['settings' => $settingsAttrObject->settings_id])->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // deactivating newly inserted model
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function settingsAttrOptionStore(Request $request, $settingsAttr)
    {
        $settingsAttrObject = SettingsAttr::find($settingsAttr);
            if(!$settingsAttrObject) {
                return back()->withErrors([
                'error' => 'unsuccessful attempt'
            ]);
        }

        // validate user input params
        $validator = Validator::make($request->only('text'), [
            'text' => 'required',
        ]);

        // Return back with errors
        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        try {
            // prepaire and execute
            $settingsAttrOtionObj = new SettingsAttrOption;
            $settingsAttrOtionObj->settings_attr_id = $settingsAttrObject->id;
            $settingsAttrOtionObj->text = $request->input('text');
            $settingsAttrOtionObj->description = $request->input('description');

            if($settingsAttrOtionObj->save()) {
                return redirect()->route('adminSettingsAttrEdit', ['settingsAttr' => $settingsAttrObject->id])->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // deactivating newly inserted model
            if ($settingsAttrOtionObj->exists) {
                // softdelete
                $settingsAttrOtionObj->delete();
            }
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function settingsAttrOptionDelete(Request $request, $settingsAttrOption)
    {
        $settingsAttrOptionObject = SettingsAttrOption::find($settingsAttrOption);
        if(!$settingsAttrOptionObject) {
            // false
        }

        try {
            // prepaire and execute
            if($settingsAttrOptionObject->delete()) {
                // true
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        // false
    }

}
