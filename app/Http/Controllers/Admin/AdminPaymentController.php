<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;

use Stripe\Error\Card;
use Cartalyst\Stripe\Stripe;

use App\Traits\GeneralTrait;
use App\User;
use App\PaymentBundle;
use App\PaymentBundleRule;

use Validator;

class AdminPaymentController extends Controller
{
	use GeneralTrait;

    public function paymentBundleList()
    {
    	return view('admin/payment/paymentBundle');
    }

    public function paymentBundleTable(Request $request)
    {
        $paymentBundleList = PaymentBundle::all()
        ->toArray();

        $pageTable = [
            "pagination" => [
                'page' => $request->pagination['page'],
                'perpage' => $request->pagination['perpage'],
            ],
            "data" => $paymentBundleList
        ];

        return response()->json($pageTable);
    }

    public function paymentBundleCreate()
    {
        $collection = collect(Config::get('settings.type.page'));
        $targetList = $collection->flip();

        $post_token = $this->randomDigit(12);

    	return view('admin/payment/paymentBundleCreate', [
            'post_token' => $post_token,
            'targetList' => $targetList->all(),
        ]);
    }

    public function paymentBundleRuleStore(Request $request)
    {
        // PaymentBundleRule
        $validator = Validator::make($request->only('count', 'target', 'token'), [
            'count' => 'required',
            'target' => 'required',
            'token' => 'required',
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status' => false,
                'message' => "Validator failed"
            ], 200);
        }

        try {
            $newPaymentBundleRuleObj = new PaymentBundleRule;
            $newPaymentBundleRuleObj->token = $request->input('token');
            $newPaymentBundleRuleObj->count = $request->input('count');
            $newPaymentBundleRuleObj->target = $request->input('target');
            $newPaymentBundleRuleObj->payment_bundle_id = $request->input('paymentId');
            

            if($newPaymentBundleRuleObj->save()) {
                return Response::json([
                    'status' => true,
                    'ruleId' => $newPaymentBundleRuleObj->id,
                    'message' => 'Rule added successfully'
                ], 200);
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return Response::json([
            'status' => false,
            'message' => "Unsuccessful attempt"
        ], 200);
    }

    public function paymentBundleRuleDelete(Request $request)
    {
        $paymentBundleRuleObj = PaymentBundleRule::find($request->input('ruleId'));
        if (!$paymentBundleRuleObj) {
            return Response::json([
                'status' => false,
                'message' => "Validator failed"
            ], 200);
        }

        try {

            if($paymentBundleRuleObj->delete()) {
                return Response::json([
                    'status' => true,
                    'message' => 'Rule deleted'
                ], 200);
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return Response::json([
            'status' => false,
            'message' => "Unsuccessful attempt"
        ], 200);

    }

    public function paymentBundleStore(Request $request)
    {
    	$validator = Validator::make($request->only('name', 'amount', 'currency', 'interval', 'description', 'token'), [
            'name' => 'required',
          	'amount' => 'required',
          	'currency' => 'required',
          	'interval' => 'required',
          	'description' => 'required',
            'token' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        try {

            $stripeInit = $this->getStaticSettings('payment', 'payment-stripe-key');
            $stripe = Stripe::make($stripeInit);
    	
	    	$plan = $stripe->plans()->create([
			    'id' => $request->input('token'),
			    'name' => $request->input('name'),
			    'amount' => $request->input('amount'),
			    'currency' => $request->input('currency'),
			    'interval' => $request->input('interval'),
			    'statement_descriptor' => $request->input('name'),
			]);
			
			if(!$plan['id']) {
				return back()->withErrors([
		            'error' => 'unsuccessful attempt'
		        ]);
			}

            $newPaymentBundleObj = new PaymentBundle;
            $newPaymentBundleObj->name = $request->input('name');
            $newPaymentBundleObj->amount = $request->input('amount');
            $newPaymentBundleObj->token = $request->input('token');
            $newPaymentBundleObj->stripe_code = $plan['id'];
            $newPaymentBundleObj->stripe_id = $plan['product'];
            $newPaymentBundleObj->description = $request->input('description');
            $newPaymentBundleObj->frequency = $request->input('interval');
            $newPaymentBundleObj->currency = $request->input('currency');

            if($newPaymentBundleObj->save()) {

                $paymentBundleRuleCollection = PaymentBundleRule::where('token', $request->input('token'))->get();

                foreach ($paymentBundleRuleCollection as $paymentBundleRule) {
                    $paymentBundleRule->payment_bundle_id = $newPaymentBundleObj->id;
                    $paymentBundleRule->save();
                }

                return redirect()->route('adminPaymentBundleList')->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function paymentBundleEdit($payment = null)
    {
        $paymentObject = PaymentBundle::findOrFail($payment);

        $collection = collect(Config::get('settings.type.page'));
        $targetList = $collection->flip();

        return view('admin/payment/paymentBundleEdit', [
            'paymentObject' => $paymentObject,
            'targetList' => $targetList,
        ]);
    }

    public function paymentBundleUpdate(Request $request, $payment)
    {
        $validator = Validator::make($request->only('name', 'amount', 'currency', 'interval', 'description', 'token'), [
            'name' => 'required',
            'amount' => 'required',
            'currency' => 'required',
            'interval' => 'required',
            'description' => 'required',
            'token' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        $paymentObject = PaymentBundle::find($payment);
        if(!$paymentObject) {
            return back()->withErrors([
                'error' => 'unsuccessful attempt'
            ]);
        }

        try {
            
            $stripeInit = $this->getStaticSettings('payment', 'payment-stripe-key');
            $stripe = Stripe::make($stripeInit);
        
            $plan = $stripe->plans()->update($request->input('token'), [
                'name' => $request->input('name'),
                'metadata' => [
                    'amount' => $request->input('amount'),
                    'currency' => $request->input('currency'),
                    'interval' => $request->input('interval'),
                ]
            ]);
            
            if(!$plan['id']) {
                return back()->withErrors([
                    'error' => 'unsuccessful attempt'
                ]);
            }

            $paymentObject->name = $request->input('name');
            $paymentObject->amount = $request->input('amount');
            $paymentObject->token = $request->input('token');
            $paymentObject->stripe_code = $plan['id'];
            $paymentObject->stripe_id = $plan['product'];
            $paymentObject->description = $request->input('description');
            $paymentObject->frequency = $request->input('interval');
            $paymentObject->currency = $request->input('currency');

            if($paymentObject->save()) {

                $paymentBundleRuleCollection = PaymentBundleRule::where('token', $request->input('token'))->get();

                foreach ($paymentBundleRuleCollection as $paymentBundleRule) {
                    $paymentBundleRule->payment_bundle_id = $paymentObject->id;
                    $paymentBundleRule->save();
                }

                return redirect()->route('adminPaymentBundleList')->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function paymentBundleDelete(Request $request, $payment)
    {
        $paymentBundleObject = PaymentBundle::find($payment);
        if (!$paymentBundleObject) {
            return back()->withErrors([
                'error' => 'not found'
            ]);
        }
        
        $stripeInit = $this->getStaticSettings('payment', 'payment-stripe-key');
        $stripe = Stripe::make($stripeInit);
        
        $plan = $stripe->plans()->delete($paymentBundleObject->token);
        
        if($paymentBundleObject->delete()) {
            return redirect()->route('adminPaymentBundleList')->with('success', 'Success');
        }
    }
}
