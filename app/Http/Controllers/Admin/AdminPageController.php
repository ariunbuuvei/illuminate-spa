<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Log;

use App\Traits\GeneralTrait;
use App\Page;
use App\Status;
use App\Category;
use App\File;
use App\Settings;
use App\ContentAttr;

use Validator;
use Auth;

class AdminPageController extends Controller
{
	use GeneralTrait;

	private $photos_path;
 
    public function __construct()
    {
        $this->photos_path = public_path('/images');
    }

    public function pageList()
    {
    	return view('admin/page/pageList');
    }

    public function pageTable(Request $request)
    {
    	$pageList = Page::dataTable()->toArray();

        $pageTable = [
            "pagination" => [
                'page' => $request->pagination['page'],
                'perpage' => $request->pagination['perpage'],
            ],
            "data" => $pageList
        ];

        return response()->json($pageTable);
    }

    public function pageCreate()
    {
        $settingsCollections = Settings::where('target', Config::get('settings.target.Page'))->get();
        
    	$statusList = Status::where('target', Config::get('settings.target.Page'))
    	->pluck('name', 'id')
    	->toArray();

    	$categoryList = Category::where('target', Config::get('settings.target.Page'))
    	->pluck('name', 'id')
    	->toArray();

        $collection = collect(Config::get('settings.type.page'));
        $typeList = $collection->flip();

    	$post_token = $this->randomDigit(12);

    	return view('admin/page/pageCreate', [
            'settingsCollections' => $settingsCollections,
            'typeList' => $typeList,
    		'statusList' => $statusList,
    		'categoryList' => $categoryList,
    		'postToken' => $post_token,
    	]);
    }

    public function pageStore(Request $request)
    {
        $validator = Validator::make($request->only('post_token', 'title', 'type', 'status', 'category', 'content'), [
            'post_token' => 'required',
            'title' => 'required|unique:pages,title',
            'type' => 'required',
            'status' => 'required',
            'category' => 'required',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput($request->all());
        }

        try {
            // prepaire and execute
            $newPageObj = new Page;
            $newPageObj->post_token = $request->input('post_token');
            $newPageObj->title = $request->input('title');
            $newPageObj->description = $request->input('description');
            $newPageObj->status_id = $request->input('status');
            $newPageObj->type_id = $request->input('type');
            $newPageObj->category_id = $request->input('category');
            $newPageObj->author_id = Auth::user()->id;
            $newPageObj->content = $request->input('content');

            $fileObjectQuery = File::where([
                'token' => $request->input('post_token'),
                'type_id' => Config::get('settings.type.file.Thumbnail')
            ]);

            if($fileObjectQuery->exists()) {
                $thumbFileObject = $fileObjectQuery->first();
                $newPageObj->file_id = $thumbFileObject->id;
            }

            $settingsCollections = Settings::where([
                'target' => Config::get('settings.target.Page'),
                'type' => 'form'
            ])->get();

            if ($settingsCollections) {
                foreach ($settingsCollections as $settings) {
                    if ($settings->settingsAttrCollection) {
                        foreach ($settings->settingsAttrCollection as $key => $settingsAttr) {
                            if ($settingsAttr->required) {
                                if(!$request->input($settingsAttr->attr)) {
                                    return back()
                                    ->withErrors('Validator must not empty')
                                    ->withInput($request->all());
                                }
                            }
                        }
                    }
                }
            }
            
            if($newPageObj->save()) {

                $fileCollectionQuery = File::where([
                    'token' => $request->input('post_token'),
                    'type_id' => Config::get('settings.type.file.Thumbnail')
                ]);

                if($fileCollectionQuery->exists()) {
                    foreach ($fileCollectionQuery->get() as $fileObject) {
                        $fileObject->target = Config::get('settings.target.Page');
                        $fileObject->target_id = $newPageObj->id;
                        $fileObject->save();

                    }
                }

                if ($settingsCollections) {
                    foreach ($settingsCollections as $settings) {
                        if ($settings->settingsAttrCollection) {
                            foreach ($settings->settingsAttrCollection as $key => $settingsAttr) {
                                ContentAttr::create([
                                    'target' => 'page',
                                    'target_id' => $newPageObj->id,
                                    'attr' => $settingsAttr->attr,
                                    'content' => $request->input($settingsAttr->attr),
                                ]);
                            }
                        }
                    }
                }

                return redirect()->route('adminPageList')->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function pageEdit($page = null)
    {
        if (!$page) {
            abort(404);
        }

        $settingsCollections = Settings::where('target', Config::get('settings.target.Page'))->get();

        $pageObject = Page::findOrFail($page);

        $collection = collect(Config::get('settings.type.page'));
        $typeList = $collection->flip();

        $statusList = Status::where('target', Config::get('settings.target.Page'))
        ->pluck('name', 'id')
        ->toArray();

        $categoryList = Category::where('target', Config::get('settings.target.Page'))
        ->pluck('name', 'id')
        ->toArray();

        return view('admin/page/pageEdit', [
            'typeList' => $typeList,
            'pageObject' => $pageObject,
            'statusList' => $statusList,
            'categoryList' => $categoryList,
            'settingsCollections' => $settingsCollections,
            'settingsCollectionValue' => $pageObject->pageAttrCollection->pluck('content', 'attr')->toArray(),
        ]);

    }

    public function pageUpdate(Request $request, $page)
    {
        $pageObject = Page::findOrFail($page);

        $validator = Validator::make($request->only('post_token', 'title', 'status', 'category', 'description', 'content'), [
            'post_token' => 'required',
            'title' => 'required',
            'status' => 'required',
            'category' => 'required',
            'description' => 'required',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        try {
            // prepaire and execute
            $pageObject->post_token = $request->input('post_token');
            $pageObject->title = $request->input('title');
            $pageObject->description = $request->input('description');
            $pageObject->status_id = $request->input('status');
            $pageObject->type_id = $request->input('type');
            $pageObject->category_id = $request->input('category');
            $pageObject->author_id = Auth::user()->id;
            $pageObject->content = $request->input('content');
            if($pageObject->save()) {

                $settingsCollections = Settings::where([
                    'target' => Config::get('settings.target.Page'),
                    'type' => 'form'
                ])->get();

                if ($settingsCollections) {
                    foreach ($settingsCollections as $settings) {
                        if ($settings->settingsAttrCollection) {
                            foreach ($settings->settingsAttrCollection as $key => $settingsAttr) {

                                if ($settingsAttr->required) {
                                    if(!$request->input($settingsAttr->attr)) {
                                        return back()
                                        ->withErrors('Validator must not empty')
                                        ->withInput($request->all());
                                    }
                                }

                                $currentContentAttr = ContentAttr::where([
                                    'target' => 'page',
                                    'target_id' => $pageObject->id,
                                    'attr' => $settingsAttr->attr,
                                ]);

                                if ($currentContentAttr->exists()) {
                                    $currentContentAttr = $currentContentAttr->first();
                                } else {
                                    $currentContentAttr = new ContentAttr;
                                }

                                $currentContentAttr->content = $request->input($settingsAttr->attr);
                                $currentContentAttr->target = 'page';
                                $currentContentAttr->target_id = $pageObject->id;
                                $currentContentAttr->attr = $settingsAttr->attr;
                                $currentContentAttr->save();

                            }
                        }
                    }
                }

                return redirect()->route('adminPageList')->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function fileUpload(Request $request)
    {
    	$photos = $request->file('file');
 
        if (!is_array($photos)) {
            $photos = [$photos];
        }
 
        if (!is_dir($this->photos_path)) {
            mkdir($this->photos_path, 0777);
        }

        try {

            for ($i = 0; $i < count($photos); $i++) {
	            $photo = $photos[$i];
	            $name = sha1(date('YmdHis') . str_random(30));
	            $save_name = $name . '.' . $photo->getClientOriginalExtension();
	            $resize_name = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();
	 
	            Image::make($photo)
	                ->resize(250, null, function ($constraints) {
	                    $constraints->aspectRatio();
	                })
	                ->save($this->photos_path . '/' . $resize_name);
	 
	            $photo->move($this->photos_path, $save_name);
	           
                // 'file' => [
                //     'single' => 1,
                //     'multi' => 2,
                //     'thumbnail' => 3,
                // ]

	            $upload = new File();
                $upload->target = Config::get('settings.target.Page');
                $upload->type_id = Config::get('settings.type.file.Thumbnail');
	            $upload->name = $save_name;
	            $upload->small_name = $resize_name;
	            $upload->path = basename($photo->getClientOriginalName());
	            $upload->token = $request->input('token');
	            $upload->save();
	        }
	        return Response::json([
	            'message' => 'Image saved Successfully'
	        ], 200);

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return Response::json('error', 400);
    }

    public function pageDelete(Request $request, $page)
    {
        $pageObject = Page::find($page);
        if (!$pageObject) {
            return back()->withErrors([
                'error' => 'not found'
            ]);
        }

        // delete
        if($pageObject->delete()) {
            return redirect()->route('adminPageList')->with('success', 'Success');
        }
    }

    // EDITORS SECTION
    public function pageEditorEdit($page = null)
    {
        $pageEditorObject = Page::findOrFail($page);

        return view('admin/page/pageEditorEdit', [
            'pageEditorObject' => $pageEditorObject
        ]);
    }

    public function pageEditorUpdate(Request $request, $page)
    {
        $validator = Validator::make($request->only('html'), [
            'html' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        $pageObject = Page::find($page);
        if (!$pageObject) {
            return Response::json([
                'status' => false,
                'data' => null,
            ], 200);
        }

        // 'page' => [
        //     'Blog' => 1,
        //     'Section' => 2,
        //     'Modal' => 3,
        // ]

        try {
            $search = array(
                '/\>[^\S ]+/s',     // strip whitespaces after tags, except space
                '/[^\S ]+\</s',     // strip whitespaces before tags, except space
                '/(\s)+/s',         // shorten multiple whitespace sequences
                '/<!--(.|\s)*?-->/' // Remove HTML comments
            );

            $replace = array(
                '>',
                '<',
                '\\1',
                ''
            );

            $buffer = preg_replace($search, $replace, $request->input('html'));

            // prepaire and execute
            $pageObject->content = $buffer;

            if($pageObject->save()) {
                return Response::json([
                'status' => "Page updated",
                    'data' => null,
                ], 200);
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function pageEditorDelete(Request $request, $page)
    {
        $pageObject = Page::find($page);
        if (!$pageObject) {
            return back()->withErrors([
                'error' => 'not found'
            ]);
        }

        // delete
        if($pageObject->delete()) {
            return redirect()->route('adminPageEditorList')->with('success', 'Success');
        }
    }

    public function pageEditorUploadImage(Request $request, $page)
    {
        $pageObject = Page::find($page);
        if (!$pageObject) {
            return Response::json([
                'status' => false,
                'data' => null,
            ], 200);
        }

        $photo = $request->file('image');

        $name = sha1(date('YmdHis') . str_random(30));
        $save_name = $name . '.' . $photo->getClientOriginalExtension();
        $resize_name = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

        $width = 250;

        $image = Image::make($photo)
        ->resize($width, null, function ($constraints) {
            $constraints->aspectRatio();
        })->save($this->photos_path . '/' . $resize_name);

        $photo->move($this->photos_path, $save_name);

        $upload = new File();
        $upload->name = $save_name;
        $upload->small_name = $resize_name;
        $upload->path = basename($photo->getClientOriginalName());
        $upload->token = $this->randomDigit(12);
        $upload->save();

        $url = asset('images') . "/" . $resize_name;

        return Response::json([
            'status' => "image uploaded",
            'data' => [
                'url' => $url,
                'size' => [$width, $image->height()],
                'alt' => $pageObject->title
            ]
        ], 200);
    }

    public function enquireList()
    {
        $enquireList = ContentAttr::where([
            'target' => Config::get('settings.target.Page'),
            'attr' => 'enquire',
            'type_id' => Config::get('settings.type.content.Comment'),
        ])->get();

        return view('admin/enquiry/enquiryList', [
            'enquireList' => $enquireList
        ]);
    }

    public function enquireSingle($enquire)
    {
        $enquireObject = ContentAttr::findOrFail($enquire);
        return view('admin/enquiry/enquiryView', [
            'enquireObject' => $enquireObject
        ]);
    }

}
