<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

use App\PaymentBundle;
use App\MetaTag;
use App\Page;

class AdminController extends Controller
{
    public function dashboard()
    {
    	$paymentBundle = PaymentBundle::all();

        return view('admin/dashboard/welcome', [
        	'paymentBundle' => $paymentBundle
        ]);
    }

}
