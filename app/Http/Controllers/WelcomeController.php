<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

use App\Traits\GeneralTrait;
use App\Traits\UserTrait;

use App\Mail\UserMail;

use App\User;
use App\Page;
use App\VerifyToken;
use App\ContentAttr;

use Validator;
use Exception;
use Auth;

class WelcomeController extends Controller
{
    use GeneralTrait, UserTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageHeader = [
            'seoObjects' => null,
        ];

        // return (new UserMail([
        //     'template' => 'invoice',
        //     'title' => 'Thank you, NAME',
        //     'body' => 'This is the invoice and receipt for your recent order. Please keep a copy for your records.',
        //     'table' => [
        //         'head' => 'Invoice Summary',
        //         'body' => [
        //             ['Invoice Number:', '164434170'],
        //             ['Invoice Date:', '11/13/2018 12:50:11 PM'],
        //             ['Billing Information:', 'Bishara Hatoum'],
        //             ['Billing Description:', 'Description Description']
        //         ],
        //     ],
        //     'link' => null,
        // ]))->render();

        return view('pages/general/welcome', [
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register()
    {
        if (Auth::check()) {
            return redirect()->route('publicWelcomeIndex');
        }

        return view('pages/login/register');
    }

    public function registerAction(Request $request)
    {   
        $validator = Validator::make($request->only('inputEmail', 'inputPassword', 'inputPasswordConfirmation'), [
            'inputEmail' => 'required|email|unique:users,email',
            'inputPassword' => 'required|min:8',
            'inputPasswordConfirmation' => 'required|same:inputPassword'
        ]);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        try {
            // prepaire and execute
            $newUserObj = new User;
            $newUserObj->name = $request->input('inputEmail');
            $newUserObj->email = $request->input('inputEmail');
            $newUserObj->password = bcrypt($request->input('inputPassword'));
            $newUserObj->verify_code = $this->randomDigit(10);
            $newUserObj->status_id = Config::get('settings.status.user.new');
            $newUserObj->type_id = Config::get('settings.type.user.regular');
            
            if($newUserObj->save()) {

                Mail::to($newUserObj->email)->send(new UserMail([
                    'template' => 'general',
                    'title' => 'Thank you',
                    'body' => 'please click here',
                    'link' => route('publicWelcomeVerify', ['token' => $newUserObj->verify_code, 'user' => $newUserObj->id]),
                ]));

                $this->userActionLogger([
                    'user_id' => $newUserObj->id,
                    'status_id' => Config::get('settings.status.logger.Succesful'),
                    'type_id' => Config::get('settings.type.logger.Create'),
                    'controller' => 'WelcomeController',
                    'class' => 'registerAction',
                    'line' => 89,
                    'code' => 200,
                    'message' => 'User read statusList',
                    'target' => Config::get('settings.target.User'),
                    'target_id' => $newUserObj->id,
                ], [
                    // 'email', 'notification'
                ]);

                if (Auth::attempt([
                    'email' => $request->input('inputEmail'), 
                    'password' => $request->input('inputPassword')
                ])) {
                    return redirect()->route(Config::get('settings.general.login.redirect'));
                }

                return redirect()->route('publicWelcomeShow');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            $this->userActionLogger([
                'user_id' => isset($newUserObj->id) ? $newUserObj->id : null,
                'status_id' => Config::get('settings.status.logger.Succesful'),
                'type_id' => Config::get('settings.type.logger.Create'),
                'controller' => 'WelcomeController',
                'class' => 'registerAction',
                'line' => 89,
                'code' => 403,
                'message' => $request->input('inputEmail') . ": - " . $e->getMessage(),
                'target' => Config::get('settings.target.User'),
                'target_id' => isset($newUserObj->id) ? $newUserObj->id : null,
            ], [
                'email'
            ]);

        } catch (\Exception $e) {
            // store error log, handle gracefully
            $this->userActionLogger([
                'user_id' => isset($newUserObj->id) ? $newUserObj->id : null,
                'status_id' => Config::get('settings.status.logger.Succesful'),
                'type_id' => Config::get('settings.type.logger.Create'),
                'controller' => 'WelcomeController',
                'class' => 'registerAction',
                'line' => 89,
                'code' => 403,
                'message' => $request->input('inputEmail') . ": - " . $e->getMessage(),
                'target' => Config::get('settings.target.User'),
                'target_id' => isset($newUserObj->id) ? $newUserObj->id : null,
            ], [
                'email'
            ]);
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    public function registerVerify()
    {
        $validator = Validator::make($request->only('token', 'user'), [
            'token' => 'required',
            'user' => 'required'
        ]);

        if ($validator->fails()) {
            abort(404);
        }

        $userObject = User::find( $request->input('user') );
        if (!$userObject) {
            abort(404);
        }

        if ($userObject->verify_code == $request->input('token')) {
            $userObject->status = 1;
            $userObject->save();
        }

        abort(404);
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route(Config::get('settings.general.logout.redirect'))->with('success', 'Successfully Logged out!');
    }

    public function login()
    {
        if (Auth::check()) {
            return redirect()->route('publicWelcomeIndex');
        }

        return view('pages/login/login');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function loginAction(Request $request)
    {
        // validate user input params
        $validator = Validator::make($request->only('email', 'password'), [
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);

        // Return back with errors
        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput($request->only('email'));
        }

        // Login attemp
        if (Auth::attempt($request->only('email', 'password'))) {
            // Log user action
            return redirect()->route(Config::get('settings.general.login.redirect'))->with('success', 'Welcome!');
        }

        // Login attempt fails back with error
        return back()
        ->withInput($request->only('email'))
        ->withErrors([
            'error' => 'Wrong details'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function forgot()
    {
        return view('pages/login/forgot');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function forgotAction(Request $request)
    {
        // validate user input params
        $validator = Validator::make($request->only('inputEmail'), [
            'inputEmail' => 'required|email',
        ]);

        // Return back with errors
        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        $userObject = User::where([
            'email' => $request->input('inputEmail'),
        ])->first();

        if ($userObject) {

            $verifyTokenObject = VerifyToken::where([
                'target_id' => $userObject->id,
                'target' => 'forgotpassword',
            ])->where('expire_at', '>', now()->toDateTimeString())
            ->first();

            if (!$verifyTokenObject) {
                $verifyTokenObject = new VerifyToken;
                $verifyTokenObject->target_id = $userObject->id;
                $verifyTokenObject->target = 'forgotpassword';
                $verifyTokenObject->token = $this->randomDigit(32);
                $verifyTokenObject->expire_at = now()->addHours(12);
                $verifyTokenObject->save();
            }

            Mail::to($userObject->email)->send(new UserMail([
                'template' => 'general',
                'title' => 'Please reset your password',
                'body' => 'please click here',
                'link' => route('publicWelcomePasswordForgotReset', ['user' => $userObject->id, 'token' => $verifyTokenObject->token]),
            ]));
            
            return redirect()->route('publicWelcomeLogin')->with('success', 'Please check your email!');
        }

        return back()
        ->withErrors([
            'error' => 'User not found'
        ]);
        
    }

    public function forgotReset($user, $token)
    {
        $userObject = User::findOrFail($user);
        
        $verifyTokenObject = VerifyToken::where([
            'target_id' => $userObject->id,
            'target' => 'forgotpassword',
            'token' => $token,
        ])->where('expire_at', '>', now()->toDateTimeString())
        ->first();

        if (!$verifyTokenObject) {
            abort(404);
        }

        return view('pages/login/reset', [
            'userObject' => $userObject,
            'verifyTokenObject' => $verifyTokenObject
        ]);
    }

    public function forgotResetAction(Request $request, $user, $token)
    {
        $validator = Validator::make($request->only('inputPassword', 'inputPasswordConfirmation'), [
            'inputPassword' => 'required|min:8',
            'inputPasswordConfirmation' => 'required|same:inputPassword'
        ]);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        $userObject = User::find($user);
        if (!$userObject) {
            return back()
            ->withErrors([
                'error' => 'Token expired'
            ]);
        }
        
        $verifyTokenObject = VerifyToken::where([
            'target_id' => $userObject->id,
            'target' => 'forgotpassword',
            'token' => $token,
        ])->where('expire_at', '>', now()->toDateTimeString())
        ->first();

        if (!$verifyTokenObject) {
            return back()
            ->withErrors([
                'error' => 'Token expired'
            ]);
        }

        $userObject->password = bcrypt($request->input('inputPassword'));
        $userObject->save();

        return redirect()->route('publicWelcomeLogin')->with('success', 'Please login');
    }

    public function emailsub(Request $request)
    {
        $validator = Validator::make($request->only('submail'), [
            'submail' => 'required|email',
        ]);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        $mailSubscribeObj = new ContentAttr;
        $mailSubscribeObj->target_id = Auth::check() ? Auth::user()->id : 0;
        $mailSubscribeObj->target = 'emailsub';
        $mailSubscribeObj->attr = $request->input('submail');
        $mailSubscribeObj->save();

        return redirect()->route('publicWelcomeIndex')->with('success', 'Thank you');
    }
}
