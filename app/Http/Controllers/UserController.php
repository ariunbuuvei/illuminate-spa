<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\User;

use Validator;
use Exception;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        if (!Auth::check()) {
            return redirect()->route('publicWelcomeLogin')->withErrors([
                'error' => 'Please login'
            ]);
        }

        $userObject = Auth::user();

        return view('pages/general/profile', [
            'userObject' => $userObject
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!Auth::check()) {
            return redirect()->route('publicWelcomeLogin')->withErrors([
                'error' => 'Please login'
            ]);
        }

        $userObject = User::find(Auth::user()->id);

        try {
            // prepaire and execute
            $userObject->firstname = $request->input('firstname');
            $userObject->lastname = $request->input('lastname');
            $userObject->mobile = $request->input('mobile');
            $userObject->address = $request->input('address');

            if($userObject->save()) {
                return redirect()->route('privateUserEdit')->with('success', 'Success');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            // store error log, handle gracefully
            Log::error($e->getMessage());
        }

        return back()->withErrors([
            'error' => 'unsuccessful attempt'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
