<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\ContentAttr;

class SettingsAttr extends Model
{
	use SoftDeletes;

	public function settingsAttrOptionCollection()
    {
        return $this->hasMany('App\SettingsAttrOption', 'settings_attr_id');
    }

    public function typeName()
    {
        return $this->belongsTo('App\Type', 'type');
    }

    public static function settingsAttrValue($target, $target_id, $attr)
    {
    	$settingsAttrValueObj = ContentAttr::where([
    		'attr' => $attr,
    		'target' => $target,
    		'target_id' => $target_id,
    	])->first();

    	if ($settingsAttrValueObj) {
    		return $settingsAttrValueObj->content;
    	}

    	return null;
    }
}
