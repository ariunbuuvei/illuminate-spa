<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
	protected $with = [
        'paymentBundleObject'
    ];

    public function paymentBundleObject()
    {
        return $this->hasOne('App\PaymentBundle', 'stripe_code', 'stripe_plan');
    }
}
