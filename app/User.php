<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Notifiable, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status_id', 'type_id', 'is_active', 'verify_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'is_admin', 'role_id'
    ];

    protected $with = [
        'statusName'
    ];

    protected $dates = [
        'deleted_at',
    ];

    public function statusName()
    {
        return $this->belongsTo('App\Status', 'status_id');
    }

    public function contentAttrCollection()
    {
        return $this->hasMany('App\ContentAttr', 'target_id')->where('target', 4);
    }

    static function getProfile($payer_id)
    {

        return DB::table(TBL_SETTINGS)
            ->join(TBL_SETTINGS_ATTRS, function ($join) {
                $join->on(TBL_SETTINGS . '.id', '=', TBL_SETTINGS_ATTRS . '.settings_id')
                    ->where(TBL_SETTINGS . ".target", 'payer')
                    ->where(TBL_SETTINGS . ".type", 'form')
                    ->where(TBL_SETTINGS . ".active", 1)
                    ->where(TBL_SETTINGS_ATTRS . ".active", 1);

            })
            ->leftJoin(TBL_PAYER_ATTRS, function ($join) use ($payer_id) {

                $join->on(TBL_SETTINGS_ATTRS . ".id", '=', TBL_PAYER_ATTRS . ".settings_attr_id")
                    ->where(TBL_PAYER_ATTRS . '.payer_id', '=', $payer_id);


            })
            ->select(TBL_SETTINGS_ATTRS . '.*', TBL_PAYER_ATTRS . ".value as attr_value")
            ->orderBy(TBL_SETTINGS_ATTRS . '.orders')
            ->get();
    }
    
}
