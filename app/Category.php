<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
	use SoftDeletes;
	
    protected $dates = [
    	'deleted_at',
    ];

    protected $with = [
        'parentObject'
    ];

    public function parentObject()
    {
        return $this->belongsTo('App\Category', 'parent_id', 'id');
    }
}
