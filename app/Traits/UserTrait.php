<?php

namespace App\Traits;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use App\Mail\AdminMail;

use App\UserLogger;
use App\Notification;
use App\NotificationTarget;

trait UserTrait
{
    public function userActionLogger($params, $report)
    {
    	$newUserLoggerObj = new UserLogger;
    	$newUserLoggerObj->user_id = $params['user_id'];
    	$newUserLoggerObj->session_ip = \Request::ip();
    	$newUserLoggerObj->status_id = $params['status_id'];
    	$newUserLoggerObj->type_id = $params['type_id'];
    	$newUserLoggerObj->controller = $params['controller'];
    	$newUserLoggerObj->class = $params['class'];
    	$newUserLoggerObj->line = $params['line'];
    	$newUserLoggerObj->code = $params['code'];
    	$newUserLoggerObj->message = $params['message'];
    	$newUserLoggerObj->target = $params['target'];
    	$newUserLoggerObj->target_id = $params['target_id'];

    	if ($newUserLoggerObj->save()) {
    		foreach ($report as $key => $value) {
	    		switch ($value) {
	    			case 'email':
		    			Mail::to(
		    				Config::get('settings.general.webAdmin')
		    			)->send(
		    				new AdminMail([
		    					'from' => Config::get('app.name'),
		    					'content' => str_replace(",","<br>",json_encode($newUserLoggerObj->toArray(), JSON_PRETTY_PRINT))
		    				])
		    			);
	    			break;

	    			case 'notification':
	    				self::userNotify($params);
	    			break;

	    			default:
	    					# code...
	    			break;
	    		}
	    	}	
    		return true;
    	}
        
    	abort(500);
    }

    public function userNotify($params)
    {
    	$newNotificationObj = new Notification();
    	$newNotificationObj->user_id = $params['user_id'];
    	$newNotificationObj->target = $params['target'];
    	$newNotificationObj->target_id = $params['target_id'];
    	$newNotificationObj->type_id = $params['type_id'];
    	$newNotificationObj->message = $params['message'];

    	if ($newNotificationObj->save()) {

    		// notification targets
    		$newNotificationTargetObj = new NotificationTarget();
    		$newNotificationTargetObj->notification_id = $newNotificationObj->id;
    		$newNotificationTargetObj->target = 'user';
    		$newNotificationTargetObj->target_id = $params['user_id'];
    		$newNotificationTargetObj->save();

			// Broadcaster
    	}
    }
}