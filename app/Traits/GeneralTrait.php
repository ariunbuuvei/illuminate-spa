<?php

namespace App\Traits;

use Illuminate\Support\Facades\Config;

use App\Settings;
use App\SettingsAttr;

trait GeneralTrait
{

	public function randomDigit($length)
    {
        $pool = array_merge(range(0,9), range('A', 'Z'));
        $key = '';
        for($i=0; $i < $length; $i++) {
            $key .= $pool[mt_rand(0, count($pool) - 1)];
        }

        return $key;
    }

    public function getStaticSettings($target, $attr)
    {
    	$parentSetting = Settings::where('type', $target)->first();
    	if (!$parentSetting) {
    		return null;
    	}

    	$currentSetting = SettingsAttr::where([
    		'settings_id' => $parentSetting->id,
    		'attr' => $attr
    	])->first();
    	if (!$currentSetting) {
    		return null;
    	}

    	return $currentSetting->value;
    }
    
}