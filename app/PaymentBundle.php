<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentBundle extends Model
{
    public function PaymentBundleRuleCollection()
    {
        return $this->hasMany('App\PaymentBundleRule', 'payment_bundle_id');
    }
}
