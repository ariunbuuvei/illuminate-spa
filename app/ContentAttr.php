<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class ContentAttr extends Model
{
    protected $fillable = [
        'target', 'target_id', 'attr', 'content'
    ];

    public function contentTargetName()
    {
        // 'page' => [
        //     'Blog' => 9,
        //     'Section' => 10,
        //     'Event' => 20,
        //     'Client' => 21,
        // ],

        switch ($this->attributes['target']) {
        	case Config::get('settings.type.page.page'):
        		$pageObject = Page::find($this->attributes['target_id']);
        		break;
        	
        	default:
        		$pageObject = Page::find($this->attributes['target_id']);
        		break;
        }

        if ($pageObject) {
            return $pageObject;
        }

        return null;
    }
}
