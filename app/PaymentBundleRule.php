<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentBundleRule extends Model
{
	public function paymentTypeObject()
	{
		return $this->belongsTo('App\Type', 'target');
	}
}
