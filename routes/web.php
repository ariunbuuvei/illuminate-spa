<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function () {

	Route::group(['namespace' => 'Admin'], function () {

		Route::group(['middleware' => 'adminRoleMiddleware'], function () {

			Route::get('/', 'AdminController@dashboard')->name('adminWelcomeIndex');

			// STATUS ROUTE
			// controler => AdminStatusController
			// model => Status
			// db => statuses

			Route::get('/status', 'AdminStatusController@statusList')->name('adminStatusList');

			Route::post('/status/table', 'AdminStatusController@statusTable')->name('adminStatusTable');

			Route::get('/status/create', 'AdminStatusController@statusCreate')->name('adminStatusCreate');

			Route::post('/status/store', 'AdminStatusController@statusStore')->name('adminStatusStore');

			Route::get('/status/edit/{status?}', 'AdminStatusController@statusEdit')->name('adminStatusEdit');

			Route::post('/status/update/{status}', 'AdminStatusController@statusUpdate')->name('adminStatusUpdate');

			Route::post('/status/delete/{status}', 'AdminStatusController@statusDelete')->name('adminStatusDelete');

			// CATEGORY ROUTE
			// controler => AdminCategoryController
			// model => Category
			// db => categories

			Route::get('/category', 'AdminCategoryController@categoryList')->name('adminCategoryList');

			Route::post('/category/table', 'AdminCategoryController@categoryTable')->name('adminCategoryTable');

			Route::get('/category/create', 'AdminCategoryController@categoryCreate')->name('adminCategoryCreate');

			Route::post('/category/store', 'AdminCategoryController@categoryStore')->name('adminCategoryStore');

			Route::get('/category/edit/{category?}', 'AdminCategoryController@categoryEdit')->name('adminCategoryEdit');

			Route::post('/category/update/{category}', 'AdminCategoryController@categoryUpdate')->name('adminCategoryUpdate');

			Route::post('/category/autocomplete', 'AdminCategoryController@categoryAutocomplete')->name('adminCategoryAutocomplete');

			Route::post('/category/delete/{category}', 'AdminCategoryController@categoryDelete')->name('adminCategoryDelete');

			// CUSTOM FORM ROUTE
			// controler => AdminSettingsController
			// model => Settings
			// db => settings

			Route::get('/form', 'AdminSettingsController@settingsList')->name('adminSettingsList');

			Route::get('/form/create', 'AdminSettingsController@settingsCreate')->name('adminSettingsCreate');

			Route::post('/form/store', 'AdminSettingsController@settingsStore')->name('adminSettingsStore');

			Route::get('/form/edit/{settings?}', 'AdminSettingsController@settingsEdit')->name('adminSettingsEdit');

			Route::post('/form/update/{settings}', 'AdminSettingsController@settingsUpdate')->name('adminSettingsUpdate');

			Route::post('/form/delete/{settings}', 'AdminSettingsController@settingsDelete')->name('adminSettingsDelete');

			Route::post('/form/attr/store/{settings}', 'AdminSettingsController@settingsAttrStore')->name('adminSettingsAttrStore');

			Route::get('/form/attr/edit/{settingsAttr}', 'AdminSettingsController@settingsAttrEdit')->name('adminSettingsAttrEdit');

			Route::post('/form/attr/update/{settingsAttr}', 'AdminSettingsController@settingsAttrUpdate')->name('adminSettingsAttrUpdate');

			Route::post('/form/attr/delete/{settingsAttr}', 'AdminSettingsController@settingsAttrDelete')->name('adminSettingsAttrDelete');

			Route::post('/form/attr/option/store/{settingsAttr}', 'AdminSettingsController@settingsAttrOptionStore')->name('adminSettingsAttrOptionStore');

			Route::post('/form/attr/option/delete/{settingsAttrOption}', 'AdminSettingsController@settingsAttrOptionDelete')->name('adminSettingsAttrOptionDelete');

			Route::get('/settings/general', 'AdminSettingsController@settingsGeneral')->name('adminSettingsGeneral');

			Route::post('/settings/general/update', 'AdminSettingsController@settingsGeneralApply')->name('adminSettingsGeneralApply');


			// USER ROUTE
			// controler => AdminUserController
			// model => User
			// db => users

			Route::get('/user', 'AdminUserController@userList')->name('adminUserIndex');

			Route::post('/user/table', 'AdminUserController@userTable')->name('adminUserTable');

			Route::get('/user/create', 'AdminUserController@userCreate')->name('adminUserCreate');

			Route::post('/user/store', 'AdminUserController@userStore')->name('adminUserStore');

			Route::get('/user/edit/{user?}', 'AdminUserController@userEdit')->name('adminUserEdit');

			Route::post('/user/update/{user}', 'AdminUserController@userUpdate')->name('adminUserUpdate');

			Route::post('/user/delete/{user}', 'AdminUserController@userDelete')->name('adminUserDelete');

			// BLOG ROUTE
			// controler => AdminPageController
			// model => Page
			// db => users

			Route::get('/content', 'AdminPageController@pageList')->name('adminPageList');

			Route::get('/content/create', 'AdminPageController@pageCreate')->name('adminPageCreate');

			Route::post('/content/table', 'AdminPageController@pageTable')->name('adminPageTable');

			Route::post('/content/store', 'AdminPageController@pageStore')->name('adminPageStore');

			Route::get('/content/edit/{page?}', 'AdminPageController@pageEdit')->name('adminPageEdit');

			Route::post('/content/update/{page}', 'AdminPageController@pageUpdate')->name('adminPageUpdate');

			Route::post('/content/delete/{page}', 'AdminPageController@pageDelete')->name('adminPageDelete');

			Route::post('/content/file/upload', 'AdminPageController@fileUpload')->name('adminFileUpload');

			// PAGE ROUTE
			// controler => AdminPageController
			// model => Page
			// db => users

			Route::get('/content/editor/edit/{page?}', 'AdminPageController@pageEditorEdit')->name('adminPageEditorEdit');

			Route::post('/content/editor/update/{page}', 'AdminPageController@pageEditorUpdate')->name('adminPageEditorUpdate');

			Route::post('/content/editor/delete/{page}', 'AdminPageController@pageEditorDelete')->name('adminPageEditorDelete');

			Route::post('/content/editor/upload/image/{page}', 'AdminPageController@pageEditorUploadImage')->name('adminPageEditorUploadImage');

			// PAYMENT ROUTE
			// controler => AdminPaymentController
			// model => PaymentBundle
			// db => payment_bundle

			Route::get('/payment/bundle', 'AdminPaymentController@paymentBundleList')->name('adminPaymentBundleList');

			Route::post('/payment/bundle/table', 'AdminPaymentController@paymentBundleTable')->name('adminPaymentBundleTable');

			Route::get('/payment/bundle/create', 'AdminPaymentController@paymentBundleCreate')->name('adminPaymentBundleCreate');

			Route::post('/payment/bundle/store', 'AdminPaymentController@paymentBundleStore')->name('adminPaymentBundleStore');

			Route::get('/payment/bundle/edit/{payment?}', 'AdminPaymentController@paymentBundleEdit')->name('adminPaymentBundleEdit');

			Route::post('/payment/bundle/update/{payment}', 'AdminPaymentController@paymentBundleUpdate')->name('adminPaymentBundleUpdate');

			Route::post('/payment/bundle/delete/{payment}', 'AdminPaymentController@paymentBundleDelete')->name('adminPaymentBundleDelete');

			Route::post('/payment/bundle/rule/store', 'AdminPaymentController@paymentBundleRuleStore')->name('adminPaymentBundleRuleStore');

			Route::post('/payment/bundle/rule/delete', 'AdminPaymentController@paymentBundleRuleDelete')->name('adminPaymentBundleRuleDelete');
			
			// PAYMENT ROUTE
			// controler => AdminPaymentController
			// model => PaymentBundle
			// db => payment_bundle

			Route::get('/enquire', 'AdminPageController@enquireList')->name('adminEnquireList');

			Route::get('/enquire/{enquire}', 'AdminPageController@enquireSingle')->name('adminEnquireSingle');

		});		
	});
});

// Public routes
// Landing page, registeration, login and general users

Route::get('/', 'WelcomeController@index')->name('publicWelcomeIndex');

// Register

Route::get('/register', 'WelcomeController@register')->name('publicWelcomeRegister');

Route::post('/register/action', 'WelcomeController@registerAction')->name('publicWelcomeRegisterAction');

Route::get('/register/verify', 'WelcomeController@registerVerify')->name('publicWelcomeVerify');

// Login

Route::get('/login', 'WelcomeController@login')->name('publicWelcomeLogin');

Route::post('/login/action', 'WelcomeController@loginAction')->name('publicWelcomeLoginAction');

Route::get('/logout', 'WelcomeController@logout')->name('publicWelcomeLogout');

// Forgot and Reset

Route::get('/forgot', 'WelcomeController@forgot')->name('publicWelcomePasswordForgot');

Route::post('/forgot/action', 'WelcomeController@forgotAction')->name('publicWelcomePasswordForgotUpdate');

Route::get('/forgot/reset/{user}/{token}', 'WelcomeController@forgotReset')->name('publicWelcomePasswordForgotReset');

Route::post('/forgot/reset/action/{user}/{token}', 'WelcomeController@forgotResetAction')->name('publicWelcomePasswordForgotResetAction');

// Booking

Route::post('/enquire/send', 'ContentController@enquireEvent')->name('publicEnquireEvent');

Route::post('/send/message', 'ContentController@sendMessage')->name('publicWelcomeEmailSub');

Route::post('/purchase/gift', 'ContentController@purchaseGift')->name('publicPurchaseGift');