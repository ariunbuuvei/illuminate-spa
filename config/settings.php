<?php

return [
    'general' => [
        'login' => [
            'redirect' => 'publicWelcomeIndex',
        ],
        'logout' => [
            'redirect' => 'publicWelcomeIndex',
        ],
        'webAdmin' => 'unlavab@gmail.com',
    ],
    'target' => [
        'General' => 1,
        'Page' => 2,
        'User' => 4,
        'Settings' => 5,
    ],
    'status' => [
        'general' => [
            'New' => 1,
        ],
        'page' => [
            'Published' => 2,
            'Hidden' => 3,
        ],
        'user' => [
            'New' => 4,
            'Verified' => 5,
        ],
        'settings' => [
            'New' => 6,
        ],
        'logger' => [
            'Succesful' => 7,
            'Error' => 8,
        ],
    ],
    'type' => [
        'form' => [
            'Text' => 1,
            'Select' => 2,
            'Textarea' => 3,
            'Date' => 4,
            'Email' => 5,
            'Number' => 6,
        ],
        'user' => [
            'Regular' => 7,
            'Invited' => 8,
        ],
        'page' => [
            'Blog' => 9,
            'Section' => 10,
            'Event' => 20,
            'Client' => 21,
            'Member' => 22,
        ],
        'logger' => [
            'Create' => 11,
            'Read' => 12,
            'Update' => 13,
            'Delete' => 14,
        ],
        'file' => [
            'Thumbnail' => 15,
            "image" => 16
        ],
        'content' => [
            "Comment" => 17,
            "Rating" => 18,
            "Enquire" => 19,
            "Message" => 23,
        ],
    ],
    'categories' => [
        'page' => [
            'General' => 1,
        ],
    ],
];