//== Class definition
var Main = function() {

    //== Init
    var initMainScripts = function() {

        
    };

    return {
        //== Init
        init: function() {
            initMainScripts();
        },
    };
}();

//== Class Initialization
jQuery(document).ready(function() {
    Main.init();
});