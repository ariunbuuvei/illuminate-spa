var userScript = function () {

    var initUserScript = function () {
        var columns = [
            {
                field: 'id',
                title: 'id',
                type: 'number',
                sortable: false,
                width: 40,
                template: function (row, index, datatable) {
                    var rowNum = ( datatable.getPageSize() * ( datatable.getCurrentPage() - 1 ) ) + index + 1;
                    return rowNum;
                }
            },
            {
                field: 'title',
                title: 'Title',
                width: 200,
                template: function (row, index, datatable) {
                    return "<a href='" + getPageUrl + "/" + row['id'] + "'>"+ row['title'] +"</a>";
                }
            },
            {
                field: 'typeName',
                title: 'Type',
                template: function (row, index, datatable) {
                    return '<span>'+row['typeName']+'</span>';
                }
            },
            {
                field: 'category_id',
                title: 'Category',
                template: function (row, index, datatable) {
                    return '<span>'+row['categoryName']+'</span>';
                }
            },
            {
                field: '1',
                title: 'Status',
                template: function (row, index, datatable) {
                    return '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">'+row['statusName']+'</span>';
                }
            },
            {
                field: 'author_id',
                title: 'Author',
                template: function (row, index, datatable) {
                    return '<span>'+row['authorMail']+'</span>';
                }
            },
            {
                field: 'created_at',
                title: 'Created',
            }
        ];

        if (typeof getPagesListUrl != 'undefined') {
            var datatable = dashoMain.initDatatable(getPagesListUrl, $('#pageList'), columns);
        }
        
    };

    var initDropzone = function () {
        // single file upload
        Dropzone.autoDiscover = false;

        if(typeof fileUploadUrl != 'undefined') {

            var myDropzone = new Dropzone("#mainDropzoneElement", {
                url: fileUploadUrl,
                withCredentials: true,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                paramName: "file", // The name that will be used to transfer the file
                maxFiles: 10,
                maxFilesize: 10, // MB
                addRemoveLinks: true,
                accept: function(file, done) {
                    if (file.name == "justinbieber.jpg") {
                        done("Naha, you don't.");
                    } else { 
                        done(); 
                    }
                }  
            });

            if(typeof fileCollection != 'undefined')
            {
                fileCollection.forEach(function(fileObject) {
                    var mockFile = { name: fileObject['name'] };
                    myDropzone.options.addedfile.call(myDropzone, mockFile);
                    myDropzone.options.thumbnail.call(myDropzone, mockFile, fileObject['path']);
                });
            }

            $("#mainDropzoneElement").addClass('dropzone');
        }
    }

    var initSummernote = function () {
        $('.summernote').summernote({
            height: 400,
        });
    }

    return {
        init: function () {
            initUserScript();
            initDropzone();
            initSummernote();
        }
    };
}();


$(function () {
    userScript.init();
});