var userScript = function () {

    var initUserScript = function () {
        var columns = [
        	{
				field: 'id',
				title: 'id',
				type: 'number',
				sortable: false,
				width: 40,
				template: function (row, index, datatable) {
					var rowNum = ( datatable.getPageSize() * ( datatable.getCurrentPage() - 1 ) ) + index + 1;
					return rowNum;
                }
			},
			{
				field: 'question',
				title: 'Question',
				overflow: 'visible',
				width: 200,
                template: function (row, index, datatable) {
                    return "<a href='" + getQuizUrl + "/" + row['id'] + "'>"+ row['question'] +"</a>";
                }
			},
            {
                field: 'course_name.title',
                title: 'Course',
                overflow: 'visible',
            },
            {
                field: 'created_at',
                title: 'Created',
            }
        ];

        if (typeof getQuizListUrl != 'undefined') {
            var datatable = dashoMain.initDatatable(getQuizListUrl, $('#userList'), columns);
        }
    };

    return {
        init: function () {
            initUserScript();
        }
    };
}();

$(function () {
    userScript.init();
});