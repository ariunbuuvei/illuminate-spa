var userScript = function () {

    var initUserScript = function () {
        var columns = [
        	{
				field: 'id',
				title: 'id',
				type: 'number',
				sortable: false,
				width: 40,
				template: function (row, index, datatable) {
					var rowNum = ( datatable.getPageSize() * ( datatable.getCurrentPage() - 1 ) ) + index + 1;
					return rowNum;
                }
			},
			{
				field: 'email',
				title: 'Email',
				overflow: 'visible',
				width: 200,
                template: function (row, index, datatable) {
                    return '<a href="'+ getUserEditUrl +'/'+ row['id'] +'">'+ row['email'] +'</a>';
                }
			},
            {
                field: 'firstname',
                title: 'Name',
                template: function (row, index, datatable) {
                    return row['firstname'] + ' ' + row['lastname'];
                }
            },
            {
                field: 'status_name.name',
                title: 'Status',
                template: function (row, index, datatable) {
                    return '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Active</span>';
                }
            },
            {
                field: 'created_at',
                title: 'Created',
            }
        ];

        var datatable = dashoMain.initDatatable(getUserTableUrl, $('#userList'), columns);
    };

    return {
        init: function () {
            initUserScript();
        }
    };
}();

$(function () {
    userScript.init();
});