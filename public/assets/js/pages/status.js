var statusScript = function () {

    var initStatusScript= function () {
        var columns = [
        	{
				field: 'id',
				title: 'id',
				type: 'number',
				sortable: false,
				width: 40,
				template: function (row, index, datatable) {
					var rowNum = ( datatable.getPageSize() * ( datatable.getCurrentPage() - 1 ) ) + index + 1;
					return rowNum;
                }
			},
            {
                field: 'name',
                title: 'Name',
                template: function (row, index, datatable) {
                    return '<a href="'+ getStatusEditUrl +'/'+ row['id'] +'">'+ row['name'] +'</a>';
                }
            },
            {
                field: 'target',
                title: 'Target',
                template: function (row, index, datatable) {
                    switch(row['target']) {
                        case '1':
                            return '<span class="m-badge m-badge--info m-badge--wide">General</span>';
                        break;

                        case '2':
                            return '<span class="m-badge m-badge--accent m-badge--wide">Page</span>';
                        break;

                        case '4':
                            return '<span class="m-badge m-badge--primary m-badge--wide">User</span>';
                        break;

                        case '5':
                            return '<span class="m-badge m-badge--focus m-badge--wide">Settings</span>';
                        break;

                        default:
                            return '<span class="m-badge m-badge--metal m-badge--wide">None</span>';
                    }
                }
            },
            {
                field: 'color',
                title: 'Color',
                template: function (row, index, datatable) {
                    if (row['color']) {
                        return row['color'];
                    } else {
                        return '<i style="font-size: 12px;">null</i>';
                    }
                }
            },
            {
                field: 'css',
                title: 'Inline CSS',
                template: function (row, index, datatable) {
                    if (row['css']) {
                        return row['css'];
                    } else {
                        return '<i style="font-size: 12px;">null</i>';
                    }
                }
            },
            {
                field: 'deleted_at',
                title: 'Status',
                template: function (row, index, datatable) {
                    return '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Active</span>';
                }
            },
            {
                field: 'created_at',
                title: 'Created',
            },
        ];

        var datatable = dashoMain.initDatatable(getStatusTableUrl, $('#statusList'), columns);
    };

    return {
        init: function () {
            initStatusScript();
        }
    };
}();

$(function () {
    statusScript.init();
});