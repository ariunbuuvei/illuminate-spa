var courseScript = function () {

    var initCourseScript = function () {
        var columns = [
        	{
				field: 'id',
				title: 'id',
				type: 'number',
				sortable: false,
				width: 40,
				template: function (row, index, datatable) {
					var rowNum = ( datatable.getPageSize() * ( datatable.getCurrentPage() - 1 ) ) + index + 1;
					return rowNum;
                }
			},
			{
				field: 'title',
				title: 'Title',
				overflow: 'visible',
				width: 200,
                template: function (row, index, datatable) {
                    return "<a href='" + getCourseUrl + "/" + row['id'] + "'>"+ row['title'] +"</a>";
                }
			},
            {
                field: 'category_id',
                title: 'Category',
                overflow: 'visible',
                template: function (row, index, datatable) {
                    return 'With Quiz';
                }
            },
            {
                field: 'type_id',
                title: 'Type',
                overflow: 'visible',
                template: function (row, index, datatable) {
                    return 'Course';
                }
            },
            {
                field: 'status_id',
                title: 'Status',
                overflow: 'visible',
                template: function (row, index, datatable) {
                    return '<span class="m-badge m-badge--success m-badge--wide">Active</span>';
                }
            },
            {
                field: 'created_at',
                title: 'Created',
            }

        ];

        var datatable = dashoMain.initDatatable(getCourseListUrl, $('#courseList'), columns);
    };

    return {
        init: function () {
            initCourseScript();
        }
    };
}();

$(function () {
    courseScript.init();
});