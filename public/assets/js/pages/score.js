var scoreScript = function () {

    var initscoreScript = function () {
        var columns = [
            {
                field: 'id',
                title: 'id',
                type: 'number',
                sortable: false,
                width: 40,
                template: function (row, index, datatable) {
                    var rowNum = ( datatable.getPageSize() * ( datatable.getCurrentPage() - 1 ) ) + index + 1;
                    return rowNum;
                }
            },
            {
                field: 'user_name.email',
                title: 'User',
            },
            {
                field: 'course_name.title',
                title: 'Course',
            },
            {
                field: 'total_marks',
                title: 'Total',
            },
            {
                field: 'created_at',
                title: 'Created',
            }
        ];

        if (typeof getScoreListUrl != 'undefined') {
            var datatable = dashoMain.initDatatable(getScoreListUrl, $('#scoreList'), columns);
        }
    };

    return {
        init: function () {
            initscoreScript();
        }
    };
}();

$(function () {
    scoreScript.init();
});