var userScript = function () {

    var initUserScript = function () {
        var columns = [
            {
                field: 'id',
                title: 'id',
                type: 'number',
                sortable: false,
                width: 40,
                template: function (row, index, datatable) {
                    var rowNum = ( datatable.getPageSize() * ( datatable.getCurrentPage() - 1 ) ) + index + 1;
                    return rowNum;
                }
            },
            {
                field: 'name',
                title: 'Name',
                template: function (row, index, datatable) {
                    return '<a href="'+ getPaymentSingleUrl +'/'+ row['id'] +'">'+ row['name'] +'</a>';
                }
            },
            {
                field: 'amount',
                title: 'Amount',
            },
            {
                field: 'currency',
                title: 'Currency',
            },
            {
                field: 'frequency',
                title: 'Frequency',
            },
            {
                field: 'created_at',
                title: 'Created',
            }
        ];

        if (typeof getPaymentListUrl != 'undefined') {
            var datatable = dashoMain.initDatatable(getPaymentListUrl, $('#paymentList'), columns);
        }
        
    };

    var initSummernote = function () {
        $('.summernote').summernote({
            height: 200,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ]
        });
    }

    return {
        init: function () {
            initUserScript();
            initSummernote();
        }
    };
}();


$(function () {
    userScript.init();
    if (typeof getRuleAddUrl != 'undefined') {
        

        $("#rule_add").on("click", function() {
            $.ajax({
                url: getRuleAddUrl,
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                data: {
                    'count': $("#rule_count").val(),
                    'target': $("#rule_target").val(),
                    'paymentId': $("#payment_id").val(),
                },
                dataType: 'JSON',
                success: function (result) {
                    if (result.status) {
                        var target_name = $("#rule_target").find('option:selected').text();

                        $("#paymentRules").fadeIn();
                        $("#paymentRules").find("tbody").append("<tr><td>"+target_name+"</td><td>"+$("#rule_count").val()+"</td><td><a href='javascript:void(0)' ruleid='"+result.ruleId+"' class='deleteThisRule'><i class='fa fa-trash'></i></a></td></tr>");
                        
                        toastr.success(result.message);
                    } else {
                        toastr.error(result.message);
                    }
                }
            });
        });

        $("#paymentRules").on('click', '.deleteThisRule', function(){ 
            var rule = $(this).closest( "tr" );
            var ruleId = $(this).attr("ruleid");
            swal({
                title: "Confirmation",
                text: "Are you sure that you want to submit the form?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Confirm"
            }).then(function(e) {
                $.ajax({
                    url: getRuleDeleteUrl,
                    headers:
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: "POST",
                    data: {
                        'ruleId': ruleId,
                    },
                    dataType: 'JSON',
                    success: function (result) {
                        if (result.status) {
                            rule.remove();
                            toastr.success(result.message);
                        } else {
                            toastr.error(result.message);
                        }
                    }
                });
            });

            
        }); 
    }
});