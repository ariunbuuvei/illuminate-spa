var userScript = function () {

    var initUserScript = function () {
        var columns = [
            {
                field: 'id',
                title: 'id',
                type: 'number',
                sortable: false,
                width: 40,
                template: function (row, index, datatable) {
                    var rowNum = ( datatable.getPageSize() * ( datatable.getCurrentPage() - 1 ) ) + index + 1;
                    return rowNum;
                }
            },
            {
                field: 'title',
                title: 'Title',
                overflow: 'visible',
                width: 200,
                template: function (row, index, datatable) {
                    return "<a href='" + getPageUrl + "/" + row['id'] + "'>"+ row['title'] +"</a>";
                }
            },
            {
                field: '2',
                title: 'Category',
                template: function (row, index, datatable) {
                    return 'Landing page';
                }
            },
            {
                field: '1',
                title: 'Status',
                template: function (row, index, datatable) {
                    return '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Published</span>';
                }
            },
            {
                field: 'created_at',
                title: 'Created',
            }
        ];

        if (typeof getPagesListUrl != 'undefined') {
            var datatable = dashoMain.initDatatable(getPagesListUrl, $('#pageList'), columns);
        }
        
    };

    var initDropzone = function () {
        // single file upload
        Dropzone.autoDiscover = false;

        if(typeof fileUploadUrl != 'undefined') {

            var myDropzone = new Dropzone("#mainDropzoneElement", {
                url: fileUploadUrl,
                withCredentials: true,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                paramName: "file", // The name that will be used to transfer the file
                maxFiles: 1,
                maxFilesize: 5, // MB
                addRemoveLinks: true,
                accept: function(file, done) {
                    if (file.name == "justinbieber.jpg") {
                        done("Naha, you don't.");
                    } else { 
                        done(); 
                    }
                }  
            });

            $("#mainDropzoneElement").addClass('dropzone');

            myDropzone.on("addedfile", function(file) {
            /* Maybe display some more file information on your page */
            });
        }
    }

    var initSummernote = function () {
        $('.summernote').summernote({
            height: 200,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ]
        });
    }


    return {
        init: function () {
            initUserScript();
            initDropzone();
            initSummernote();
        }
    };
}();


$(function () {
    userScript.init();
});