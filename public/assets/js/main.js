/* global datatableTranslation, watchListUrl, blockWaitTxt, toastr, mApp */

var dashoMain = function () {
    var initCommonEvent = function () {
        initDatatableSearch();

        $(".askBeforeSend").on("click", function() {
            var targetForm = $(this).attr('rel');
            swal({
                title: "Confirmation",
                text: "Are you sure that you want to submit the form?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Confirm"
            }).then(function(e) {
                e.value && $(targetForm).submit()
            })
        });
    };

    var initDatatableSearch = function () {
        var $searchForm = $('#searchForm');
        $searchForm.find('#clearBtn').click(function () {
            clearForm($searchForm);
            $searchForm.find('.m-input').trigger('change');
        });
    };

    var initDatatableSearchEvent = function ($searchForm, datatable) {
        var query = {};
        $.each($searchForm.find('input, select'), function () {
            var $this = $(this);
            if ($this.val() !== '') {
                var val = $this.val().replace(/\s/g, '');
                query[$this.attr('name')] = val;
            }
        });
        datatable.setDataSourceQuery(query);
        datatable.reload();
    };

    var initDatatable = function (url, $table, columns, options, callback) {

        if ($table.length === 0) {
            return false;
        }

        var defaultConfig = {
            pageSize: 10
        };

        var params = query = {};

        if (typeof options !== 'undefined') {
            $.extend(defaultConfig, options);

            if (typeof options.params !== 'undefined') {
                params = options.params;
            }

            if (typeof options.query !== 'undefined') {
                query = options.query;
            }
        }

        var jsonData;

        var config = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        method: 'POST',
                        params: {
                            query: query,
                            params: params
                        },
                        url: url,
                        map: function (raw) {
                            jsonData = raw.data;
                            return jsonData;
                        }
                    }
                },
                pageSize: defaultConfig.pageSize,
                saveState: {
                    // save datatable state(pagination, filtering, sorting, etc) in cookie or browser webstorage
                    cookie: false,
                    webstorage: false,
                },
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },

            // layout definition
            layout: {
                scroll: true,
                footer: true,
                smoothScroll: {
                    scrollbarShown: true
                }
            },

            // column sorting
            sortable: true,

            // pagination
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 25, 50, 100, 200, 500, 1000]
                    }
                }
            },

            // search input
            search: {
                input: $('#generalSearch')
            },

            // columns definition
            columns: columns
        };

        if (typeof datatableTranslation !== "undefined") {
            $.extend(config, datatableTranslation);
        }

        var datatable = $table.mDatatable(config);

        if (typeof callback === 'function') {
            $table.on('m-datatable--on-ajax-done', function () {
                callback(jsonData);
            });
        }

        return datatable;
    };

    var initActiveMenu = function () {
        var $this = $('#m_ver_menu').find("a[href='" + $(location).attr("pathname") + "']");
        $parent = $this.parent();
        if ($parent.hasClass('ubex-submenu')) {
            $this.closest('.m-menu__item--submenu').addClass("m-menu__item--open");
        }
        $parent.addClass('m-menu__item--active');

    };

    var initSelect2 = function (target) {
        if ($().select2) {
            var $target = (typeof target === 'undefined') ? $(document.body) : target;
            var $targetElement = $target.find('select.select2');

            if ($targetElement.hasClass('remote')) {

                $targetElement.select2({
                    placeholder: 'Please select',
                    minimumInputLength: 3,
                    ajax: {
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        url: $targetElement.attr('url'),
                        type: 'post',
                        data: function (params) {
                            var queryParameters = {
                              autocomplete: params.term
                            }

                            return queryParameters;
                        },
                        dataType: 'json',
                        delay: 250,
                    },
                    dropdownAutoWidth: true,
                    width: '100%'
                });

            } else {
                $targetElement.select2({
                    dropdownAutoWidth: true,
                    width: '100%'
                });
            }
        }
    };

    var showMessage = function (type, text, title) {

        if (type === 'failed') {
            type = 'warning';
        }

        toastr.options = {
            "closeButton": true,
            "debug": true,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "showDuration": "2000",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        toastr[type](((typeof text !== 'undefined') ? text : ''), ((typeof title !== 'undefined') ? title : type));
    };

    var blockUI = function (options) {
        var message = blockWaitTxt;
        var target = 'body';

        if (typeof options !== 'undefined' && typeof options.message !== 'undefined') {
            message = options.message;
        }

        if (typeof options !== 'undefined' && typeof options.target !== 'undefined') {
            target = options.target;
        }

        mApp.block(target, {
            overlayColor: '#000000',
            type: 'loader',
            state: 'primary',
            message: message
        });
    };

    var handleNumberReplaceInitial = function () {
        $('body').on('keyup', '.amountInput', function () {
            this.value = this.value.replace(/[^0-9]/g, '');
        });
        $('body').on('keyup', '.mobileInput', function () {
            this.value = this.value.replace(/[^0-9\b+]/g, '');
        });
    };

    var ajax = function (url, config, successCallback, errorCallback, doneCallback) {

        var dataType = "JSON";
        var type = "POST";
        var data = {};

        if (typeof config !== 'undefined') {
            if (typeof config.data !== 'undefined') {
                data = config.data;
            }
            if (typeof config.type !== 'undefined') {
                type = config.type;
            }
            if (typeof config.type !== 'undefined') {
                type = config.type;
            }
        }

        $.ajax({
            url: url,
            data: data,
            dataType: dataType,
            type: type,
            beforeSend: function () {
                blockUI();
            },
            success: function (result) {
                if (typeof successCallback === 'function') {
                    successCallback(result);
                }
                if (typeof result.status !== 'undefined') {
                    dashoMain.showMessage(result.status, result.message);
                }
            },
            error: function (jqXHR, exception) {
                if (typeof errorCallback === 'function') {
                    errorCallback();
                }
            }
        }).done(function () {
            dashoMain.unblockUI();
            if (typeof doneCallback === 'function') {
                doneCallback();
            }
        });
    };

    var clearForm = function ($form) {
        $form.find('input, select, textarea').each(function () {
            if ($(this).is("input") || $(this).is("textarea")) {
                $(this).val("");
            } else if ($(this).is("select")) {
                $(this).val('');
            }
        });

        $form.find('input[type=checkbox]').parents("span").attr("class", "");
    };

    var initDatepicker = function ($el) {
        if ($().datepicker) {
            $el.datepicker({
                autoclose: true,
                todayBtn: "linked",
                clearBtn: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd',
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });

            $el.inputmask("yyyy-mm-dd", {
                autoUnmask: true
            });
        }
    };

    return {
        init: function () {
            initCommonEvent();
            initActiveMenu();
            handleNumberReplaceInitial();
            initSelect2();
            initDatepicker($('.initDate'))
        },
        initDatatable: function (url, $table, columns, options, callback) {
            return initDatatable(url, $table, columns, options, callback);
        },
        initDatatableSearchEvent: function ($searchForm, datatable) {
            initDatatableSearchEvent($searchForm, datatable);
        },
        showMessage: function (type, text, title) {
            showMessage(type, text, title);
        },
        initSelect2: function () {
            initSelect2();
        },
        blockUI: function (options) {
            blockUI(options);
        },
        unblockUI: function (target) {
            mApp.unblock(target);
        },
        ajax: function (url, config, successCallback, errorCallback, doneCallback) {
            ajax(url, config, successCallback, errorCallback, doneCallback);
        },
        redirect: function (url) {
            $(location).attr('href', url);
        },
        clearForm: function ($form) {
            clearForm($form);
        }
    };
}();

$(function () {
    dashoMain.init();
});