<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	DB::table('users')->insert([
            'name' => str_random(10),
            'email' => 'unlavab@gmail.com',
            'firstname' => 'Admin',
            'password' => bcrypt('erhemee0406'),
            'verify_code' => str_random(6),
            'is_admin' => 1,
            'created_at' => now(),
        ]);

        // STATUS SEEDER

            // GENERAL CONSTANT SEEDER

            DB::table('statuses')->insert([
                'id' => 1,
                'target' => 1,
                'name' => 'New',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            // PAGE CONSTANT SEEDER

            DB::table('statuses')->insert([
                'id' => 2,
                'target' => 2,
                'name' => 'Published',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('statuses')->insert([
                'id' => 3,
                'target' => 2,
                'name' => 'Hidden',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            // USER CONSTANT SEEDER

            DB::table('statuses')->insert([
                'id' => 4,
                'target' => 4,
                'name' => 'New',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('statuses')->insert([
                'id' => 5,
                'target' => 4,
                'name' => 'Verified',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            // SETTINGS CONSTANT SEEDER

            DB::table('statuses')->insert([
                'id' => 6,
                'target' => 5,
                'name' => 'New',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            // LOGGER CONSTANT SEEDER

            DB::table('statuses')->insert([
                'id' => 7,
                'target' => 1,
                'name' => 'Succesful',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('statuses')->insert([
                'id' => 8,
                'target' => 1,
                'name' => 'Error',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

        // TYPE SEEDER

            // FORM CONSTANT SEEDER

            DB::table('types')->insert([
                'id' => 1,
                'target' => 'form',
                'name' => 'Text',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('types')->insert([
                'id' => 2,
                'target' => 'form',
                'name' => 'Select',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('types')->insert([
                'id' => 3,
                'target' => 'form',
                'name' => 'Textarea',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('types')->insert([
                'id' => 4,
                'target' => 'form',
                'name' => 'Date',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('types')->insert([
                'id' => 5,
                'target' => 'form',
                'name' => 'Email',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('types')->insert([
                'id' => 6,
                'target' => 'form',
                'name' => 'Number',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            // USER CONSTANT SEEDER

            DB::table('types')->insert([
                'id' => 7,
                'target' => 'user',
                'name' => 'Regular',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('types')->insert([
                'id' => 8,
                'target' => 'user',
                'name' => 'Invited',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            // PAGE CONSTANT SEEDER

            DB::table('types')->insert([
                'id' => 9,
                'target' => 'page',
                'name' => 'Blog',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('types')->insert([
                'id' => 10,
                'target' => 'page',
                'name' => 'Section',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('types')->insert([
                'id' => 20,
                'target' => 'page',
                'name' => 'Event',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('types')->insert([
                'id' => 21,
                'target' => 'page',
                'name' => 'Client',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('types')->insert([
                'id' => 22,
                'target' => 'page',
                'name' => 'Member',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            // LOGGER CONSTANT SEEDER

            DB::table('types')->insert([
                'id' => 11,
                'target' => 'logger',
                'name' => 'Create',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('types')->insert([
                'id' => 12,
                'target' => 'logger',
                'name' => 'Read',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('types')->insert([
                'id' => 13,
                'target' => 'logger',
                'name' => 'Update',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('types')->insert([
                'id' => 14,
                'target' => 'logger',
                'name' => 'Delete',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            // FILE TYPES

            DB::table('types')->insert([
                'id' => 15,
                'target' => 'file',
                'name' => 'Thumbnail',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('types')->insert([
                'id' => 16,
                'target' => 'file',
                'name' => 'Image',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            // CONTENT TYPES

            DB::table('types')->insert([
                'id' => 17,
                'target' => 'content',
                'name' => 'Comment',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('types')->insert([
                'id' => 18,
                'target' => 'content',
                'name' => 'Rating',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('types')->insert([
                'id' => 19,
                'target' => 'content',
                'name' => 'Enquire',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

            DB::table('types')->insert([
                'id' => 23,
                'target' => 'content',
                'name' => 'Message',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

        // CATEGORY SEEDER

            // PAGE CONSTANT SEEDER

            DB::table('categories')->insert([
                'id' => 1,
                'target' => 2,
                'name' => 'General',
                'description' => '0x33f System generated seed',
                'created_at' => now(),
            ]);

    }
}
