<?php

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seoSettingsId = DB::table('settings')->insertGetId([
        	'target' => 2,
            'type' => 'form',
            'name' => 'SEO Meta Tags',
            'description' => 'SEO Meta Tags',
            'created_at' => now(),
        ]);

        DB::table('settings_attrs')->insert([
            'settings_id' => $seoSettingsId,
            'attr' => 'seo-title',
            'type' => 1,
            'placeholder' => 'Title',
            'created_at' => now(),
        ]);

        DB::table('settings_attrs')->insert([
            'settings_id' => $seoSettingsId,
            'attr' => 'seo-description',
            'type' => 1,
            'placeholder' => 'Description',
            'created_at' => now(),
        ]);

        DB::table('settings_attrs')->insert([
            'settings_id' => $seoSettingsId,
            'attr' => 'seo-keywords',
            'type' => 1,
            'placeholder' => 'Keywords',
            'created_at' => now(),
        ]);

        $generalSettingsId = DB::table('settings')->insertGetId([
            'target' => 1,
            'type' => 'general',
            'name' => 'General Settings',
            'description' => 'General Settings',
            'created_at' => now(),
        ]);

        DB::table('settings_attrs')->insert([
            'settings_id' => $generalSettingsId,
            'attr' => 'seo-description',
            'type' => 1,
            'placeholder' => 'Description',
            'created_at' => now(),
        ]);

        DB::table('settings_attrs')->insert([
            'settings_id' => $generalSettingsId,
            'attr' => 'seo-title',
            'type' => 1,
            'placeholder' => 'Title',
            'created_at' => now(),
        ]);

        DB::table('settings_attrs')->insert([
            'settings_id' => $generalSettingsId,
            'attr' => 'seo-keywords',
            'type' => 1,
            'placeholder' => 'Keywords',
            'created_at' => now(),
        ]);

        DB::table('settings_attrs')->insert([
            'settings_id' => $generalSettingsId,
            'attr' => 'general-address',
            'type' => 1,
            'placeholder' => 'Address',
            'created_at' => now(),
        ]);

        DB::table('settings_attrs')->insert([
            'settings_id' => $generalSettingsId,
            'attr' => 'general-phone',
            'type' => 1,
            'placeholder' => 'Phone',
            'created_at' => now(),
        ]);

        DB::table('settings_attrs')->insert([
            'settings_id' => $generalSettingsId,
            'attr' => 'general-mail',
            'type' => 1,
            'placeholder' => 'Email',
            'created_at' => now(),
        ]);

        $paymentSettingsId = DB::table('settings')->insertGetId([
            'target' => 1,
            'type' => 'payment',
            'name' => 'Payment Settings',
            'description' => 'Payment Settings',
            'created_at' => now(),
        ]);

        DB::table('settings_attrs')->insert([
            'settings_id' => $paymentSettingsId,
            'attr' => 'payment-stripe-key',
            'type' => 1,
            'placeholder' => 'Stripe Key',
            'created_at' => now(),
        ]);
    }
}
