<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaymentBundleRules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_bundle_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('payment_bundle_id')->nullable();
            $table->string('token');
            $table->string('count');
            $table->string('target');
            $table->string('target_id')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_bundle_rules');
    }
}
