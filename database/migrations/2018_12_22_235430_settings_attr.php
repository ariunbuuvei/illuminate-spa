<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SettingsAttr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings_attrs', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('settings_id');
            $table->string('attr');
            $table->string('type');
            $table->string('value')->nullable();
            $table->string('placeholder')->nullable();
            $table->string('path')->nullable();
            $table->boolean('required')->nullable();
            $table->string('description')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->string('orders')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings_attrs');
    }
}
