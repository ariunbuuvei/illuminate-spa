<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContentAttr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_attrs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('target_id');
            $table->string('target');
            $table->string('attr');
            $table->string('author')->nullable();
            $table->string('user')->nullable();
            $table->string('mobile')->nullable();
            $table->timestamp('content_date')->nullable();
            $table->timestamp('content_end_date')->nullable();
            $table->unsignedInteger('type_id')->nullable();
            $table->unsignedInteger('file_id')->nullable();
            $table->text('content')->nullable();
            $table->text('support_content')->nullable();
            $table->string('order')->nullable();
            $table->string('template')->nullable();
            $table->string('css_class')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_attrs');
    }
}
