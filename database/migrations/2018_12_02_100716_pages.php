<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->unsignedInteger('status_id')->nullable();
            $table->unsignedInteger('type_id')->nullable();
            $table->unsignedInteger('category_id')->nullable();
            $table->unsignedInteger('author_id')->nullable();
            $table->unsignedInteger('file_id')->nullable();
            $table->text('description')->nullable();
            $table->string('post_token')->nullable();
            $table->text('content');
            $table->string('order')->nullable();
            $table->string('template')->nullable();
            $table->string('css_class')->nullable();
            $table->integer('visit')->nullable()->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
