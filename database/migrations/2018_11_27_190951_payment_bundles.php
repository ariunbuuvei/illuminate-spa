<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaymentBundles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_bundles', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('token');
            $table->string('stripe_id')->collation('utf8mb4_bin')->nullable();
            $table->string('stripe_code')->nullable();
            $table->float('amount');
            $table->string('frequency')->nullable();
            $table->string('currency')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_bundles');
    }
}
