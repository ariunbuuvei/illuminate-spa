<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserLogger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_loggers', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->nullable();
            $table->string('session_ip')->nullable();
            $table->unsignedInteger('status_id');
            $table->unsignedInteger('type_id');
            $table->string('controller');
            $table->string('class');
            $table->integer('line');
            $table->integer('code');
            $table->text('message');
            $table->string('target')->nullable();
            $table->string('target_id')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_loggers');
    }
}
