-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 02, 2019 at 07:31 AM
-- Server version: 5.7.25-0ubuntu0.18.04.2
-- PHP Version: 7.2.15-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gigahire_cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `css` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `html_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `target`, `name`, `parent_id`, `color`, `css`, `html_class`, `position`, `deleted_at`, `description`, `created_at`, `updated_at`) VALUES
(1, '2', 'General', NULL, NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `content_attrs`
--

CREATE TABLE `content_attrs` (
  `id` int(10) UNSIGNED NOT NULL,
  `target_id` int(11) NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attr` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_date` timestamp NULL DEFAULT NULL,
  `content_end_date` timestamp NULL DEFAULT NULL,
  `type_id` int(10) UNSIGNED DEFAULT NULL,
  `file_id` int(10) UNSIGNED DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `support_content` text COLLATE utf8mb4_unicode_ci,
  `order` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `css_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `content_attrs`
--

INSERT INTO `content_attrs` (`id`, `target_id`, `target`, `attr`, `author`, `user`, `mobile`, `content_date`, `content_end_date`, `type_id`, `file_id`, `content`, `support_content`, `order`, `template`, `css_class`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'page', 'seo-title', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asd', NULL, NULL, NULL, NULL, NULL, '2019-02-10 01:32:52', '2019-02-10 01:32:52'),
(2, 1, 'page', 'seo-description', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asd', NULL, NULL, NULL, NULL, NULL, '2019-02-10 01:32:52', '2019-02-10 01:32:52'),
(3, 1, 'page', 'seo-keywords', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asd', NULL, NULL, NULL, NULL, NULL, '2019-02-10 01:32:52', '2019-02-10 01:32:52'),
(4, 1, '2', 'enquire', 'Admin', 'unlavab@gmail.com', '0495123456', '1992-04-05 14:00:00', NULL, 17, NULL, '31321312', NULL, NULL, NULL, NULL, NULL, '2019-02-10 03:29:10', '2019-02-10 03:29:10'),
(5, 2, 'page', 'seo-title', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asd', NULL, NULL, NULL, NULL, NULL, '2019-02-10 03:56:01', '2019-02-10 03:56:01'),
(6, 2, 'page', 'seo-description', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asd', NULL, NULL, NULL, NULL, NULL, '2019-02-10 03:56:01', '2019-02-10 03:56:01'),
(7, 2, 'page', 'seo-keywords', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asd', NULL, NULL, NULL, NULL, NULL, '2019-02-10 03:56:01', '2019-02-10 03:56:01'),
(8, 0, '1', 'message', 'Admin', 'unlavab@gmail.com', '499199014', NULL, NULL, 23, NULL, 'asdadadasd', NULL, NULL, NULL, NULL, NULL, '2019-02-10 04:38:48', '2019-02-10 04:38:48');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(10) UNSIGNED NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target_id` int(10) UNSIGNED DEFAULT NULL,
  `type_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `small_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `target`, `token`, `target_id`, `type_id`, `name`, `small_name`, `path`, `order`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '2', 'DWU3Z81JRTRW', 1, 15, 'b6efc649ddf39ea1ac2994311f3724313a2fa37a.png', 'b6efc649ddf39ea1ac2994311f3724313a2fa37aSt.png', 'Screenshot from 2019-01-30 19-29-17.png', NULL, NULL, '2019-02-10 01:32:38', '2019-02-10 01:32:52'),
(2, '2', 'DWU3Z81JRTRW', 1, 15, '741aa7c5b2a1ea8c50e6b51a07366d411414ebab.png', '741aa7c5b2a1ea8c50e6b51a07366d411414ebabKn.png', 'Screenshot from 2019-01-30 19-24-32.png', NULL, NULL, '2019-02-10 01:32:39', '2019-02-10 01:32:52'),
(3, '2', 'DWU3Z81JRTRW', 1, 15, '12064a87a59957002094459fe03ee82c09b932ff.png', '12064a87a59957002094459fe03ee82c09b932ffiA.png', 'Screenshot from 2019-01-21 15-46-38.png', NULL, NULL, '2019-02-10 01:32:39', '2019-02-10 01:32:52'),
(4, '2', 'JQHZTIS8YJ2J', 2, 15, 'f489b872afe95626dd88f4d0cdf86d2ee1d1f95a.png', 'f489b872afe95626dd88f4d0cdf86d2ee1d1f95aHS.png', 'Screenshot from 2019-01-18 03-25-17.png', NULL, NULL, '2019-02-10 03:55:54', '2019-02-10 03:56:01');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2018_11_24_054928_status', 1),
(3, '2018_11_24_054934_type', 1),
(4, '2018_11_24_055052_category', 1),
(5, '2018_11_24_082803_verify_token', 1),
(6, '2018_11_24_105529_subscriptions', 1),
(7, '2018_11_27_190951_payment_bundles', 1),
(8, '2018_12_02_100716_pages', 1),
(9, '2018_12_02_101548_content_attr', 1),
(10, '2018_12_02_164947_files', 1),
(11, '2018_12_22_235419_settings', 1),
(12, '2018_12_22_235430_settings_attr', 1),
(13, '2018_12_22_235441_settings_attr_options', 1),
(14, '2019_01_01_151716_payment_bundle_rules', 1),
(15, '2019_01_31_143823_user_logger', 1),
(16, '2019_01_31_144929_user_roles', 1),
(17, '2019_02_01_013717_notifications', 1),
(18, '2019_02_01_083909_notification_targets', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `target_id` int(10) UNSIGNED DEFAULT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notification_targets`
--

CREATE TABLE `notification_targets` (
  `id` int(10) UNSIGNED NOT NULL,
  `notification_id` int(10) UNSIGNED NOT NULL,
  `target_id` int(10) UNSIGNED DEFAULT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_id` int(10) UNSIGNED DEFAULT NULL,
  `type_id` int(10) UNSIGNED DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `author_id` int(10) UNSIGNED DEFAULT NULL,
  `file_id` int(10) UNSIGNED DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `post_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `css_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visit` int(11) DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `status_id`, `type_id`, `category_id`, `author_id`, `file_id`, `description`, `post_token`, `content`, `order`, `template`, `css_class`, `visit`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Tergel', 2, 20, 1, 1, 1, 'asdadas', 'DWU3Z81JRTRW', '<p>asdadas<br></p>', NULL, NULL, NULL, 0, NULL, '2019-02-10 01:32:52', '2019-02-10 03:26:16'),
(2, 'sdfsdfsdf', 2, 22, 1, 1, 4, 'asdasd', 'JQHZTIS8YJ2J', '<p>asdasdasd</p>', NULL, NULL, NULL, 2, NULL, '2019-02-10 03:56:01', '2019-02-10 04:45:57');

-- --------------------------------------------------------

--
-- Table structure for table `payment_bundles`
--

CREATE TABLE `payment_bundles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `stripe_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(8,2) NOT NULL,
  `frequency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_bundle_rules`
--

CREATE TABLE `payment_bundle_rules` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_bundle_id` bigint(20) DEFAULT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `count` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_id` int(10) UNSIGNED DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `target`, `target_id`, `type`, `name`, `status_id`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '2', NULL, 'form', 'SEO Meta Tags', NULL, 'SEO Meta Tags', NULL, '2019-02-09 19:31:07', NULL),
(2, '1', NULL, 'general', 'General Settings', NULL, 'General Settings', NULL, '2019-02-09 19:31:07', NULL),
(3, '1', NULL, 'payment', 'Payment Settings', NULL, 'Payment Settings', NULL, '2019-02-09 19:31:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings_attrs`
--

CREATE TABLE `settings_attrs` (
  `id` int(10) UNSIGNED NOT NULL,
  `settings_id` bigint(20) NOT NULL,
  `attr` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `placeholder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `required` tinyint(1) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `orders` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings_attrs`
--

INSERT INTO `settings_attrs` (`id`, `settings_id`, `attr`, `type`, `value`, `placeholder`, `path`, `required`, `description`, `deleted_at`, `orders`, `created_at`, `updated_at`) VALUES
(1, 1, 'seo-title', '1', NULL, 'Title', NULL, NULL, NULL, NULL, NULL, '2019-02-09 19:31:07', NULL),
(2, 1, 'seo-description', '1', NULL, 'Description', NULL, NULL, NULL, NULL, NULL, '2019-02-09 19:31:07', NULL),
(3, 1, 'seo-keywords', '1', NULL, 'Keywords', NULL, NULL, NULL, NULL, NULL, '2019-02-09 19:31:07', NULL),
(4, 2, 'seo-description', '1', 'description', 'Description', NULL, NULL, NULL, NULL, NULL, '2019-02-09 19:31:07', '2019-02-09 19:57:10'),
(5, 2, 'seo-title', '1', 'title', 'Title', NULL, NULL, NULL, NULL, NULL, '2019-02-09 19:31:07', '2019-02-09 19:57:10'),
(6, 2, 'seo-keywords', '1', 'asd, asdasd, asda', 'Keywords', NULL, NULL, NULL, NULL, NULL, '2019-02-09 19:31:07', '2019-02-09 19:57:10'),
(7, 2, 'general-address', '1', '100 Harris, NSW Sydney', 'Address', NULL, NULL, NULL, NULL, NULL, '2019-02-09 19:31:07', '2019-02-09 19:57:10'),
(8, 2, 'general-phone', '1', '1234567890', 'Phone', NULL, NULL, NULL, NULL, NULL, '2019-02-09 19:31:07', '2019-02-09 19:57:10'),
(9, 2, 'general-mail', '1', 'info@gigahire.com.au', 'Email', NULL, NULL, NULL, NULL, NULL, '2019-02-09 19:31:07', '2019-02-09 19:57:10'),
(10, 3, 'payment-stripe-key', '1', NULL, 'Stripe Key', NULL, NULL, NULL, NULL, NULL, '2019-02-09 19:31:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings_attr_options`
--

CREATE TABLE `settings_attr_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `settings_attr_id` bigint(20) NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `css` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `html_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `target`, `name`, `color`, `css`, `html_class`, `position`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '1', 'New', NULL, NULL, NULL, NULL, '0x33f System generated seed', NULL, '2019-02-09 19:31:06', NULL),
(2, '2', 'Published', NULL, NULL, NULL, NULL, '0x33f System generated seed', NULL, '2019-02-09 19:31:06', NULL),
(3, '2', 'Hidden', NULL, NULL, NULL, NULL, '0x33f System generated seed', NULL, '2019-02-09 19:31:06', NULL),
(4, '4', 'New', NULL, NULL, NULL, NULL, '0x33f System generated seed', NULL, '2019-02-09 19:31:06', NULL),
(5, '4', 'Verified', NULL, NULL, NULL, NULL, '0x33f System generated seed', NULL, '2019-02-09 19:31:06', NULL),
(6, '5', 'New', NULL, NULL, NULL, NULL, '0x33f System generated seed', NULL, '2019-02-09 19:31:06', NULL),
(7, '1', 'Succesful', NULL, NULL, NULL, NULL, '0x33f System generated seed', NULL, '2019-02-09 19:31:06', NULL),
(8, '1', 'Error', NULL, NULL, NULL, NULL, '0x33f System generated seed', NULL, '2019-02-09 19:31:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `stripe_plan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `id` int(10) UNSIGNED NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `css` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `html_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `target`, `name`, `color`, `css`, `html_class`, `position`, `deleted_at`, `description`, `created_at`, `updated_at`) VALUES
(1, 'form', 'Text', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:06', NULL),
(2, 'form', 'Select', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:06', NULL),
(3, 'form', 'Textarea', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:06', NULL),
(4, 'form', 'Date', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:06', NULL),
(5, 'form', 'Email', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:06', NULL),
(6, 'form', 'Number', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:06', NULL),
(7, 'user', 'Regular', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:07', NULL),
(8, 'user', 'Invited', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:07', NULL),
(9, 'page', 'Blog', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:07', NULL),
(10, 'page', 'Section', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:07', NULL),
(11, 'logger', 'Create', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:07', NULL),
(12, 'logger', 'Read', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:07', NULL),
(13, 'logger', 'Update', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:07', NULL),
(14, 'logger', 'Delete', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:07', NULL),
(15, 'file', 'Thumbnail', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:07', NULL),
(16, 'file', 'Image', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:07', NULL),
(17, 'content', 'Comment', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:07', NULL),
(18, 'content', 'Rating', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:07', NULL),
(19, 'content', 'Enquire', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:07', NULL),
(20, 'page', 'Event', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:07', NULL),
(21, 'page', 'Client', NULL, NULL, NULL, NULL, NULL, '0x33f System generated seed', '2019-02-09 19:31:07', NULL),
(22, 'page', 'Member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middlename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `birthday` date DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `verify_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `status_id` int(10) UNSIGNED DEFAULT NULL,
  `type_id` int(10) UNSIGNED DEFAULT NULL,
  `is_admin` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `firstname`, `middlename`, `lastname`, `mobile`, `address`, `birthday`, `email_verified_at`, `verify_code`, `category_id`, `status_id`, `type_id`, `is_admin`, `role_id`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'M8Ni8I1oj1', 'unlavab@gmail.com', '$2y$10$XRCjKq0jmXHOaHNDqSE4Y.O2JcRHbzYTzwb0pXrvXXtQsuYuP8zEq', 'Admin', NULL, NULL, NULL, NULL, NULL, NULL, 'DyCIqS', NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-02-09 19:31:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_loggers`
--

CREATE TABLE `user_loggers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `session_ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `line` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `verify_tokens`
--

CREATE TABLE `verify_tokens` (
  `id` int(10) UNSIGNED NOT NULL,
  `target_id` int(11) DEFAULT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expire_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_attrs`
--
ALTER TABLE `content_attrs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_targets`
--
ALTER TABLE `notification_targets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_bundles`
--
ALTER TABLE `payment_bundles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_bundle_rules`
--
ALTER TABLE `payment_bundle_rules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_attrs`
--
ALTER TABLE `settings_attrs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_attr_options`
--
ALTER TABLE `settings_attr_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_loggers`
--
ALTER TABLE `user_loggers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `verify_tokens`
--
ALTER TABLE `verify_tokens`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `content_attrs`
--
ALTER TABLE `content_attrs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notification_targets`
--
ALTER TABLE `notification_targets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `payment_bundles`
--
ALTER TABLE `payment_bundles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_bundle_rules`
--
ALTER TABLE `payment_bundle_rules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `settings_attrs`
--
ALTER TABLE `settings_attrs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `settings_attr_options`
--
ALTER TABLE `settings_attr_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_loggers`
--
ALTER TABLE `user_loggers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `verify_tokens`
--
ALTER TABLE `verify_tokens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
