@extends('layouts.mail')

@section('content')

# {{ $title }}

{{ $body }}

@endsection

@section('table')
	@if ($table)
		<table>
			<thead>
				<tr>
					<th colspan="2">{{ $table['head'] }}</th>
				</tr>
			</thead>
			<tbody>
				@foreach($table['body'] as $rows)
					<tr>
						@foreach($rows as $columns)
							<td>{{ $columns }}</td>
						@endforeach
					</tr>
				@endforeach
			</tbody>
		</table>
	@endif
@endsection