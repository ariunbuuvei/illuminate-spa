@component('mail::layout')

	@slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{ config('app.name') }}
        @endcomponent
    @endslot
    
    @yield('content')

    @slot('table')
        @component('mail::table')
            @yield('table')
        @endcomponent
    @endslot

    @slot('subcopy')
        @component('mail::subcopy')
            You're receiving this email because you recently completed a purchase with us. The prices, product status and availability of products above are correct as of the time this email was generated and subject to change.
        @endcomponent
    @endslot

	@slot('footer')
        @component('mail::footer')
            9997 Rose Hills Road, Whittier, CA 90601 USA
        @endcomponent
    @endslot

@endcomponent