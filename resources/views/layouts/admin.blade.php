<!DOCTYPE html>
<html lang="en" >
    <!-- begin::Head -->
    <head>
        <meta charset="utf-8" />
        <title>
            Dasho Partners CMS
        </title>
        <meta name="description" content="First beta look for FionaPay">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
            WebFont.load({
                google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
                active: function() {
                    sessionStorage.fonts = true;
                }
            });
        </script>
        <!--end::Web font -->

        <!--begin::Base Styles -->  
        <link href="{{ URL::asset('assets/css/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
        <!--end::Base Styles -->
        <link rel="shortcut icon" href="{{ URL::asset('assets/media/dasho_box_logo.png') }}" />

        @yield('stylesheet')
    </head>
    <!-- end::Head -->
    
    <!-- begin::Body -->
    <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <!-- BEGIN: Header -->
            @include('import.admin-header')
            <!-- END: Header -->      

            <!-- begin::Body -->
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

                <!-- BEGIN: Left Aside -->
                @include('import.admin-menu')
                <!-- END: Left Aside -->

                <!-- BEGIN: Content -->
                @yield('content')
                <!-- END: Content --> 
            </div>
            <!-- end:: Body -->


            <!-- begin::Footer -->
            @include('import.admin-footer')
            <!-- end::Footer -->
        </div>
        <!-- end:: Page -->

        <!-- begin::Scroll Top -->
        <div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
            <i class="la la-arrow-up"></i>
        </div>
        <!-- end::Scroll Top -->

        <!--begin::Base Scripts -->
        <script src="{{ URL::asset('assets/js/vendors.bundle.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('assets/js/main.js') }}" type="text/javascript"></script>
        <script type="text/javascript">
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            
            @if(session()->has('success'))
                toastr.success("{{ session()->get('success') }}");
            @endif

            @if($errors->any())
                @foreach ($errors->all() as $error)
                    toastr.error("{{ $error }}");
                @endforeach
            @endif
            
        </script>

        @yield('javascript')
        <!--end::Base Scripts -->

    </body>
    <!-- end::Body -->
</html>
