<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @include('pages.includes.seo')

        <link href="{{ URL::asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">

        @yield('stylesheet')
        
    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-md-4 offset-md-4">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if (session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                </div>                
            </div>

            <!-- BEGIN: Content -->
            @yield('content')
            <!-- END: Content --> 
        </div>

    	<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script> -->
        <script src="{{ URL::asset('assets/js/jquery-3.3.1.slim.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('assets/js/popper.min.js') }}" type="text/javascript"></script>
    	<script src="{{ URL::asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
        @yield('javascript')
    </body>
</html>
