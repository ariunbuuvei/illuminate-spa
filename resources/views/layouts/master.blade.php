<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    @include('pages.includes.seo')

    <link href='https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,200,300' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="{{ asset('template/css/master.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/noty/lib/noty.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>
<div id="overlay"></div>
<div id="mobile-menu">
  <ul>

    <li class="page-scroll"><a href="#services">Home</a></li>
    <li class="page-scroll"><a href="#services">Service Menu</a></li>

    <li class="page-scroll"><a href="#contact">Contact</a></li>
    <li class="page-scroll"><a href="#appoinment">Book Private Here</a></li>
  </ul>
</div>

<div id="page">

<header id="pagetop">
    <nav>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="logo page-scroll"><a href="#pagetop">NU Illumiate Salon</a></div>
            <div class="mm-toggle-wrap">
              <div class="mm-toggle"> <i class="icon-menu"><img src="{{ asset('template/images/menu-icon.png') }}" alt="Menu"></i></div>
            </div>
            <ul class="menu">

                <li class="page-scroll"><a href="#services">Home</a></li>
                <li class="page-scroll"><a href="#services">Service Menu</a></li>
     
                <li class="page-scroll"><a href="#contact">Contact</a></li>
                <li class="page-scroll"><a href="#appoinment">Book Private Here</a></li>
            </ul>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </nav>
</header>

@yield('content')

<section id="contact" class="wow fadeInUp">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h2>Contact</h2>
        </div>
        <div class="col-sm-12 col-md-8 col-lg-8">
          <div class="contactmap">
            <div class="mapcont">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d47513.096656816!2d-87.79662866759004!3d41.90213526379646!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x25ed9871a5cced!2sSalon+Lofts+Wicker+Park!5e0!3m2!1sen!2sau!4v1555907139888!5m2!1sen!2sau" style="border:0"></iframe>
            </div>
            <div class="social">
              <p>1638 W DIVISON ST, Salon Lofts, Loft #4, Chicago IL 60622</p>
              <span>Phone - +1 312-536-9353 </span> <span>Email - <a href="mailto:info@websitec.com">info@illuminate-spa.com</a></span>
              <div class="social-icon"> 
                <a href="#" class="facebook"></a>
                <a href="#" class="twitter"></a>
                <a href="#" class="google"></a>
                <a href="#" class="youtube"></a>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 pull-right">
            <div id="ContactSuccessMessage"></div>
            <div id="ContactErrorMessage"></div>
          <form name="ContactForm" id="ContactForm" method="post">
            <div class="textarea pull-left">
              <input type="text" class="form-control required" name="ContactFullName" id="ContactFullName" placeholder="Name" >
            </div>
            <div class="form-group pull-left">
              <input type="email" class="form-control required email" name="ContactEmail" id="ContactEmail" placeholder="Email" >
            </div>
            <div class="form-group pull-left marright0">
              <input type="text" class="form-control required number" name="ContactNumber" id="ContactNumber" placeholder="Conatct Number"  >
            </div>
            <div class="textarea pull-left">
              <textarea placeholder="Description" name="ContactDescription" id="ContactDescription" class="form-control"></textarea>
            </div>
            <button type="submit" class="btn btn-default" value="Submit">SUBMIT</button>
          </form>
          <div class="coypright">
            <p>&copy; NU Illumiate Salon {{ now()->format('Y') }}</p>
          </div>
        </div>
      </div>
    </div>
</section>

<a href="#" class="scrollup">Top</a>
</div>
</body>


<script src="{{ asset('template/js/jquery.1.11.2.js') }}"></script>
<script src="{{ asset('template/js/bootstrap.js') }}"></script>
<script src="{{ asset('template/js/function.js') }}"></script>
<script src="{{ asset('template/js/bootstrap-datepicker.js') }}"></script> 
<script src="{{ asset('template/js/parallax.js') }}"></script> 
<script src="{{ asset('template/js/scorll.js') }}"></script> 
<script src="{{ asset('template/js/jquery.easing.min.js') }}"></script> 
<script src="{{ asset('template/js/slick.js') }}"></script> 
<script src="{{ asset('template/js/menu.js') }}"></script> 
<script src="{{ asset('template/js/ios-timer.js') }}"></script> 
<script src="{{ asset('template/js/jquery.fencybox.js') }}"></script> 
<script src="{{ asset('template/js/jquery.portfolio.js') }}"></script> 
<script src="{{ asset('template/js/jquery.mousewheel-3.0.6.pack.js') }}"></script>
<script src="{{ asset('template/js/wow.js') }}"></script>
<script src="{{ asset('template/js/jquery.validate.js') }}"></script>
<!-- REVOLUTION JS FILES --> 
<script type="text/javascript" src="{{ asset('template/js/revoluation/jquery.themepunch.tools.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('template/js/revoluation/jquery.themepunch.revolution.min.js') }}"></script> 
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
(Load Extensions only on Local File Systems ! 
The following part can be removed on Server for On Demand Loading) --> 
<script type="text/javascript" src="{{ asset('template/js/revoluation/revolution.extension.layeranimation.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('template/js/revoluation/revolution.extension.migration.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('template/js/revoluation/revolution.extension.navigation.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('template/js/revoluation/revolution.extension.parallax.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('template/js/revoluation/revolution.extension.slideanims.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('template/js/revoluation/revoluationfunction.js') }}"></script> 
<script type="text/javascript" src="{{ asset('template/noty/lib/noty.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<script type="text/javascript">
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                new Noty({
                    theme: 'metroui',
                    type: 'error',
                    layout: 'topCenter',
                    text: '{{ $error }}'
                }).show();
            @endforeach
        @endif

        @if (session()->has('success'))
            new Noty({
                theme: 'metroui',
                type: 'success',
                layout: 'topCenter',
                text: '{{ session()->get('success') }}'
            }).show();
        @endif
        
</script>
@yield('javascript')

</html>