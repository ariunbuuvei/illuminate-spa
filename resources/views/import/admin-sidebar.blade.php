<div class="sidebar_header">
    Үндсэн цэс
</div>
    
<ul class="sideNav">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> Нүүр хуудас</a></li>
    <li class="hasSub">
        <a href="#"><i class="fa fa-graduation-cap"></i> Сургалт <i class="fa fa-angle-right rotate"></i></a>
        <ul>
            <li><a href="{{ route('listClass') }}">Жагсаалт</a></li>
            <li><a href="{{ route('createClass') }}">Шинээр нэмэх</a></li>
        </ul>
    </li>

    @if (!empty($thisClass))

    <li><a href="{{ route('viewSchedule', [$thisClass->id]) }}"><i class="fa fa-calendar-check-o"></i> Хуваарь</a></li>
    <li><a href="{{ route('viewPerson', [$thisClass->id]) }}"><i class="fa fa-group"></i> Оюутан, сурагчид</a></li>

    @endif

    <!-- <li class="hasSub">
        <a href="#"><i class="fa fa-flask"></i> Багш нар <i class="fa fa-angle-right rotate"></i></a>
        <ul>
            <li><a href="#">Dink</a></li>
            <li><a href="#">Kink</a></li>
        </ul>
    </li>

    <li><a href="#"><i class="fa fa-flask"></i> Сурагчид</a></li>
    <li><a href="#"><i class="fa fa-flask"></i> Хичээлийн хуваарь</a></li>
    <li><a href="#"><i class="fa fa-flask"></i> Дүн бүртгэл</a></li> -->
</ul>