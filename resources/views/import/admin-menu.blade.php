<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
    <i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-dark ">
    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark" data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            
            <li class="m-menu__item" aria-haspopup="true" >
                <a  href="{{ route('adminWelcomeIndex') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-line-graph"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Dashboard
                            </span>
                        </span>
                    </span>
                </a>
            </li>

            <li class="m-menu__item" aria-haspopup="true" >
                <a  href="{{ route('adminStatusList') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-interface-9"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Statuses
                            </span>
                        </span>
                    </span>
                </a>
            </li>

            <li class="m-menu__item" aria-haspopup="true" >
                <a  href="{{ route('adminCategoryList') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-list-1"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Categories
                            </span>
                        </span>
                    </span>
                </a>
            </li>

            <li class="m-menu__item" aria-haspopup="true" >
                <a  href="{{ route('adminSettingsList') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-file-1"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Custom form
                            </span>
                        </span>
                    </span>
                </a>
            </li>

            <li class="m-menu__item" aria-haspopup="true" >
                <a  href="{{ route('adminUserIndex') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-users"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Users
                            </span>
                        </span>
                    </span>
                </a>
            </li>

            <li class="m-menu__item" aria-haspopup="true" >
                <a  href="{{ route('adminPageList') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-open-box"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Content
                            </span>
                        </span>
                    </span>
                </a>
            </li>

            <li class="m-menu__item" aria-haspopup="true" >
                <a  href="{{ route('adminEnquireList') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-speech-bubble"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Enquiry
                            </span>
                        </span>
                    </span>
                </a>
            </li>

            <!-- <li class="m-menu__item" aria-haspopup="true" >
                <a  href="{{ route('adminPaymentBundleList') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-gift"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Payment Bundle
                            </span>
                        </span>
                    </span>
                </a>
            </li> -->

            <!-- <li class="m-menu__item" aria-haspopup="true" >
                <a  href="#" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-confetti"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Subscription
                            </span>
                        </span>
                    </span>
                </a>
            </li> -->

            <li class="m-menu__item" aria-haspopup="true" >
                <a  href="{{ route('adminSettingsGeneral') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-settings"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Settings
                            </span>
                        </span>
                    </span>
                </a>
            </li>

            <!-- <li class="m-menu__item" aria-haspopup="true" >
                <a  href="#" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-map-location"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                User Tracker
                            </span>
                        </span>
                    </span>
                </a>
            </li> -->

        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>