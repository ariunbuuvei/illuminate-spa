
@extends('template.layouts.master')

@section('stylesheet')
@endsection

@section('content')

<div class="container margin_60_35">
			<div class="box_booking">

				<div class="strip_booking">
					<div class="row">
						<div class="col-lg-2 col-md-2">
							<div class="date">
								<span class="month">Dec</span>
								<span class="day"><strong>28</strong>Fri</span>
							</div>
						</div>
						<div class="col-lg-6 col-md-5">
							<h3 class="bars_booking">Mojto Bar<span>2 Adults</span></h3>
						</div>
						<div class="col-lg-2 col-md-3">
							<ul class="info_booking">
								<li><strong>Booking id</strong> 23442</li>
								<li><strong>Booked on</strong> Sat. 20 Dec. 2018</li>
							</ul>
						</div>
						<div class="col-lg-2 col-md-2">
							<div class="booking_buttons">
								<a href="{{ route('publicPageBookInvoice') }}" target="_blank" class="btn_2">Invoice</a>
								<a href="#0" class="btn_3">Cancel</a>
							</div>
						</div>
					</div>
					<!-- /row -->
				</div>
				<!-- /strip booking -->

			</div>
		</div>
<!-- /container -->


@endsection

@section('javascript')
@endsection