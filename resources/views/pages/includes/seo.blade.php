@isset($pageHeader['seoObjects'])
    @php ( $settingsList = $pageHeader['seoObjects'] )
@else
    @php ( $settingsList = \App\SettingsAttr::where('settings_id', 2)->get()->toArray() )
@endif


<title>Illuminate SPA</title>
<meta property="og:url" content="{{ url()->current() }}" />
<meta property="og:site_name" content="{{ config('app.name') }}" />