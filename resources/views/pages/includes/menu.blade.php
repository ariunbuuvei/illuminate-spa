
<ul id="top_menu">
    <li><a href="{{ route('publicPagePricing') }}" class="btn_add">Enquire now</a></li>
    @if (Auth::user())
            <li>
                <a href="{{ route('publicWelcomeLogout') }}" class="login">Logout</a>
            </li>
        @if (Auth::user()->is_admin)
            <li>
                <a href="{{ route('adminWelcomeIndex') }}" target="_blank" class="btn_add">Admin</a>
            </li>
        @endif
    @else
        <li><a href="#sign-in-dialog" id="sign-in" class="login" title="Sign In">Sign In</a></li>
    @endif
</ul>
<!-- /top_menu -->
<a href="#menu" class="btn_mobile">
    <div class="hamburger hamburger--spin" id="hamburger">
        <div class="hamburger-box">
            <div class="hamburger-inner"></div>
        </div>
    </div>
</a>
<nav id="menu" class="main-menu">
    <ul>
        <li><span><a href="{{ route('publicWelcomeIndex') }}">Home</a></span>
        </li>
        <li><span><a href="#0">Events</a></span>
            @php ( $eventList = \App\Page::where('type_id', Config::get('settings.type.page.Event'))->get() )

            <ul>
            @foreach($eventList as $event)
                <li><a href="{{ route('publicPageSingleEvent', ['event' => str_replace(' ', '-', $event->title) ]) }}">{{ $event->title }}</a></li>
            @endforeach
            </ul>
        </li>
        <li><span><a href="{{ route('publicPageTeam') }}">Team</a></span></li>
        <li><span><a href="{{ route('publicPageReview') }}">Reviews</a></span></li>
        <li><span><a href="{{ route('publicPagePricing') }}">Pricing</a></span></li>
        <li><span><a href="{{ route('publicPageBlogList') }}">Blog</a></span></li>
        <li><span><a href="{{ route('publicPageContact') }}">Contact us</a></span></li>
    </ul>
</nav>