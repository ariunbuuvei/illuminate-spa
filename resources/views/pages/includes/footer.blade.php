<footer>
    <div class="container margin_60_35">
        <div class="row">

            <div class="col-lg-3 col-md-6 col-sm-6">
                <a data-toggle="collapse" data-target="#collapse_ft_1" aria-expanded="false" aria-controls="collapse_ft_1" class="collapse_bt_mobile">
                    <h3>Events</h3>
                    <div class="circle-plus closed">
                        <div class="horizontal"></div>
                        <div class="vertical"></div>
                    </div>
                </a>
                <div class="collapse show" id="collapse_ft_1">
                    <ul class="links">

                        @php ( $eventList = \App\Page::where('type_id', Config::get('settings.type.page.Event'))->get() )
                        
                        @foreach($eventList as $event)
                            <li>
                                <a href="{{ route('publicPageSingleEvent', ['event' => str_replace(' ', '-', $event->title) ]) }}">
                                    {{ $event->title }}
                                </a>
                            </li>
                        @endforeach

                    </ul>
                </div>
            </div>

            <!-- <div class="col-lg-4 col-md-6 col-sm-6">
                <a data-toggle="collapse" data-target="#collapse_ft_2" aria-expanded="false" aria-controls="collapse_ft_2" class="collapse_bt_mobile">
                    <h3>Services</h3>
                    <div class="circle-plus closed">
                        <div class="horizontal"></div>
                        <div class="vertical"></div>
                    </div>
                </a>
                <div class="collapse show" id="collapse_ft_2">
                    <ul class="links">
                        <li><a href="#0">Photo booth</a></li>
                        <li><a href="#0">Service</a></li>
                    </ul>
                </div>
            </div> -->

            <div class="col-lg-4 col-md-6 col-sm-6">
                <a data-toggle="collapse" data-target="#collapse_ft_3" aria-expanded="false" aria-controls="collapse_ft_3" class="collapse_bt_mobile">
                    <h3>Contacts</h3>
                    <div class="circle-plus closed">
                        <div class="horizontal"></div>
                        <div class="vertical"></div>
                    </div>
                </a>
                <div class="collapse show" id="collapse_ft_3">
                    <ul class="contacts">
                        @foreach(\App\SettingsAttr::where('settings_id', 2)->get() as $footerlink)
                            @if ( in_array('general', explode('-', $footerlink->attr)) )
                                <li>
                                @if ( in_array('address', explode('-', $footerlink->attr)) )
                                    <i class="ti-home"></i>
                                @endif

                                @if ( in_array('phone', explode('-', $footerlink->attr)) )
                                    <i class="ti-headphone-alt"></i>
                                @endif

                                @if ( in_array('mail', explode('-', $footerlink->attr)) )
                                    <i class="ti-email"></i>
                                @endif
                                
                                {!! $footerlink->value !!}
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="col-lg-5 col-md-6 col-sm-6">
                <a data-toggle="collapse" data-target="#collapse_ft_4" aria-expanded="false" aria-controls="collapse_ft_4" class="collapse_bt_mobile">
                    <div class="circle-plus closed">
                        <div class="horizontal"></div>
                        <div class="vertical"></div>
                    </div>
                    <h3>Keep in touch</h3>
                </a>
                <div class="collapse show" id="collapse_ft_4">
                    <div id="newsletter">
                        <div id="message-newsletter"></div>

                        {!! Form::open([
                            'method' => 'POST', 
                            'url' => route('publicWelcomeEmailSub'), 
                            'id' => 'newsletter_form', 
                            'name' => 'newsletter_form'
                        ]) !!}
                            <div class="form-group">
                                <input type="email" name="submail" id="submail" class="form-control" placeholder="Your email">
                                <input type="submit" value="Submit" id="submit-newsletter">
                            </div>
                        {!! Form::close() !!}

                    </div>
                    <div class="follow_us">
                        <h5>Follow Us</h5>
                        <ul>
                            <li><a href="#0"><i class="ti-facebook"></i></a></li>
                            <li><a href="#0"><i class="ti-twitter-alt"></i></a></li>
                            <li><a href="#0"><i class="ti-google"></i></a></li>
                            <li><a href="#0"><i class="ti-pinterest"></i></a></li>
                            <li><a href="#0"><i class="ti-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /row-->
        <hr>
        <div class="row">
            <div class="col-lg-6">
                <ul id="footer-selector">
                    <li><img src="{{ URL::asset('template/img/cards_all.svg') }}" alt=""></li>
                </ul>
            </div>
            <div class="col-lg-6">
                <ul id="additional_links">
                    <li><a href="#0">Terms and conditions</a></li>
                    <li><a href="#0">Privacy</a></li>
                    <li><span>© {{ now()->format('Y') }} {{ config('app.name') }}</span></li>
                </ul>
            </div>
        </div>
    </div>
</footer>