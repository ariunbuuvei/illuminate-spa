@extends('layouts.master')

@section('stylesheet')
@endsection

@section('content')
<div class="sliderfull">
    <div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classicslider1" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;"> 
        <!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
        <div id="rev_slider_4_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
            <ul>
                <!-- SLIDE  -->
                <li data-index="rs-16" data-transition="zoomout" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-description=""> 
                <!-- MAIN IMAGE --> 
                    <img src="{{ asset('template/images/slider/slider3.jpg') }}"  alt="slider"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina> 
                </li>
                
                <!-- SLIDE  -->
                <li data-index="rs-17" data-transition="zoomout" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-description=""> 
                    <!-- MAIN IMAGE --> 
                    <img src="{{ asset('template/images/slider/slider2.jpg') }}"  alt="slider"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina> 
    
                </li>

                <!-- SLIDE  -->
                <li data-index="rs-18" data-transition="zoomout" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-description=""> 
                    <!-- MAIN IMAGE --> 
                    <img src="{{ asset('template/images/slider/slider1.jpg') }}"  alt="slider"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina> 
                </li>
            </ul>
            <div class="tp-static-layers"></div>
            <div class="tp-bannertimer" style="height: 7px; background-color: rgba(255, 255, 255, 0.25);"></div>
        </div>
    </div>
</div>

<section class="slider-titile">
    <div class="container">
        <div class="row">
            <div class="col-lg-11 pull-right">
                <div class="sliderarrow">
                    <a class="left rev-leftarrow">Left</a>
                    <a class="right rev-rightarrow">Right</a>
                </div>
                <div class="titile-bg">
                    <h1>NU Illuminate Salon</h1>
                </div>    
                <div class="white-bg">
                    <p>We aim to elevate every aspect of the experience for our customers as our version for nails is so different from the rest.</p>
                    <p>“AN OBSESSION FOR NAIL ARTISTRY BASED ON PURE PASSION.” – NAMUUNAA</p>
                </div> 
            </div>
        </div>
    </div>
</section>

<section id="services" class="col-padtop">
    <div class="container">
        <div class="row marbottom">
            <div class="col-sm-12 col-md-7 col-lg-5 pull-right wow fadeInUp">
                <h2>SERVICE MENU</h2>
                <p class="pull-right">Sit back relax and we will take care of the rest. These services are the full works to bring back youth and softness in your hands and feet not to mention super Instagramable.</p>

            </div>
        </div>
        <div class="row marbottom wow fadeInUp">
            <div class="col-sm-12 col-md-5 col-padright-none">
                <div class="subtitle">
                    <h2 class="titile col-xs-offset-1 col-sm-offset-0 col-md-offset-1 ">SIGNATURE  SERVICES</h2>
                </div>
                <img src="{{ asset('template/images/image4.jpg') }}" class="img-responsive" alt="cutting"> 
            </div>
        <div class="col-sm-12 col-md-7 col-padleft-none">
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Service</th>
                <th>Description</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Shape & Polish Hands</td>
                <td class="bigw">file . shape . paint</td>
                <td class="smallw">Polish 15 Gel 32</td>
              </tr>
              <tr>
                <td>Signature Nochip Manicure</td>
                <td>shape . full cuticle care with our gentle high-tech device . mini massage . paint . oil</td>
                <td>Polish 20 / Gel 45</td>
              </tr>
              <tr>
                <td>Shape & Polish Feet</td>
                <td>file .shape . cuticle push back. paint</td>
                <td>Polish 30 / Gel 45</td>
              </tr>
              <tr>
                <td>Signature Nochip Pedicure</td>
                <td>shape . cuticle care . callus treatment . massage . paint . oil</td>
                <td>Polish 40 / Gel 60</td>
              </tr>
              <tr>
                <td>Regular Manicure & Pedicure Combo</td>
                <td>as above</td>
                <td>Polish 50 / Gel 70</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row marbottom wow fadeInUp">
        <div class="col-sm-12 col-md-5 col-padleft-none displayhide">
          <div class="subtitle">
            <h2 class="titile col-xs-offset-2">ADD-ON SERVICES</h2>
          </div>
          <div class="subtitle">
            <h2 class="color">ADD-ON SERVICES</h2>
          </div>
          <img src="{{ asset('template/images/image3.jpg') }}" class="img-responsive" alt="Colour">
        </div>
        <div class="col-sm-12 col-md-7 col-padright-none">
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Service</th>
                <th>Description</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Nail Effects and Finishes</td>
                <td class="bigw">Add that magic to your nails with our finishing touches on all gel services.</td>
                <td class="smallw">$15</td>
              </tr>
              <tr>
                <td>French Finish</td>
                <td>French nail art</td>
                <td>$10</td>
              </tr>
              <tr>
                <td>Gel removal</td>
                <td>Removal of your gel manicure with care</td>
                <td>$10</td>
              </tr>
              <tr>
                <td>IBX Treatment</td>
                <td>Repair and protect your nails with keratin treatment</td>
                <td>$10-15</td>
              </tr>
              <tr>
                <td>Hand massage</td>
                <td>10 Minute hand massage</td>
                <td>15</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="col-sm-12 col-md-5 col-padleft-none displayvisible">
          <div class="subtitle">
            <h2 class="titile col-xs-offset-2">ADD-ON SERVICES</h2>
          </div>
          <img src="{{ asset('template/images/image3.jpg') }}" class="img-responsive" alt="Colour"> </div>
      </div>


      <div class="row wow fadeInUp">
        <div class="col-sm-12 col-md-5 col-padright-none">
          <div class="subtitle">
            <h2 class="titile col-xs-offset-1">ENHANCEMENTS & EXTENSIONS</h2>
          </div>
          <img src="{{ asset('template/images/image2.jpg') }}" class="img-responsive" alt="Style"> </div>
        <div class="col-sm-12 col-md-7 col-padleft-none">
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Service</th>
                <th>Description</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Full set EXTENSIONS with tips </td>
                <td>Hard gel or Dipping powder Added length . instant strength . cuticle care . paint . oil</td>
                <td>Polish 38 / Gel 70</td>
              </tr>
              <tr>
                <td>Overlay on natural nails</td>
                <td>Instant strength . cuticle care . paint . oil</td>
                <td>Polish 30 / Gel 55</td>
              </tr>
              <tr>
                <td>Removal & Mini mani</td>
                <td>Removal with care . cuticle work . oil</td>
                <td>25</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <!-- <div class="row wow fadeInUp" style="margin-top: 40px; font-style: italic; text-align: justify;">
        
            
        
          <small>IMPORTANT NOTICE: All extension bookings require a  50% non-refundable deposit in order to hold the time and date. We are sorry to say that if the required deposit is not paid in advance then your booking will not be confirmed.</small>

      </div> -->

    </div>
</section>

<!-- <section class="excellence wow fadeInUp">
    <div id="parallax-2" class="parallax fixed fixed-desktop" style="background: url({{ asset('template/images/image1.jpg') }}) 50% 0 no-repeat;">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-md-5 col-lg-5 pull-right col-pad5 bg-white">
            <h2>Purchase Gift Certificate</h2>
            <p>With just a few clicks, you can give the gift of any nail treatment. 
            Purchase Gift Certificate</p>
            <div style="margin-top: 30px;">

            <a href="#giftPurchaseModal" rel="modal:open" class="btn btn-primary">Purchase</a>

                <div id="giftPurchaseModal" class="modal" style="max-width: 700px; background-color: #f4f4f4; padding: 70px;">
                
                    <h2 class="text-center">Promotions and Specials</h2>
                    
                        <form action="{{ route('publicPurchaseGift') }}" method="post" id="nonce-form">

                            @csrf
                              <div id="firstpage">
                                <div class="form-group black-border">
                                    <label>Gift amount</label>
                                    <select class="form-control">
                                        <option>--select--</option>
                                        <option value="10">$ 10.00</option>
                                        <option value="25">$ 25.00</option>
                                        <option value="50">$ 50.00</option>
                                        <option value="75">$ 75.00</option>
                                        <option value="100">$ 100.00</option>
                                        <option value="150">$ 150.00</option>
                                        <option value="300">$ 300.00</option>
                                    </select>
                                </div>

                                <div class="form-group black-border">
                                    <label>Name</label>
                                    <input type="text" class="form-control">
                                </div>

                                <div class="form-group black-border">
                                    <label>Email</label>
                                    <input type="text" class="form-control">
                                </div>

                                <div class="form-group black-border">
                                    <label>Contact Number </label>
                                    <input type="text" class="form-control">
                                </div>

                                <div class="form-group black-border">
                                    <label>Description</label>
                                    <textarea type="text" class="form-control"></textarea>
                                </div>
                                <button type="button" class="btn btn-primary" id="nextpage">Next</button>
                              </div>

                              <div id="secondpage" style="display: none;">

                                <div class="form-group black-border" style="margin-top: 40px;">
                                    <label>Total Amount</label>
                                    <input type="text" class="form-control" id="totalAmount">
                                </div>

                                <div class="form-group black-border">
                                    <label>Card Number</label>
                                    <input type="text" class="form-control">
                                </div>

                                <div class="row" style="margin-bottom: 40px;">
                                  <div class="col-sm-4">
                                    <div class="form-group black-border">
                                      <label>Expiration month</label>
                                      <select class="form-control">
                                        <option value="">MM</option>
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-sm-4">
                                    <div class="form-group black-border">
                                        <label>Expiration year</label>
                                        <select class="form-control">
                                          <option value="">YYYY</option>
                                          @for ($i = 0; $i < 10; $i++)
                                            <option value="{{ now()->addYears($i)->format('Y') }}">{{ now()->addYears($i)->format('Y') }}</option>
                                          @endfor
                                          
                                        </select>
                                    </div>
                                  </div>
                                  <div class="col-sm-4">
                                    <div class="form-group black-border">
                                        <label>CVV</label>
                                        <input type="text" class="form-control">
                                    </div>
                                  </div>
                                </div>

                                <input type="hidden" id="card-nonce" name="nonce">
                                 
                                <button type="submit" class="btn btn-primary">Purchase</button>
                              </div>
                                
                        </form>

                </div>
                        
            </div>

           </div>
            
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
</section> -->

<section id="appoinment" style="margin-top: 120px;">

  <div class="appoimentbg" style="background: url({{ asset('template/images/image8.jpg') }});  padding: 200px 0;">
    <div class="container">
      <div class="col-sm-12 col-md-9 col-lg-8">
        <h2>make an appointment</h2>
        <p>All extension bookings require a  50% non-refundable deposit in order to hold the time and date. We are sorry to say that if the required deposit is not paid in advance then your booking will not be confirmed.</p>
        <div class="row" style="margin-top: 30px;">
          <div class="col-md-3">
            <a href="https://app.acuityscheduling.com/schedule.php?owner=15315728" target="_blank" class="btn btn-default">Book</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>



<div id="purchaseSuccess" class="modal" style="max-width: 700px; background-color: #f4f4f4; padding: 70px;">
                
  <h2 class="text-center">Thank you</h2>

  <p class="text-center" style="font-size: 48px; color: green; margin: 40px 0;"><i class="fa fa-check-circle"></i></p>

  <p class="text-center">Transaction successful please check your email</p>
  
</div>

@endsection

@section('javascript')

<script src="https://d3gxy7nm8y4yjr.cloudfront.net/js/embed.js" type="text/javascript"></script>
<script type="text/javascript" src="https://js.squareup.com/v2/paymentform"></script>
<script type="text/javascript">
// $("#purchaseSuccess").modal({
//   fadeDuration: 100,
// });

  $("#nextpage").on('click', function() {
    $("#firstpage").fadeOut();
    $("#totalAmount").val(500);
    requestCardNonce();
    
    setTimeout(function(){
      $("#secondpage").fadeIn();
    
    }, 500);
  });

</script>
<script type="text/javascript">


  // Set the application ID
  var applicationId = "sq0idp-XYT1rJNGMNiSiRGR3EtcLg";

  // Set the location ID
  var locationId = "Z5707199T5WTX";

  var paymentForm = new SqPaymentForm({

  // Initialize the payment form elements
  applicationId: applicationId,
  locationId: locationId,
  inputClass: 'sq-input',
  autoBuild: false,

  // Customize the CSS for SqPaymentForm iframe elements
  inputStyles: [{
    fontSize: '16px',
    fontFamily: 'Helvetica Neue',
    padding: '16px',
    color: '#373F4A',
    backgroundColor: 'transparent',
    lineHeight: '24px',
    placeholderColor: '#CCC',
    _webkitFontSmoothing: 'antialiased',
    _mozOsxFontSmoothing: 'grayscale'
  }],

  // Initialize Apple Pay placeholder ID
  applePay: false,

  // Initialize Masterpass placeholder ID
  masterpass: false,

  // Initialize the credit card placeholders
  cardNumber: {
    elementId: 'sq-card-number',
    placeholder: '• • • •  • • • •  • • • •  • • • •'
  },
  cvv: {
    elementId: 'sq-cvv',
    placeholder: 'CVV'
  },
  expirationDate: {
    elementId: 'sq-expiration-date',
    placeholder: 'MM/YY'
  },
  postalCode: {
    elementId: 'sq-postal-code',
    placeholder: '12345'
  },

  // SqPaymentForm callback functions
  callbacks: {
    /*
     * callback function: createPaymentRequest
     * Triggered when: a digital wallet payment button is clicked.
     * Replace the JSON object declaration with a function that creates
     * a JSON object with Digital Wallet payment details
     */
    createPaymentRequest: function () {

      return {
        requestShippingAddress: false,
        requestBillingInfo: true,
        currencyCode: "USD",
        countryCode: "US",
        total: {
          label: "MERCHANT NAME",
          amount: "100",
          pending: false
        },
        lineItems: [
          {
            label: "Subtotal",
            amount: "100",
            pending: false
          }
        ]
      }
    },

    /*
     * callback function: cardNonceResponseReceived
     * Triggered when: SqPaymentForm completes a card nonce request
     */
    cardNonceResponseReceived: function (errors, nonce, cardData) {
      if (errors) {
        // Log errors from nonce generation to the Javascript console
        console.log("Encountered errors:");
        errors.forEach(function (error) {
          console.log('  ' + error.message);
          alert(error.message);
        });

        return;
      }
      // Assign the nonce value to the hidden form field
      document.getElementById('card-nonce').value = nonce;

      // POST the nonce form to the payment processing page
      document.getElementById('nonce-form').submit();

    },

    /*
     * callback function: unsupportedBrowserDetected
     * Triggered when: the page loads and an unsupported browser is detected
     */
    unsupportedBrowserDetected: function () {
      /* PROVIDE FEEDBACK TO SITE VISITORS */
    },

    /*
     * callback function: inputEventReceived
     * Triggered when: visitors interact with SqPaymentForm iframe elements.
     */
    inputEventReceived: function (inputEvent) {
      switch (inputEvent.eventType) {
        case 'focusClassAdded':
          /* HANDLE AS DESIRED */
          break;
        case 'focusClassRemoved':
          /* HANDLE AS DESIRED */
          break;
        case 'errorClassAdded':
          document.getElementById("error").innerHTML = "Please fix card information errors before continuing.";
          break;
        case 'errorClassRemoved':
          /* HANDLE AS DESIRED */
          document.getElementById("error").style.display = "none";
          break;
        case 'cardBrandChanged':
          /* HANDLE AS DESIRED */
          break;
        case 'postalCodeChanged':
          /* HANDLE AS DESIRED */
          break;
      }
    },

    /*
     * callback function: paymentFormLoaded
     * Triggered when: SqPaymentForm is fully loaded
     */
    paymentFormLoaded: function () {
      /* HANDLE AS DESIRED */
      console.log("The form loaded!");
    }
  }
});
</script>
@endsection