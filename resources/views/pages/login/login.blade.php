@extends('layouts.login')


@section('stylesheet')
@endsection


@section('content')

<div class="container">
    <div class="row">
        <div class="col-6 offset-3">
            <h2 class="text-center mb-3">Login</h2>

            <form action="{{ route('publicWelcomeLoginAction') }}" method="post">
                <!-- CRFT TOKEN GEN -->
                @csrf

                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Email address">
                </div>
                <div class="form-group">
                    <label for="pasword">Password</label>
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>

                <div class="row">
                    <div class="col-6">
                        <button type="submit" class="btn btn-main">Login</button>
                    </div>
                    <div class="col-6">
                        <a href="{{ route('publicWelcomeRegister') }}">Register a new account</a>
                        <br>
                        <a href="{{ route('publicWelcomePasswordForgot') }}">Forgot password</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

@endsection


@section('javascript')
@endsection