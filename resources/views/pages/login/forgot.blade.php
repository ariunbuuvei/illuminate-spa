@extends('layouts.login')


@section('stylesheet')
@endsection


@section('content')

<div class="container">
    <div class="row">
        <div class="col-6 offset-3">
            <h2 class="text-center mb-3">Register</h2>

                <form action="{{ route('publicWelcomePasswordForgotUpdate') }}" method="post">
                    <!-- CRFT TOKEN GEN -->
                    @csrf

                    <div class="form-group">
                        <label for="inputEmail">Email address</label>
                        <input type="email" class="form-control" name="inputEmail" aria-describedby="inputCodeHelp" placeholder="Enter email">
                    </div>

                    <button type="submit" class="btn btn-main">Submit</button>

                </form>

        </div>
    </div>
</div>

@endsection


@section('javascript')
@endsection