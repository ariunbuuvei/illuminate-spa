@extends('layouts.login')


@section('stylesheet')
@endsection


@section('content')

<div class="container">
    <div class="row">
        <div class="col-6 offset-3">
            <h2 class="text-center mb-3">Reset password</h2>

            <form action="{{ route('publicWelcomePasswordForgotResetAction', ['user' => $userObject->id, 'token' => $verifyTokenObject->token]) }}" method="post">
                <!-- CRFT TOKEN GEN -->
                @csrf

                <div class="form-group">
                    <label for="inputPassword">Password</label>
                    <input type="password" class="form-control" name="inputPassword" placeholder="Password">
                </div>
                <div class="form-group">
                    <label for="inputPasswordConfirmation">Password again</label>
                    <input type="password" class="form-control" name="inputPasswordConfirmation" placeholder="Password">
                </div>

                <div class="row">
                    <div class="col-6">
                        <button type="submit" class="btn btn-main">Reset</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

@endsection


@section('javascript')
@endsection