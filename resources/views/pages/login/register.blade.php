@extends('layouts.login')


@section('stylesheet')
@endsection


@section('content')

<div class="container">
    <div class="row">
        <div class="col-6 offset-3">
            <h2 class="text-center mb-3">Register</h2>

            <form action="{{ route('publicWelcomeRegisterAction') }}" method="post">
                <!-- CRFT TOKEN GEN -->
                @csrf

                <div class="form-group">
                    <label for="inputEmail1">Email address</label>
                    <input type="email" class="form-control" name="inputEmail" aria-describedby="emailHelp" placeholder="Enter email">
                </div>

                <div class="form-group">
                    <label for="inputPassword">Password</label>
                    <input type="password" class="form-control" name="inputPassword" placeholder="Password">
                </div>
                <div class="form-group">
                    <label for="inputPasswordConfirmation">Password again</label>
                    <input type="password" class="form-control" name="inputPasswordConfirmation" placeholder="Password">
                </div>

                <div class="row">
                    <div class="col-6">
                        <button type="submit" class="btn btn-main">Register</button>
                    </div>
                    <div class="col-6">
                        <a href="{{ route('publicWelcomeLogin') }}">Login with existing account</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

@endsection


@section('javascript')
@endsection