@extends('layouts.admin')


@section('stylesheet')
@endsection


@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!--Begin::Section-->
	<div class="m-content">
		<div class="row">

			<div class="col-xl-12">
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									Enquiry list
								</h3>
							</div>
						</div>
					</div>
					<div class="m-portlet__body">
						<!--begin::Section-->
						<div class="m-section">
							<div class="m-section__content">
								<!-- adminSettingsEdit -->
								<table class="table m-table m-table--head-bg-brand">
									<thead>
										<tr>
											<th>Id</th>
											<th>User</th>
											<th>Email</th>
											<th>Mobile</th>
											<th>Content</th>
											<th>Status</th>
											<th>Created</th>
										</tr>
									</thead>
									<tbody>
										@foreach($enquireList as $enquiry)
										<tr>
											<td>{{ $enquiry->id }}</td>
											<td><a href="{{ route('adminEnquireSingle', ['enquiry' => $enquiry->id]) }}">{{ $enquiry->author }}</a></td>
											<td>{{ $enquiry->user }}</td>
											<td>{{ $enquiry->mobile }}</td>
											<td>{{ $enquiry->contentTargetName()->title }}</td>
											<td><span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Active</span></td>
											<td>{{ $enquiry->created_at->format('d M, Y') }}</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
						<!--end::Section-->
					</div>
				</div>
    		</div>

    	</div>
	</div>
	<!--End::Section-->
</div>
@endsection

@section('javascript')
@endsection









