@extends('layouts.admin')


@section('stylesheet')
@endsection


@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!--Begin::Section-->
	<div class="m-content">
		<div class="row">

			<div class="offset-md-2 col-md-8">
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									{{ $enquireObject->user }}
								</h3>
							</div>
						</div>
					</div>
					<div class="m-portlet__body">
						<!--begin::Section-->
						<div class="m-section">
							<div class="m-section__content">
								<div class="row">

									<div class="form-group col-md-12">
										<label for="placeholder">Content</label>
										<input type="text" class="form-control" value="{{ $enquireObject->contentTargetName()->title }}" readonly>
									</div>

									<div class="form-group col-md-12">
										<label for="placeholder">User</label>
										<input type="text" class="form-control" value="{{ $enquireObject->author }}" readonly>
									</div>

									<div class="form-group col-md-12">
										<label for="placeholder">Email</label>
										<input type="text" class="form-control" value="{{ $enquireObject->user }}" readonly>
									</div>

									<div class="form-group col-md-12">
										<label for="placeholder">Mobile</label>
										<input type="text" class="form-control" value="{{ $enquireObject->mobile }}" readonly>
									</div>

									<div class="form-group col-md-12">
										<label for="placeholder">Requested at</label>
										<input type="text" class="form-control" value="{{ $enquireObject->created_at->format('d M, Y') }}" readonly>
									</div>

									<div class="form-group col-md-12">
										<label for="placeholder">Message</label>
										<textarea class="form-control" rows="7" readonly>{{ $enquireObject->content }}</textarea>
									</div>

									<div class="col-sm-12 text-center">
					  					<a href="{{ route('adminEnquireList') }}" class="btn btn-outline-metal">Back</a>
							  		</div>

								</div>
							</div>
						</div>
						<!--end::Section-->
					</div>
				</div>
    		</div>

    	</div>
	</div>
	<!--End::Section-->
</div>
@endsection

@section('javascript')
@endsection









