@extends('layouts.admin')


@section('stylesheet')
@endsection


@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!--Begin::Section-->
	<div class="m-content">
		<div class="row">

			@isset($settingsCollections)
			<div class="col-md-12">
			@else
			<div class="offset-md-2 col-md-8">
			@endisset
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									Create content
								</h3>
							</div>
						</div>
					</div>
					<div class="m-portlet__body">
						<!--begin::Section-->
						<div class="m-section">
							<div class="m-section__content">
								{!! Form::open(['method' => 'POST', 'url' => route('adminPageStore') ]) !!}

									<div class="row">

										<input type="hidden" name="post_token" value="{{ $postToken }}">

										@isset($settingsCollections)
										<div class="col-md-6">
										@else
										<div class="col-md-12">
										@endisset
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														{!! Form::label('title', 'Title') !!}
														<span class="m--font-danger">*</span>
														<div class="m-input-icon m-input-icon--left">
															{!! Form::text('title', old('title'), ['class' => 'form-control m-input', 'placeholder' => 'Page title', 'required' => 'required']) !!}
															<span class="m-input-icon__icon m-input-icon__icon--left">
																<span><i class="flaticon-notes"></i></span>
															</span>
														</div>
													</div>
												</div>
												<div class="col-md-12">
													<div class="form-group">
														{!! Form::label('type', 'Type') !!}
														<span class="m--font-danger">*</span>
														{!! Form::select('type', $typeList, old('type'), [
														'class' => 'form-control', 
														'placeholder' => '-Select type-',
														'required' => 'required'
														]) !!}
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														{!! Form::label('status', 'Status') !!}
														<span class="m--font-danger">*</span>
														{!! Form::select('status', $statusList, old('status'), [
														'class' => 'form-control', 
														'placeholder' => '-Select status-',
														'required' => 'required'
														]) !!}
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														{!! Form::label('category', 'Category') !!}
														<span class="m--font-danger">*</span>
														{!! Form::select('category', $categoryList, old('category'), [
														'class' => 'form-control', 
														'placeholder' => '-Select category-',
														'required' => 'required'
														]) !!}
													</div>
												</div>
												<div class="col-md-12">
													<div class="form-group m-form__group row">
														<div class="col-md-12">
															{!! Form::label('thumbnail', 'Thumbnail image') !!}
														</div>
														<div class="col-md-12">
															<div class="m-dropzone" id="mainDropzoneElement">
																<div class="m-dropzone__msg dz-message needsclick">
																	<h3 class="m-dropzone__msg-title">
																		Drop file here or click to upload.
																	</h3>
																	<span class="m-dropzone__msg-desc">
																		Allowed maximum size for one file is
																		<strong>
																			25 MB
																		</strong>
																	</span>
																</div>
															</div>
														</div>
													</div>
												</div>
												
											</div>
										</div>


										@isset($settingsCollections)
											<div class="col-md-6">
												
												@foreach($settingsCollections as $settings)
													<div class="m-portlet m-portlet--skin-dark m-portlet--bordered-semi m--bg-brand">
														<div class="m-portlet__head">
															<div class="m-portlet__head-caption">
																<div class="m-portlet__head-title">
																	<span class="m-portlet__head-icon">
																		<i class="flaticon-file-1"></i>
																	</span>
																	<h3 class="m-portlet__head-text">
																		{{ $settings->name }}
																	</h3>
																</div>
															</div>
														</div>
														<div class="m-portlet__body">
															@foreach($settings->settingsAttrCollection as $settingsAttr)
																<div class="form-group">
																	{!! Form::label($settingsAttr->attr, $settingsAttr->placeholder) !!}
																	{!! Form::text($settingsAttr->attr, old($settingsAttr->attr), [
																		'class' => 'form-control m-input', 
																		'placeholder' => $settingsAttr->placeholder,
																	]) !!}
																</div>
															@endforeach
														</div>
													</div>
												@endforeach
											</div>
										@endisset

										<div class="col-md-12">
											<div class="form-group">
												{!! Form::label('description', 'Description') !!}
												{!! Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => 'Course description', 'rows' => '8']) !!}
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												{!! Form::label('content', 'Content') !!}
												{!! Form::textarea('content', old('content'), ['class' => 'summernote']) !!}
											</div>
										</div>
										<div class="col-sm-12 text-center">
											<a href="{{ route('adminPageList') }}" class="btn btn-outline-metal">Back</a>
											{{ Form::button('Submit', ['type' => 'submit', 'class' => 'btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning'] )  }}
										</div>

									</div>

				  				{!! Form::close() !!}

							</div>
						</div>
						<!--end::Section-->
					</div>
				</div>
    		</div>

    	</div>
	</div>
	<!--End::Section-->
</div>
@endsection

@section('javascript')
	<script type="text/javascript">
		var fileUploadUrl = "{{ route('adminFileUpload', ['token' => $postToken]) }}";
	</script>
	<script src="{{ URL::asset('assets/js/pages/page.js') }}" type="text/javascript"></script>
@endsection









