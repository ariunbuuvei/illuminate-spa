@extends('layouts.admin')


@section('stylesheet')
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/plugins/content-tool/content-tools.min.css') }}">
	<link href="{{ URL::asset('assets/style.css') }}" rel="stylesheet" type="text/css">
	<style type="text/css">
		.ct-ignition {
			display: none;
		}
	</style>
@endsection


@section('content')


<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!--Begin::Section-->
	<div class="m-content">
		<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							{{ $pageEditorObject->title }}
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools">
					<button class="btn btn-info" id="editorStart">Edit</button>
					<button class="btn btn-success" id="editorStop" disabled>Save</button>
				</div>
			</div>
			<div class="m-portlet__body">
				<!--begin::Section-->
				<div class="m-section">
					<div class="m-section__content" data-name="main-content" id="mainContent">
						{!! $pageEditorObject->content !!}
					</div>
					<div class="text-center" style="margin-top: 20px;">
						{!! Form::open(['method' => 'POST', 'id' => 'adminPageEditorDelete', 'url' => route('adminPageEditorDelete', ['page' => $pageEditorObject->id]) ]) !!}
							<a href="{{ route('adminPageList') }}" class="btn btn-outline-metal">Back</a>
							{{ Form::button('Delete', [
								'type' => 'button',
								'class' => 'btn btn-danger askBeforeSend',
								'rel' => '#adminPageEditorDelete'
							]) }}
						{!! Form::close() !!}
					</div>
				</div>
				<!--end::Section-->
			</div>
		</div>
	</div>
	<!--End::Section-->
</div>


@endsection

@section('javascript')
	<script src="{{ URL::asset('assets/plugins/content-tool/content-tools.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		window.addEventListener('load', function() {

    		var editor;

			editor = ContentTools.EditorApp.get();

			editor.init(
			    '[data-editable]', 
			    'data-name',
			);

			$("#editorStart").on("click", function() {
				$(this).attr("disabled", true);
				$("#editorStop").removeAttr("disabled");
				editor.start();
			});

			$("#editorStop").on("click", function() {
				$(this).attr("disabled", true);
				$("#editorStart").removeAttr("disabled");
				editor.stop(true);
			});

			ContentTools.IMAGE_UPLOADER = imageUploader;

			function imageUploader(dialog) {
			    var image, xhr, xhrComplete, xhrProgress;

			    // Set up the event handlers
			    dialog.addEventListener('imageuploader.fileready', function (ev) {
			        // Upload a file to the server
			        var formData;
			        var file = ev.detail().file;

			        // Define functions to handle upload progress and completion
			        xhrProgress = function (ev) {
			            // Set the progress for the upload
			            dialog.progress((ev.loaded / ev.total) * 100);
			        }

			        // Set the dialog state to uploading and reset the progress bar to 0
			        dialog.state('uploading');
			        dialog.progress(0);

			        // Build the form data to post to the server
			        formData = new FormData();
			        formData.append('image', file);

			        $.ajax({
			            url: "{{ route("adminPageEditorUploadImage", ['page' => $pageEditorObject->id]) }}",
			            headers:
					    {
					        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					    },
			            method: "POST",
			            data: formData,
			            processData: false,
  						contentType: false,
  						xhr: function() {
			                var myXhr = $.ajaxSettings.xhr();
			                if(myXhr.upload){
			                    myXhr.upload.addEventListener('progress', xhrProgress);
			                }
			                return myXhr;
			            },
			            success: function (result) {
			            	if (result.status) {

			            		image = {
				                    size: result.data.size,
				                    url: result.data.url,
				                    alt: result.data.alt,
				                };

			            		dialog.populate(result.data.url, result.data.size);

			            		toastr.success(result.status);
			            	}
			            },
			        });
			    });

			    dialog.addEventListener('imageuploader.clear', function () {
			        // Clear the current image
			        dialog.clear();
			        image = null;
			    });

			    dialog.addEventListener('imageuploader.save', function () {
			        var crop, cropRegion, formData;
			        dialog.busy(true);

			        dialog.save(
			        	image.url,
			        	image.size,
			        	{
			        		'alt': image.alt,
			        		'data-ce-max-width': image.size[0]
			        	}
			        );

			        toastr.success("image inserted");
			    });

			    
			}

			

			editor.addEventListener('saved', function (ev) {

			    var name, payload, regions, xhr;

			    // Check that something changed
			    regions = ev.detail().regions;
			    if (Object.keys(regions).length == 0) {
			        return;
			    }

			    // console.log(regions);

			    // Set the editor as busy while we save our changes
			    this.busy(true);

			    // Collect the contents of each region into a FormData instance
			    // payload = new FormData();
			    // for (name in regions) {
			    //     if (regions.hasOwnProperty(name)) {
			    //         payload.append(name, regions[name]);
			    //     }
			    // }

			    // Send the update content to the server to be saved
			    function onStateChange(ev) {
			        // Check if the request is finished
			        if (ev.target.readyState == 4) {
			            editor.busy(false);
			            if (ev.target.status == '200') {
			                // Save was successful, notify the user with a flash
			                new ContentTools.FlashUI('ok');
			            } else {
			                // Save failed, notify the user with a flash
			                new ContentTools.FlashUI('no');
			            }
			        }
			    };


			    $.ajax({
		            url: "{{ route("adminPageEditorUpdate", ['page' => $pageEditorObject->id]) }}",
		            headers:
				    {
				        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				    },
		            method: "POST",
		            data: {
		            	'html': $("#mainContent").html()
		            },
		            dataType: 'JSON',
		            success: function (result) {
		            	if (result.status) {
		            		toastr.success(result.status);
		            	}
		            },
		        });
			});

		});
	</script>
@endsection









