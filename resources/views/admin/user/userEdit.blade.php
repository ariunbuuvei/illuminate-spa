@extends('layouts.admin')


@section('stylesheet')
@endsection


@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!--Begin::Section-->
	<div class="m-content">
		<div class="m-portlet m-portlet--tabs">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							{{ $userObject->email }}
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools">
					<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--right m-tabs-line-danger" role="tablist">
						<li class="nav-item m-tabs__item">
							<a class="nav-link m-tabs__link active show" data-toggle="tab" href="#m_portlet_tab_1_1" role="tab" aria-selected="false">
								Profile
							</a>
						</li>
						<li class="nav-item m-tabs__item">
							<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_portlet_tab_1_2" role="tab" aria-selected="true">
								User history 
							</a>
						</li>
						<!-- <li>
							{!! Form::open(['method' => 'POST', 'id' => 'adminUserDelete', 'url' => route('adminUserDelete', ['status' => $userObject->id]) ]) !!}
							{{ Form::button('Delete', [
								'type' => 'button', 
								'class' => 'btn btn-danger askBeforeSend',
								'rel' => '#adminUserDelete'
								] )  }}
							{!! Form::close() !!}
						</li> -->
					</ul>
				</div>
			</div>
			<div class="m-portlet__body">
				<div class="tab-content">
					<div class="tab-pane active show" id="m_portlet_tab_1_1">
						<!--begin::Section-->
						<div class="m-section">

							<div class="row">
								@if ($userSettingsCollection)
									<div class="col-md-6">
										<div class="m-section__content">
											{!! Form::open(['method' => 'POST', 'url' => route('adminUserUpdate', ['user' => $userObject->id]), 'id' => 'adminUserUpdate' ]) !!}
											<div class="row">

												<div class="col-md-12">
													<div class="form-group">
														{!! Form::label('firstname', 'Firstname') !!}
														{!! Form::text('firstname', $userObject->firstname, ['class' => 'form-control', 'placeholder' => 'First name']) !!}
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														{!! Form::label('lastname', 'Lasttname') !!}
														{!! Form::text('lastname', $userObject->lastname, ['class' => 'form-control', 'placeholder' => 'Last name']) !!}
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														{!! Form::label('mobile', 'Mobile') !!}
														{!! Form::text('mobile', $userObject->mobile, ['class' => 'form-control', 'placeholder' => 'Mobile number']) !!}
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														{!! Form::label('address', 'Address') !!}
														{!! Form::textarea('address', $userObject->address, ['class' => 'form-control', 'placeholder' => 'Address']) !!}
													</div>
												</div>

												<div class="col-sm-12 text-center">
													<a href="{{ route('adminUserIndex') }}" class="btn btn-outline-metal">Back</a>
													{{ Form::button('Submit', ['type' => 'button', 'class' => 'btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning askBeforeSend', 'rel' => '#adminUserUpdate'] )  }}
												</div>

											</div>
											{!! Form::close() !!}
										</div>
									</div>

									<div class="col-md-6">
										@foreach ($userSettingsCollection as $userSettings)
											<div class="m-portlet m-portlet--skin-dark m-portlet--bordered-semi m--bg-brand">
												<div class="m-portlet__head">
													<div class="m-portlet__head-caption">
														<div class="m-portlet__head-title">
															<span class="m-portlet__head-icon">
																<i class="flaticon-file-1"></i>
															</span>
															<h3 class="m-portlet__head-text">
																{{ $userSettings->name }}
															</h3>
														</div>
													</div>
												</div>
												<div class="m-portlet__body">
													@foreach($userSettings->settingsAttrCollection as $settingsAttr)
														<div class="form-group">
															{!! Form::label($settingsAttr->attr, $settingsAttr->placeholder) !!}
															{!! Form::text($settingsAttr->attr, $settingsAttr->settingsAttrValue('user', $userObject->id, $settingsAttr->attr), ['class' => 'form-control m-input', 'placeholder' => $settingsAttr->placeholder]) !!}
														</div>
													@endforeach
												</div>
											</div>
										@endforeach
									</div>
								@else

								@endif
							</div>
							
						</div>
						<!--end::Section-->
					</div>
					<div class="tab-pane" id="m_portlet_tab_1_2">
						<div class="m-section">
							<div class="row">
								<div class="col-md-12">
									<div class="m-portlet m-portlet--brand m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered">
										<div class="m-portlet__head">
											<div class="m-portlet__head-caption">
												<div class="m-portlet__head-title">
													<h3 class="m-portlet__head-text">
													User payment history
													</h3>
												</div>			
											</div>
										</div>
										<div class="m-portlet__body">
											<table class="table m-table m-table--head-separator-primary">
												<thead>
													<tr>
														<th>#</th>
														<th>Name</th>
														<th>Amount</th>
														<th>Currency</th>
														<th>Bundle</th>
														<th>Created</th>
														<th>Ends</th>
													</tr>
												</thead>
												<tbody>
													@foreach($userSubscriptionCollection as $userSubscription)
													<tr>
														<th scope="row">{{ $userSubscription->id }}</th>
														<td>{{ $userObject->name }}</td>
														<td>{{ $userSubscription->paymentBundleObject->amount }}</td>
														<td>{{ $userSubscription->paymentBundleObject->currency }}</td>
														<td>{{ $userSubscription->paymentBundleObject->name }}</td>
														<td>{{ $userSubscription->created_at }}</td>
														<td>{{ $userSubscription->ends_at }}</td>
													</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>

								<div class="col-md-12">
									<div class="m-portlet m-portlet--brand m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered">
										<div class="m-portlet__head">
											<div class="m-portlet__head-caption">
												<div class="m-portlet__head-title">
													<h3 class="m-portlet__head-text">
													Attended courses
													</h3>
												</div>			
											</div>
										</div>
									</div>
								</div>
							</div>

							
						</div>
					</div>
				</div>

				
			</div>
		</div>
	</div>
	<!--End::Section-->
</div>
@endsection

@section('javascript')
<script type="text/javascript">

$( "#adminUserCreate" ).validate({
    // define validation rules
    rules: {
    	email: {
    		required: true,
    		email: true 
    	},
    	url: {
    		required: true 
    	},
    	digits: {
    		required: true,
    		digits: true
    	},
    	creditcard: {
    		required: true,
    		creditcard: true 
    	},
    },

    //display error alert on form submit  
    invalidHandler: function(event, validator) {     
    	mApp.scrollTo("#adminUserCreate");
    },

    submitHandler: function (form) {
    //form[0].submit(); // submit the form
}
});  

</script>
@endsection









