@extends('layouts.admin')


@section('stylesheet')
@endsection


@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!--Begin::Section-->
	<div class="m-content">
    <div class="m-portlet">
     <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
       <div class="m-portlet__head-title">
        <h3 class="m-portlet__head-text">
         Create User
       </h3>
     </div>
   </div>
 </div>
 <div class="m-portlet__body">
  <!--begin::Section-->
  <div class="m-section">
    {!! Form::open(['method' => 'POST', 'url' => route('adminUserStore'), 'id' => 'adminUserStore' ]) !!}



   <div class="m-section__content">
    
    <div class="row">

      <div class="col-md-6">
        <div class="row">
              <div class="col-md-12 m-form__group">
         {!! Form::label('email', 'Email', ['class' => 'col-form-label']) !!}
         <span class="m--font-danger">*</span>
         <div class="form-group">
          {!! Form::email('email', null, ['class' => 'form-control m-input', 'placeholder' => 'Enter user email', 'required' => 'required']) !!}
        </div>
      </div>

    <div class="col-md-12 mb-4">
     {!! Form::label('password', 'Password') !!}
     <span class="m--font-danger">*</span>
     <div class="input-group">
       {!! Form::text('password', null, ['class' => 'form-control form-control-danger', 'rel' => 'gp', 'data-size' => '12', 'data-character-set' => 'a-z,A-Z,0-9,#', 'required' => 'required']) !!}
       <div class="input-group-append">
         <button type="button" class="btn btn-primary m-btn m-btn--icon" id="getNewPass" rel="password">
           <i class="fa fa-refresh"></i>
         </button>
       </div>
     </div>
   </div>

   <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('firstname', 'Firstname') !!}
      {!! Form::text('firstname', null, ['class' => 'form-control', 'placeholder' => 'First name']) !!}
    </div>
  </div>

  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('lastname', 'Lasttname') !!}
      {!! Form::text('lastname', null, ['class' => 'form-control', 'placeholder' => 'Last name']) !!}
    </div>
  </div>

  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('mobile', 'Mobile') !!}
      {!! Form::text('mobile', null, ['class' => 'form-control', 'placeholder' => 'Mobile number']) !!}
    </div>
  </div>

  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('address', 'Address') !!}
      {!! Form::textarea('address', null, ['class' => 'form-control', 'placeholder' => 'Address']) !!}
    </div>
  </div>

  <div class="col-sm-12 text-center">
   <a href="{{ route('adminUserIndex') }}" class="btn btn-outline-metal">Back</a>
   {{ Form::button('Submit', ['type' => 'button', 'class' => 'btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning askBeforeSend', 'rel' => '#adminUserStore'] )  }}
 </div>
        </div>
      </div>

      <div class="col-md-6">


              @foreach ($userSettingsCollection as $userSettings)
                <div class="m-portlet m-portlet--skin-dark m-portlet--bordered-semi m--bg-brand">
                  <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                      <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                          <i class="flaticon-file-1"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                          {{ $userSettings->name }}
                        </h3>
                      </div>
                    </div>
                  </div>
                  <div class="m-portlet__body">
                    @foreach($userSettings->settingsAttrCollection as $settingsAttr)
                      <div class="form-group">
                        {!! Form::label($settingsAttr->attr, $settingsAttr->placeholder) !!}
                        {!! Form::text($settingsAttr->attr, null, ['class' => 'form-control m-input', 'placeholder' => $settingsAttr->placeholder]) !!}
                      </div>
                    @endforeach
                  </div>
                </div>
              @endforeach


      </div>


      

</div>


</div>

{!! Form::close() !!}




</div>
<!--end::Section-->
</div>
</div>

</div>
<!--End::Section-->
</div>
@endsection

@section('javascript')
<script type="text/javascript">
	
// Generate a password string
function randString(id){
  	var dataSet = $(id).attr('data-character-set').split(',');  
  	var possible = '';

  	if($.inArray('a-z', dataSet) >= 0){
    	possible += 'abcdefghijklmnopqrstuvwxyz';
  	}

  	if($.inArray('A-Z', dataSet) >= 0){
    	possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  	}

  	if($.inArray('0-9', dataSet) >= 0){
    	possible += '0123456789';
  	}

  	if($.inArray('#', dataSet) >= 0){
    	possible += '![]{}()%&*$#^<>~@|';
  	}

  	var text = '';
  	for(var i=0; i < $(id).attr('data-size'); i++) {
    	text += possible.charAt(Math.floor(Math.random() * possible.length));
  	}

  	return text;
}

// Create a new password on page load
$('input[rel="gp"]').each(function(){
  	$(this).val(randString($(this)));
});

// Create a new password
$("#getNewPass").click(function() {
  	var field = $('input[name="'+$(this).attr("rel")+'"]');
  	field.val(randString(field));
});

// Auto Select Pass On Focus
$('input[rel="gp"]').on("click", function () {
   	$(this).select();
});

$( "#adminUserCreate" ).validate({
    // define validation rules
    rules: {
    	email: {
    		required: true,
    		email: true 
    	},
    	url: {
    		required: true 
    	},
    	digits: {
    		required: true,
    		digits: true
    	},
    	creditcard: {
    		required: true,
    		creditcard: true 
    	},
    },

    //display error alert on form submit  
    invalidHandler: function(event, validator) {     
    	mApp.scrollTo("#adminUserCreate");
    },

    submitHandler: function (form) {
    //form[0].submit(); // submit the form
}
});  

</script>
@endsection









