@extends('layouts.admin')


@section('stylesheet')
@endsection


@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!--Begin::Section-->
	<div class="m-content">
		<div class="row">

			<div class="offset-md-2 col-md-8">
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									Create course
								</h3>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							{!! Form::open(['method' => 'POST', 'id' => 'adminCourseDelete', 'url' => route('adminCourseDelete', ['page' => $courseObject->id]) ]) !!}
								{{ Form::button('Delete', [
									'type' => 'button', 
									'class' => 'btn btn-danger askBeforeSend',
									'rel' => '#adminCourseDelete'
								] )  }}
							{!! Form::close() !!}
						</div>
					</div>
					<div class="m-portlet__body">
						<!--begin::Section-->
						<div class="m-section">
							<div class="m-section__content">
								{!! Form::open(['method' => 'POST', 'url' => route('adminCourseUpdate', ['course' => $courseObject->id ]), 'enctype' => 'multipart/form-data' ]) !!}
									<input type="hidden" name="post_token" value="{{ $courseObject->post_token }}">
					  				<div class="row">
					  					<div class="col-md-12">
					  						<div class="form-group">
					  							{!! Form::label('title', 'Title') !!}
					  							<span class="m--font-danger">*</span>
					  							<div class="m-input-icon m-input-icon--left">
						  							{!! Form::text('title', $courseObject->title , ['class' => 'form-control m-input', 'placeholder' => 'Course title', 'required' => 'required']) !!}
						  							<span class="m-input-icon__icon m-input-icon__icon--left">
						  								<span><i class="flaticon-notes"></i></span>
						  							</span>
					  							</div>
					  						</div>
					  					</div>
					  					<div class="col-md-6">
						  					<div class="form-group">
					  							{!! Form::label('per_q_mark', 'Per Question Score') !!}
					  							<span class="m--font-danger">*</span>
					  							{!! Form::number('per_q_mark', $courseObject->per_q_mark, ['class' => 'form-control', 'placeholder' => 'Question score', 'required' => 'required']) !!}
					  						</div>
					  					</div>
					  					<div class="col-md-6">
					  						<div class="form-group">
					  							{!! Form::label('timer', 'Quiz Time (Minute)') !!}
					  							{!! Form::number('timer', $courseObject->timer, ['class' => 'form-control', 'placeholder' => 'Quiz Total time (In Minutes)']) !!}
					  						</div>
					  					</div>
					  					<div class="col-md-12">
					  						<div class="form-group m-form__group row">
					  							<div class="col-md-12">
					  								{!! Form::label('thumbnail', 'Thumbnail image') !!}
					  							</div>
												<div class="col-md-12">
													<div class="m-dropzone" id="mainDropzoneElement">
														<div class="m-dropzone__msg dz-message needsclick">
															<h3 class="m-dropzone__msg-title">
																Drop file here or click to upload.
															</h3>
															<span class="m-dropzone__msg-desc">
																Allowed maximum size for one file is
																<strong>
																	25 MB
																</strong>
															</span>
														</div>
													</div>
												</div>
					  						</div>
					  					</div>

					  					<div class="col-md-12">
					  						<div class="form-group">
												<label for="exampleInputEmail1">Course video</label>
												<div></div>
												<div class="custom-file">
												  	<input type="file" class="custom-file-input" id="customFile" name="file">
												  	<label class="custom-file-label" for="customFile">Choose file</label>
												</div>
											</div>
										</div>

					  					<div class="col-md-12 mb-3">
					  						<div class="form-group">
					  							{!! Form::label('description', 'Description') !!}
					  							{!! Form::textarea('description', $courseObject->description, ['class' => 'summernote']) !!}
					  						</div>
					  					</div>
					  					<div class="col-sm-12 text-center">
					  						<button type="button" class="btn btn-outline-metal">Back</button>
							  				{{ Form::button('Submit', ['type' => 'submit', 'class' => 'btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning'] )  }}
							  			</div>
					  				</div>
				  				{!! Form::close() !!}

							</div>
						</div>
						<!--end::Section-->
					</div>
				</div>
    		</div>

    	</div>
	</div>
	<!--End::Section-->
</div>
@endsection

@section('javascript')
	<script type="text/javascript">
		var fileUploadUrl = "{{ route('adminFileUpload', ['token' => $courseObject->post_token]) }}";
	</script>
	<script src="{{ URL::asset('assets/js/pages/page.js') }}" type="text/javascript"></script>
@endsection









