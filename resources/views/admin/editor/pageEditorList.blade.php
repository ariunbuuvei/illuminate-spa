@extends('layouts.admin')


@section('stylesheet')
@endsection


@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!--Begin::Section-->
	<div class="m-content">
		<div class="row">

			<div class="col-xl-12">
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									Content list
								</h3>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							<a href="{{ route('adminPageEditorCreate') }}" class="btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning"><i class="flaticon-add"></i> Add new</a>
						</div>
					</div>
					<div class="m-portlet__body">
						<!--begin::Section-->
						<div class="m-section">
							<div class="m-section__content">
								<!-- adminSettingsEdit -->
								<table class="table m-table m-table--head-bg-brand">
									<thead>
										<tr>
											<th>id</th>
											<th>Title</th>
											<th>Fields</th>
											<th>Status</th>
											<th>Created</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($pageEditorList as $pageEditor)
											<tr>
												<td>{{ $pageEditor->id }}</td>
												<td><a href="{{ route('adminPageEditorEdit', ['page' => $pageEditor->id]) }}">{{ $pageEditor->title }}</a></td>
												<td>{{ $pageEditor->type_id }}</td>
												<td><span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Active</span></td>
												<td>{{ $pageEditor->created_at->format('d M, Y') }}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
						<!--end::Section-->
					</div>
				</div>
    		</div>

    	</div>
	</div>
	<!--End::Section-->
</div>
@endsection

@section('javascript')
@endsection









