@extends('layouts.admin')


@section('stylesheet')
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/plugins/content-tool/content-tools.min.css') }}">
	<link href="{{ URL::asset('assets/style.css') }}" rel="stylesheet" type="text/css">
	<style type="text/css">
		.ct-ignition {
			display: none;
		}
	</style>
@endsection


@section('content')


<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!--Begin::Section-->
	<div class="m-content">
		<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							Page editor
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools">
					<button class="btn btn-info" id="editorStart">Edit</button>
					<button class="btn btn-success" id="editorStop" disabled>Save</button>
				</div>
			</div>
			<div class="m-portlet__body">
				<!--begin::Section-->
				<div class="m-section">
					<div class="m-section__content" data-name="main-content" id="mainContent">
						
						

							<div class="mt-3"><div class="container"><div class="row"><div class="col-md-12 col-sm-12 mb-5"><h1 class="text-center">Benifits of Program</h1></div></div><div class="row"><div class="col-md-4"><img src="http://127.0.0.1:8000/assets/images/doggy.gif" class="mx-auto mb-4 d-none d-md-block"></div><div class="col-md-4"><img src="http://127.0.0.1:8000/assets/images/piggy.gif" class="mx-auto mb-4 d-none d-md-block"></div><div class="col-md-4"><img src="http://127.0.0.1:8000/assets/images/Camera.gif" class="mx-auto mb-4 d-none d-md-block"></div></div><div class="row"><div class="col-md-4" data-editable=""><img class="mx-auto mb-4 d-block d-md-none" height="250" src="http://127.0.0.1:8000/assets/images/doggy.gif" width="300"><h2 class="mb-3"> Flexibility1</h2><p class="p-secondary text-justify"> The dogs school program is convenient and fits around your busy schedule. If you want to train at 4am or 10pm, you have that flexibility of doing just that. Do you want to focus on one particular aspect of training? You can do that? Did you miss something ad need to repeat it. You can replay at your convenient on your PC, table or smart phone.</p></div><div class="col-md-4" data-editable=""><img class="mx-auto mb-4 d-block d-md-none" height="250" src="http://127.0.0.1:8000/assets/images/piggy.gif" width="300"><h2 class="mb-3"> Saving you money</h2><p class="p-secondary text-justify"> In home training can be costly and time consuming. The Dog School is competitive and allows you to break up the cost of in home training over monthly instalments, weekly installments or even as a lump sum. You’ll also never have to miss a lesson because something came up.</p></div><div class="col-md-4" data-editable=""><img class="mx-auto mb-4 d-block d-md-none" height="250" src="http://127.0.0.1:8000/assets/images/Camera.gif" width="300"><h2 class="mb-3"> Dogs School Community</h2><p class="p-secondary text-justify"> Giving you access to our exclusive Dogs school community where you can post questions, share videos of your achievements and ask advice on training problem areas. Fully monitored by our Dog training experts.</p></div></div></div></div>
						
						    


						</div>
					</div>
				</div>
				<!--end::Section-->
			</div>
		</div>
	</div>
	<!--End::Section-->
</div>


@endsection

@section('javascript')
	<script src="{{ URL::asset('assets/plugins/content-tool/content-tools.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		window.addEventListener('load', function() {

    		var editor;

			editor = ContentTools.EditorApp.get();



			editor.init(
			    '[data-editable]', 
			    'data-name',
			);

			$("#editorStart").on("click", function() {
				$(this).attr("disabled", true);
				$("#editorStop").removeAttr("disabled");
				editor.start();
			});

			$("#editorStop").on("click", function() {
				$(this).attr("disabled", true);
				$("#editorStart").removeAttr("disabled");
				editor.stop(true);
			});

			editor.addEventListener('saved', function (ev) {

			    var name, payload, regions, xhr;

			    // Check that something changed
			    regions = ev.detail().regions;
			    if (Object.keys(regions).length == 0) {
			        return;
			    }

			    // console.log(regions);

			    // Set the editor as busy while we save our changes
			    this.busy(true);

			    // Collect the contents of each region into a FormData instance
			    // payload = new FormData();
			    // for (name in regions) {
			    //     if (regions.hasOwnProperty(name)) {
			    //         payload.append(name, regions[name]);
			    //     }
			    // }

			    // Send the update content to the server to be saved
			    function onStateChange(ev) {
			        // Check if the request is finished
			        if (ev.target.readyState == 4) {
			            editor.busy(false);
			            if (ev.target.status == '200') {
			                // Save was successful, notify the user with a flash
			                new ContentTools.FlashUI('ok');
			            } else {
			                // Save failed, notify the user with a flash
			                new ContentTools.FlashUI('no');
			            }
			        }
			    };


			    $.ajax({
		            url: "{{ route("adminPageEditorUpdate") }}",
		            headers:
				    {
				        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				    },
		            method: "POST",
		            data: {
		            	'html': $("#mainContent").html()
		            },
		            dataType: 'JSON',
		            success: function (result) {
		                console.log(result);
		            },
		        });

			});

		});
	</script>
@endsection









