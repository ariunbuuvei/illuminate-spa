@extends('layouts.admin')
<link href="{{ URL::asset('assets/plugins/codemirror-5.42.2/lib/codemirror.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/codemirror-5.42.2/addon/fold/foldgutter.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/codemirror-5.42.2/addon/dialog/dialog.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/codemirror-5.42.2/theme/monokai.css') }}" rel="stylesheet" type="text/css">
@section('stylesheet')



@endsection


@section('content')


<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!--Begin::Section-->
	<div class="m-content">
		<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							Create editor
						</h3>
					</div>
				</div>
			</div>
			<div class="m-portlet__body">
				<!--begin::Section-->
				<div class="m-section">
					<div class="m-section__content">
						{!! Form::open(['method' => 'POST', 'url' => route('adminPageEditorStore'), 'id' => 'adminPageEditorStore', ]) !!}
							<div class="form-group">
								{!! Form::label('title', 'Title') !!}
								<span class="m--font-danger">*</span>
								<div class="m-input-icon m-input-icon--left">
									{!! Form::text('title', null, ['class' => 'form-control m-input', 'placeholder' => 'Page title', 'required' => 'required']) !!}
									<span class="m-input-icon__icon m-input-icon__icon--left">
										<span><i class="flaticon-notes"></i></span>
									</span>
								</div>
							</div>

							<div class="form-group">
								{!! Form::label('description', 'Description') !!}
								{!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Course description', 'rows' => '8']) !!}
							</div>

							<div class="form-group">
								{!! Form::label('code', 'Code') !!}
								<textarea id="code" name="code"></textarea>
							</div>
							<div class="text-center" style="margin-top: 20px;">
						  		<a href="{{ route('adminPageEditorList') }}" class="btn btn-outline-metal">Back</a>
								{{ Form::button('Submit', ['type' => 'button', 'class' => 'btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning askBeforeSend', 'rel' => '#adminPageEditorStore'] )  }}
							</div>

						{!! Form::close() !!}
					</div>
				</div>
				<!--end::Section-->
			</div>
		</div>
	</div>
	<!--End::Section-->
</div>


@endsection

@section('javascript')
<script src="{{ URL::asset('assets/plugins/codemirror-5.42.2/lib/codemirror.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/codemirror-5.42.2/lib/util/codemirror.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/codemirror-5.42.2/addon/search/searchcursor.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/codemirror-5.42.2/addon/search/search.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/codemirror-5.42.2/addon/dialog/dialog.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/codemirror-5.42.2/addon/edit/matchbrackets.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/codemirror-5.42.2/addon/edit/closebrackets.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/codemirror-5.42.2/addon/comment/comment.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/codemirror-5.42.2/addon/wrap/hardwrap.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/codemirror-5.42.2/addon/fold/foldcode.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/codemirror-5.42.2/addon/fold/brace-fold.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/codemirror-5.42.2/mode/javascript/javascript.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/codemirror-5.42.2/mode/htmlmixed/htmlmixed.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/codemirror-5.42.2/mode/xml/xml.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/codemirror-5.42.2/mode/css/css.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/codemirror-5.42.2/mode/markdown/markdown.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/codemirror-5.42.2/keymap/sublime.js') }}"></script>
<script type="text/javascript">
	var value = "// The bindings defined specifically in the Sublime Text mode\nvar bindings = {\n";
	var map = CodeMirror.keyMap.sublime;
	for (var key in map) {
		var val = map[key];
		if (key != "fallthrough" && val != "..." && (!/find/.test(val) || /findUnder/.test(val)))
			value += "  \"" + key + "\": \"" + val + "\",\n";
	}
	value += "}\n\n// The implementation of joinLines\n";
	value += CodeMirror.commands.joinLines.toString().replace(/^function\s*\(/, "function joinLines(").replace(/\n  /g, "\n") + "\n";

	var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
		value: value,
		lineNumbers: true,
		smartIndent: true,
		mode: "text/html",
		keyMap: "sublime",
		autoCloseBrackets: true,
		matchBrackets: true,
		showCursorWhenSelecting: true,
		theme: "monokai",
		tabSize: 2
	});

	editor.autoFormatRange();
</script>
@endsection









