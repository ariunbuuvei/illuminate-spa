@extends('layouts.admin')


@section('stylesheet')
@endsection


@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!--Begin::Section-->
	<div class="m-content">
		<div class="row">

			<div class="offset-md-2 col-md-8">
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									Create page
								</h3>
							</div>
						</div>
					</div>
					<div class="m-portlet__body">
						<!--begin::Section-->
						<div class="m-section">
							<div class="m-section__content">
								{!! Form::open(['method' => 'POST', 'url' => route('adminQuizStore') ]) !!}
					  				<div class="row">

					  					<div class="col-md-12">
					  						<div class="form-group">
					  							{!! Form::label('course', 'Parent course') !!}
					  							<span class="m--font-danger">*</span>
					  							{!! Form::select('course', $courseList, null, [
					  								'class' => 'form-control', 
					  								'placeholder' => '-Select status-',
					  								'required' => 'required'
					  							]) !!}
					  						</div>
					  					</div>

					  					<div class="col-md-12">
					  						<div class="form-group">
					  							{!! Form::label('question', 'Question') !!}
					  							{!! Form::textarea('question', null, ['class' => 'form-control', 'placeholder' => 'Question', 'rows' => '3']) !!}
					  						</div>
					  					</div>

					  					<div class="col-md-12">
					  						<div class="form-group">
					  							{!! Form::label('a', 'First answer') !!}
					  							<div class="m-input-icon m-input-icon--left">
						  							{!! Form::text('a', null, ['class' => 'form-control m-input', 'placeholder' => 'Answer A']) !!}
						  							<span class="m-input-icon__icon m-input-icon__icon--left">
						  								<span><i class="flaticon-notes"></i></span>
						  							</span>
					  							</div>
					  						</div>
					  					</div>

					  					<div class="col-md-12">
					  						<div class="form-group">
					  							{!! Form::label('b', 'Second answer') !!}
					  							<div class="m-input-icon m-input-icon--left">
						  							{!! Form::text('b', null, ['class' => 'form-control m-input', 'placeholder' => 'Answer B']) !!}
						  							<span class="m-input-icon__icon m-input-icon__icon--left">
						  								<span><i class="flaticon-notes"></i></span>
						  							</span>
					  							</div>
					  						</div>
					  					</div>

					  					<div class="col-md-12">
					  						<div class="form-group">
					  							{!! Form::label('c', 'Third answer') !!}
					  							<div class="m-input-icon m-input-icon--left">
						  							{!! Form::text('c', null, ['class' => 'form-control m-input', 'placeholder' => 'Answer C']) !!}
						  							<span class="m-input-icon__icon m-input-icon__icon--left">
						  								<span><i class="flaticon-notes"></i></span>
						  							</span>
					  							</div>
					  						</div>
					  					</div>

					  					<div class="col-md-12">
					  						<div class="form-group">
					  							{!! Form::label('d', 'Fourth answer') !!}
					  							<div class="m-input-icon m-input-icon--left">
						  							{!! Form::text('d', null, ['class' => 'form-control m-input', 'placeholder' => 'Answer D']) !!}
						  							<span class="m-input-icon__icon m-input-icon__icon--left">
						  								<span><i class="flaticon-notes"></i></span>
						  							</span>
					  							</div>
					  						</div>
					  					</div>

					  					<div class="col-md-12">
					  						<div class="form-group">
					  							{!! Form::label('correct', 'Correct answer') !!}
					  							<span class="m--font-danger">*</span>
					  							{!! Form::select('correct', ['a' => 'A answer', 'b' => 'B answer', 'c' => 'C answer', 'd' => 'D answer'], null, [
					  								'class' => 'form-control', 
					  								'placeholder' => '-Select status-',
					  								'required' => 'required'
					  							]) !!}
					  						</div>
					  					</div>

					  					<div class="col-sm-12 text-center mt-3">
					  						<button type="button" class="btn btn-outline-metal">Back</button>
							  				{{ Form::button('Submit', ['type' => 'submit', 'class' => 'btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning'] )  }}
							  			</div>
					  				</div>
				  				{!! Form::close() !!}

							</div>
						</div>
						<!--end::Section-->
					</div>
				</div>
    		</div>

    	</div>
	</div>
	<!--End::Section-->
</div>
@endsection

@section('javascript')
@endsection









