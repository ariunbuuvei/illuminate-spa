@extends('layouts.admin')


@section('stylesheet')
@endsection


@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!--Begin::Section-->
	<div class="m-content">
		<div class="row">

			<div class="col-xl-12">
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									Category list
								</h3>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							<a href="{{ route('adminCategoryCreate') }}" class="btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning"><i class="flaticon-add"></i> Add new</a>
						</div>
					</div>
					<div class="m-portlet__body">
						<!--begin::Section-->
						<div class="m-section">
							<div class="m-section__content">
								<div id="categoryList"></div>
							</div>
						</div>
						<!--end::Section-->
					</div>
				</div>
    		</div>

    	</div>
	</div>
	<!--End::Section-->
</div>
@endsection

@section('javascript')
	<script type="text/javascript">
		var getCategoryTableUrl = '{{ route('adminCategoryTable') }}';
		var getCategoryEditUrl = '{{ route('adminCategoryEdit') }}';
	</script>
	<script src="{{ URL::asset('assets/js/pages/category.js') }}" type="text/javascript"></script>
@endsection









