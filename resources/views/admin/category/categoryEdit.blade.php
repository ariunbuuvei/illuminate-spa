@extends('layouts.admin')


@section('stylesheet')
<link href="{{ URL::asset('assets/plugins/bootstrap-colorpicker-3.0.3/bootstrap-colorpicker.css') }}" rel="stylesheet" type="text/css">
@endsection


@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!--Begin::Section-->
	<div class="m-content">
		<div class="row">

			<div class="offset-md-2 col-md-8">
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									Create category
								</h3>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							{!! Form::open(['method' => 'POST', 'id' => 'adminCategoryDelete', 'url' => route('adminCategoryDelete', ['category' => $categoryObject->id]) ]) !!}
								{{ Form::button('Delete', [
									'type' => 'button', 
									'class' => 'btn btn-danger askBeforeSend',
									'rel' => '#adminCategoryDelete'
								] )  }}
							{!! Form::close() !!}
						</div>
					</div>
					<div class="m-portlet__body">
						<!--begin::Section-->
						<div class="m-section">
							<div class="m-section__content">
								{!! Form::open(['method' => 'POST', 'url' => route('adminCategoryUpdate', ['category' => $categoryObject->id]), 'id' => 'adminCategoryUpdate', ]) !!}
					  				<div class="row">

					  					<div class="col-md-12">
					  						<div class="form-group">
					  							{!! Form::label('target', 'Target') !!}
				  							<span class="m--font-danger">*</span>
					  							{!! Form::select('target', $targetList, $categoryObject->target, [
					  								'class' => 'form-control', 
					  								'placeholder' => '-Select target-',
					  								'required' => 'required'
					  							]) !!}
					  						</div>
					  					</div>

					  					<div class="col-md-12">
					  						<div class="form-group">
					  							{!! Form::label('name', 'Category name') !!}
					  							<span class="m--font-danger">*</span>
					  							<div class="m-input-icon m-input-icon--left">
						  							{!! Form::text('name', $categoryObject->name, ['class' => 'form-control m-input', 'placeholder' => 'Category name', 'required' => 'required']) !!}
						  							<span class="m-input-icon__icon m-input-icon__icon--left">
						  								<span><i class="flaticon-notes"></i></span>
						  							</span>
					  							</div>
					  						</div>
					  					</div>

					  					<div class="col-md-12">
					  						<div class="form-group">
					  							{!! Form::label('parent', 'Parent') !!}
					  							<select class="form-control select2 remote" name="parent" url="{{ route('adminCategoryAutocomplete') }}">
					  								@if ($categoryObject->parent_id)
					  									<option value="{{ $categoryObject->parent_id }}">{{ $categoryObject->parentObject->name }}</option>
					  								@else
					  									<option></option>
					  								@endif
					  							</select>
					  						</div>
					  					</div>

					  					<div class="col-md-12">
					  						<div class="form-group">
					  							{!! Form::label('color', 'Category color') !!}
					  							<div class="m-input-icon m-input-icon--left">
						  							{!! Form::text('color', $categoryObject->color, ['class' => 'form-control m-input', 'id' => 'colorpicker', 'placeholder' => 'Category color']) !!}
						  							<span class="m-input-icon__icon m-input-icon__icon--left">
						  								<span><i class="fa fa-paint-brush"></i></span>
						  							</span>
					  							</div>
					  						</div>
					  					</div>

					  					<div class="col-md-12">
					  						<div class="form-group">
					  							{!! Form::label('html_class', 'Html class') !!}
					  							<div class="m-input-icon m-input-icon--left">
						  							{!! Form::text('html_class', $categoryObject->html_class, ['class' => 'form-control m-input', 'placeholder' => 'Html class']) !!}
						  							<span class="m-input-icon__icon m-input-icon__icon--left">
						  								<span><i class="fa fa-html5"></i></span>
						  							</span>
					  							</div>
					  						</div>
					  					</div>

					  					<div class="col-md-12">
					  						<div class="form-group">
					  							{!! Form::label('css', 'Inline CSS') !!}
					  							<div class="m-input-icon m-input-icon--left">
						  							{!! Form::text('css', $categoryObject->css, ['class' => 'form-control m-input', 'placeholder' => 'Inline CSS']) !!}
						  							<span class="m-input-icon__icon m-input-icon__icon--left">
						  								<span><i class="fa fa-css3"></i></span>
						  							</span>
					  							</div>
					  						</div>
					  					</div>

					  					

					  					<div class="col-md-12 mb-3">
					  						<div class="form-group">
					  							{!! Form::label('description', 'Description') !!}
					  							{!! Form::textarea('description', $categoryObject->description, ['class' => 'form-control']) !!}
					  						</div>
					  					</div>

					  					<div class="col-sm-12 text-center">
					  						<a href="{{ route('adminCategoryList') }}" class="btn btn-outline-metal">Back</a>
							  				{{ Form::button('Submit', ['type' => 'button', 'class' => 'btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning askBeforeSend', 'rel' => '#adminCategoryUpdate'] )  }}
							  			</div>

					  				</div>
				  				{!! Form::close() !!}

							</div>
						</div>
						<!--end::Section-->
					</div>
				</div>
    		</div>

    	</div>
	</div>
	<!--End::Section-->
</div>
@endsection

@section('javascript')
<script src="{{ URL::asset('assets/plugins/bootstrap-colorpicker-3.0.3/bootstrap-colorpicker.js') }}" type="text/javascript"></script>
<script>
    $(function () {
      // Basic instantiation:
      $('#colorpicker').colorpicker();
    });
  </script>
@endsection









