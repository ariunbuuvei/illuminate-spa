@extends('layouts.admin')

@section('stylesheet')
@endsection


@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!--Begin::Section-->
	<div class="m-content">
		<div class="row">

			<div class="col-md-6">
				@foreach($generalSettings as $setting)
					<div class="m-portlet">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										{{ $setting->name }}
									</h3>
								</div>
							</div>
						</div>
						<!--begin::Form-->
						{!! Form::open(['method' => 'POST', 'id' => 'adminSettingsGeneralApply', 'class' => 'm-form', 'url' => route('adminSettingsGeneralApply') ]) !!}

							<div class="m-portlet__body">
								<input type="hidden" name="settings" value="{{ $setting->id }}">

								@foreach($setting->settingsAttrCollection as $settingsAttr)
									<div class="form-group m-form__group">
										{!! Form::label($settingsAttr->attr, $settingsAttr->placeholder) !!}
										{!! $settingsAttr->required ? '<span class="m--font-danger">*</span>' : null !!}
										{!! Form::text($settingsAttr->attr, $settingsAttr->value, ['class' => 'form-control m-input', 'placeholder' => $settingsAttr->placeholder ]) !!}
									</div>
								@endforeach
				            </div>
				            
				            <div class="m-portlet__foot m-portlet__foot--fit">
								<div class="m-form__actions m-form__actions--right">
									<div class="row">
										<div class="col m--align-left">
											{{ Form::button('Save', ['type' => 'button', 'class' => 'btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning askBeforeSend', 'rel' => '#adminSettingsGeneralApply'] )  }}
										</div>
										<div class="col m--align-right">
											<button type="reset" class="btn btn-secondary">Reset</button>
										</div>
									</div>
								</div>
							</div>
						{!! Form::close() !!}

						<!--end::Form-->
					</div>
				@endforeach
    		</div>

    		<div class="col-md-6">
    			@foreach($paymentSettings as $settings)
					<div class="m-portlet">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										{{ $settings->name }}
									</h3>
								</div>
							</div>
						</div>
						<!--begin::Form-->
						{!! Form::open(['method' => 'POST', 'id' => 'adminSettingsPaymentApply', 'class' => 'm-form', 'url' => route('adminSettingsGeneralApply') ]) !!}
						<!--  -->
							<div class="m-portlet__body">

								<input type="hidden" name="settings" value="{{ $settings->id }}">

								@foreach($settings->settingsAttrCollection as $settingsAttr)
									<div class="form-group m-form__group">
										{!! Form::label('name', 'Stripe Key') !!}
										<span class="m--font-danger">*</span>
										<div class="m-input-icon m-input-icon--left">
											{!! Form::text($settingsAttr->attr, $settingsAttr->value, ['class' => 'form-control m-input', 'placeholder' => 'Payment Bunle name', 'required' => 'required']) !!}
											<span class="m-input-icon__icon m-input-icon__icon--left">
												<span><i class="fa fa-cc-stripe"></i></span>
											</span>
										</div>
									</div>
								@endforeach

				            </div>
				            <div class="m-portlet__foot m-portlet__foot--fit">
								<div class="m-form__actions m-form__actions--right">
									<div class="row">
										<div class="col m--align-left">
											{{ Form::button('Save', ['type' => 'button', 'class' => 'btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning askBeforeSend', 'rel' => '#adminSettingsPaymentApply'] )  }}
										</div>
										<div class="col m--align-right">
											<button type="reset" class="btn btn-secondary">Reset</button>
										</div>
									</div>
								</div>
							</div>
						{!! Form::close() !!}

						<!--end::Form-->
					</div>
				@endforeach
    		</div>

    	</div>
	</div>
	<!--End::Section-->
</div>

@endsection

@section('javascript')
@endsection