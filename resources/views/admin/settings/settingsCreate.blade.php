@extends('layouts.admin')

@section('stylesheet')
<link href="{{ URL::asset('assets/plugins/bootstrap-colorpicker-3.0.3/bootstrap-colorpicker.css') }}" rel="stylesheet" type="text/css">
@endsection


@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!--Begin::Section-->
	<div class="m-content">
		<div class="row">

			<div class="offset-md-2 col-md-8">
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									Create settings
								</h3>
							</div>
						</div>
					</div>
					<div class="m-portlet__body">
						<!--begin::Section-->
						<div class="m-section">
							<div class="m-section__content">
								{!! Form::open(['method' => 'POST', 'url' => route('adminSettingsStore'), 'id' => 'adminSettingsStore', ]) !!}
					  				<div class="row">

					  					<div class="col-md-12">
					  						<div class="form-group">
					  							{!! Form::label('target', 'Target') !!}
				  							<span class="m--font-danger">*</span>
					  							{!! Form::select('target', $targetList, null, [
					  								'class' => 'form-control', 
					  								'placeholder' => '-Select target-',
					  								'required' => 'required'
					  							]) !!}
					  						</div>
					  					</div>
					  					
					  					<input type="hidden" name="type" value="form">

					  					<div class="col-md-12">
					  						<div class="form-group">
					  							{!! Form::label('name', 'Settings name') !!}
					  							<span class="m--font-danger">*</span>
					  							<div class="m-input-icon m-input-icon--left">
						  							{!! Form::text('name', null, ['class' => 'form-control m-input', 'placeholder' => 'Settings name', 'required' => 'required']) !!}
						  							<span class="m-input-icon__icon m-input-icon__icon--left">
						  								<span><i class="flaticon-interface-9"></i></span>
						  							</span>
					  							</div>
					  						</div>
					  					</div>

					  					<div class="col-md-12 mb-3">
					  						<div class="form-group">
					  							{!! Form::label('description', 'Description') !!}
					  							{!! Form::textarea('description', null, ['class' => 'form-control']) !!}
					  						</div>
					  					</div>

					  					<div class="col-sm-12 text-center">
					  						<a href="{{ route('adminStatusList') }}" class="btn btn-outline-metal">Back</a>
							  				{{ Form::button('Submit', ['type' => 'button', 'class' => 'btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning askBeforeSend', 'rel' => '#adminSettingsStore'] )  }}
							  			</div>

					  				</div>
				  				{!! Form::close() !!}

							</div>
						</div>
						<!--end::Section-->
					</div>
				</div>
    		</div>

    	</div>
	</div>
	<!--End::Section-->
</div>

@endsection

@section('javascript')
<script>

</script>
@endsection