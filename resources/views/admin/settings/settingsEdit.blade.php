@extends('layouts.admin')

@section('stylesheet')
@endsection


@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!--Begin::Section-->
	<div class="m-content">
		<div class="row">

			<div class="col-md-5">
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									{{ $settingsObject->name }}
								</h3>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							{!! Form::open(['method' => 'POST', 'id' => 'adminSettingsDelete', 'url' => route('adminSettingsDelete', ['settings' => $settingsObject->id]) ]) !!}
								{{ Form::button('Delete', [
									'type' => 'button', 
									'class' => 'btn btn-danger askBeforeSend',
									'rel' => '#adminSettingsDelete'
								] )  }}
							{!! Form::close() !!}
						</div>
					</div>

					<div class="m-portlet__body">
						<!--begin::Section-->
						<div class="m-section">
							<div class="m-section__content">
								{!! Form::open(['method' => 'POST', 'url' => route('adminSettingsUpdate', ['settings' => $settingsObject->id]), 'id' => 'adminSettingsUpdate', ]) !!}
					  				<div class="row">

					  					<div class="col-md-12">
					  						<div class="form-group">
					  							{!! Form::label('target', 'Target') !!}
				  							<span class="m--font-danger">*</span>
					  							{!! Form::select('target', $targetList, $settingsObject->type, [
					  								'class' => 'form-control', 
					  								'placeholder' => '-Select target-',
					  								'required' => 'required'
					  							]) !!}
					  						</div>
					  					</div>

					  					<div class="col-md-12">
					  						<div class="form-group">
					  							{!! Form::label('name', 'Settings name') !!}
					  							<span class="m--font-danger">*</span>
					  							<div class="m-input-icon m-input-icon--left">
						  							{!! Form::text('name', $settingsObject->name, ['class' => 'form-control m-input', 'placeholder' => 'Settings name', 'required' => 'required']) !!}
						  							<span class="m-input-icon__icon m-input-icon__icon--left">
						  								<span><i class="flaticon-interface-9"></i></span>
						  							</span>
					  							</div>
					  						</div>
					  					</div>

					  					<div class="col-md-12 mb-3">
					  						<div class="form-group">
					  							{!! Form::label('description', 'Description') !!}
					  							{!! Form::textarea('description', $settingsObject->description, ['class' => 'form-control']) !!}
					  						</div>
					  					</div>

					  					<div class="col-sm-12 text-center">
					  						<a href="{{ route('adminSettingsList') }}" class="btn btn-outline-metal">Back</a>
							  				{{ Form::button('Submit', ['type' => 'button', 'class' => 'btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning askBeforeSend', 'rel' => '#adminSettingsUpdate'] )  }}
							  			</div>

					  				</div>
				  				{!! Form::close() !!}

							</div>
						</div>
						<!--end::Section-->
					</div>
				</div>
    		</div>

    		<div class="col-md-7">
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									Child fields
								</h3>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							<button class="btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning" type="button" data-toggle="modal" data-target="#createSettingsAttr">
								<i class="flaticon-add"></i> Add option
							</button>
						</div>
					</div>
					<div class="m-portlet__body">
						<!--begin::Section-->
						<div class="m-section">
							<div class="m-section__content">
								
								<table class="table m-table m-table--head-bg-brand">
									<thead>
										<tr>
											<th>Name</th>
											<th>Format</th>
											<th>Slug</th>
											<th>Required</th>
											<th>Status</th>
											<th>Created</th>
										</tr>
									</thead>
									<tbody>
										@foreach($settingsObject->settingsAttrCollection as $settingsAttr)
											<tr>
												<td><a href="{{ route('adminSettingsAttrEdit', ['settingsAttr' => $settingsAttr->id]) }}">{{ $settingsAttr->placeholder }}</a></td>
												<td>{{ $settingsAttr->typeName->name }}</td>
												<td>{{ $settingsAttr->attr }}</td>
												<td>
													@isset($settingsAttr->required)
														<i class="fa fa-check"></i>
													@endisset
												</td>
												<td><span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Active</span></td>
												<td>{{ $settingsAttr->created_at->format('d M, Y') }}</td>
											</tr>
										@endforeach
									</tbody>
								</table>

							</div>
						</div>
						<!--end::Section-->
					</div>
				</div>
    		</div>

    	</div>
	</div>
	<!--End::Section-->
</div>


<div class="modal" tabindex="-1" id="createSettingsAttr" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">

			<div class="modal-header">
				<h5 class="modal-title">Add option</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<form method="post" action="{{ route('adminSettingsAttrStore', ['settings' => $settingsObject->id]) }}">
				<!-- CRFT TOKEN GEN -->
				@csrf

				<div class="modal-body">

					<div class="form-group col-md-12">
						<label for="attr">Name</label>
						<span class="m--font-danger">*</span>
						<input type="text" name="placeholder" class="form-control" placeholder="Name" required>
					</div>

					<div class="form-group col-md-12">
						<label for="value">Format</label>
						<span class="m--font-danger">*</span>
						{!! Form::select('type', $settingsAttrType, null, [
					  		'class' => 'form-control', 
					  		'placeholder' => '-Select type-',
					  		'required' => 'required'
					  	]) !!}
					</div>

					<div class="form-group col-md-12">
						<label for="placeholder">Slug</label>
						<span class="m--font-danger">*</span>
						<input type="text" name="attr" class="form-control" placeholder="slug" required>
					</div>

					<div class="form-group col-md-12">
						<input type="checkbox" id="required" name="required" value="1">
				        <label for="required">Required</label>
				    </div>
				    
					<div class="form-group col-md-12">
						<label for="description">Description</label>
						<textarea name="description" class="form-control"></textarea>
					</div>

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Confirm</button>
				</div>
			</form>

		</div>
	</div>
</div>

@endsection

@section('javascript')
@endsection