@extends('layouts.admin')


@section('stylesheet')
@endsection


@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!--Begin::Section-->
	<div class="m-content">
		<div class="row">

			<!-- 'Text' => 1,
            'Select' => 2,
            'Textarea' => 3,
            'Date' => 4,
            'Email' => 5,
            'Number' => 6, -->

			@switch( $settingsAttrObject->type )
			    @case(2)
			        <div class="col-md-5">
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											{{ $settingsAttrObject->placeholder }}
										</h3>
									</div>
								</div>
								<div class="m-portlet__head-tools">
									{!! Form::open(['method' => 'POST', 'id' => 'adminSettingsAttrDelete', 'url' => route('adminSettingsAttrDelete', ['settingsAttr' => $settingsAttrObject->id]) ]) !!}
										{{ Form::button('Delete', [
											'type' => 'button', 
											'class' => 'btn btn-danger askBeforeSend',
											'rel' => '#adminSettingsAttrDelete'
										] )  }}
									{!! Form::close() !!}
								</div>
							</div>
							<div class="m-portlet__body">
								<!--begin::Section-->
								<div class="m-section">
									<div class="m-section__content">
										
										<form method="post" action="{{ route('adminSettingsAttrUpdate', ['settingsAttr' => $settingsAttrObject->id]) }}" id="adminSettingsAttrUpdate">
											<!-- CRFT TOKEN GEN -->
											@csrf

												<div class="form-group col-md-12">
													<label for="attr">Name</label>
													<span class="m--font-danger">*</span>
													<input type="text" name="placeholder" class="form-control" placeholder="Name" value="{{ $settingsAttrObject->placeholder }}" required>
												</div>

												<div class="form-group col-md-12">
													<label for="value">Format</label>
													<span class="m--font-danger">*</span>
													{!! Form::select('type', $settingsAttrType, $settingsAttrObject->type, [
												  		'class' => 'form-control', 
												  		'placeholder' => '-Select type-',
												  		'required' => 'required'
												  	]) !!}
												</div>

												<div class="form-group col-md-12">
													<label for="placeholder">Slug</label>
													<span class="m--font-danger">*</span>
													<input type="text" name="attr" class="form-control" placeholder="Placeholder" value="{{ $settingsAttrObject->attr }}" required>
												</div>

												<div class="form-group col-md-12">
													<input type="checkbox" id="required" name="required" value="1">
											        <label for="required">Required</label>
											    </div>
											    
												<div class="form-group col-md-12">
													<label for="description">Description</label>
													<textarea name="description" class="form-control">{{ $settingsAttrObject->description }}</textarea>
												</div>

											<div class="col-sm-12 text-center">
						  						<a href="{{ route('adminSettingsEdit', ['settingsAttr' => $settingsAttrObject->settings_id]) }}" class="btn btn-outline-metal">Back</a>
								  				{{ Form::button('Submit', ['type' => 'button', 'class' => 'btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning askBeforeSend', 'rel' => '#adminSettingsAttrUpdate'] )  }}
								  			</div>
										</form>

									</div>
								</div>
								<!--end::Section-->
							</div>
						</div>
		    		</div>

		    		<div class="col-md-7">
		    			<div class="col-xl-12">
							<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text">
												Select options
											</h3>
										</div>
									</div>
									<div class="m-portlet__head-tools">
										<button class="btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning" type="button" data-toggle="modal" data-target="#createSettingsAttrOption">
											<i class="flaticon-add"></i> Add option
										</button>
									</div>
								</div>
								<div class="m-portlet__body">
									<!--begin::Section-->
									<div class="m-section">
										<div class="m-section__content">
											<!-- adminSettingsEdit -->
											<table class="table m-table m-table--head-bg-brand">
												<thead>
													<tr>
														<th>Name</th>
														<th>Status</th>
														<th>Created</th>
													</tr>
												</thead>
												<tbody>
													@foreach ($settingsAttrObject->settingsAttrOptionCollection as $settingsAttrOption)
														<tr>
															<td>{{ $settingsAttrOption->text }}</td>
															<td><span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Active</span></td>
															<td>{{ $settingsAttrOption->created_at->format('d M, Y') }}</td>
														</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
									<!--end::Section-->
								</div>
							</div>
			    		</div>
		    		</div>

		    		<!-- Modal -->
					<div class="modal" tabindex="-1" id="createSettingsAttrOption" role="dialog">
						<div class="modal-dialog" role="document">
							<div class="modal-content">

								<div class="modal-header">
									<h5 class="modal-title">Add option</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>

								<form method="post" action="{{ route('adminSettingsAttrOptionStore', ['settingsAttr' => $settingsAttrObject->id]) }}">
									<!-- CRFT TOKEN GEN -->
									@csrf

									<div class="modal-body">

										<div class="form-group col-md-12">
											<label for="text">Value</label>
											<input type="text" name="text" class="form-control" placeholder="Value" required>
										</div>
									    
										<div class="form-group col-md-12">
											<label for="description">Description</label>
											<textarea name="description" class="form-control"></textarea>
										</div>

									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										<button type="submit" class="btn btn-primary">Confirm</button>
									</div>
								</form>

							</div>
						</div>
					</div>
			        @break

			    @default
			        <div class="offset-md-2 col-md-8">
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											{{ $settingsAttrObject->placeholder }}
										</h3>
									</div>
								</div>
								<div class="m-portlet__head-tools">
									{!! Form::open(['method' => 'POST', 'id' => 'adminSettingsAttrDelete', 'url' => route('adminSettingsAttrDelete', ['settingsAttr' => $settingsAttrObject->id]) ]) !!}
										{{ Form::button('Delete', [
											'type' => 'button', 
											'class' => 'btn btn-danger askBeforeSend',
											'rel' => '#adminSettingsAttrDelete'
										] )  }}
									{!! Form::close() !!}
								</div>
							</div>
							<div class="m-portlet__body">
								<!--begin::Section-->
								<div class="m-section">
									<div class="m-section__content">
										
										<form method="post" action="{{ route('adminSettingsAttrUpdate', ['settingsAttr' => $settingsAttrObject->id]) }}" id="adminSettingsAttrUpdate">
											<!-- CRFT TOKEN GEN -->
											@csrf

												<div class="form-group col-md-12">
													<label for="attr">Name</label>
													<span class="m--font-danger">*</span>
													<input type="text" name="placeholder" class="form-control" placeholder="Name" value="{{ $settingsAttrObject->placeholder }}" required>
												</div>

												<div class="form-group col-md-12">
													<label for="value">Format</label>
													<span class="m--font-danger">*</span>
													{!! Form::select('type', $settingsAttrType, $settingsAttrObject->type, [
												  		'class' => 'form-control', 
												  		'placeholder' => '-Select type-',
												  		'required' => 'required'
												  	]) !!}
												</div>

												<div class="form-group col-md-12">
													<label for="placeholder">Slug</label>
													<span class="m--font-danger">*</span>
													<input type="text" name="attr" class="form-control" placeholder="Placeholder" value="{{ $settingsAttrObject->attr }}" required>
												</div>

												<div class="form-group col-md-12">
													<input type="checkbox" id="required" name="required" value="1" {{ $settingsAttrObject->required ? "checked" : "" }}>
											        <label for="required">Required</label>
											    </div>
											    
												<div class="form-group col-md-12">
													<label for="description">Description</label>
													<textarea name="description" class="form-control">{{ $settingsAttrObject->description }}</textarea>
												</div>

											<div class="col-sm-12 text-center">
						  						<a href="{{ route('adminSettingsEdit', ['settingsAttr' => $settingsAttrObject->settings_id]) }}" class="btn btn-outline-metal">Back</a>
								  				{{ Form::button('Submit', ['type' => 'button', 'class' => 'btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning askBeforeSend', 'rel' => '#adminSettingsAttrUpdate'] )  }}
								  			</div>
										</form>

									</div>
								</div>
								<!--end::Section-->
							</div>
						</div>
		    		</div>
			@endswitch


			

    	</div>
	</div>
	<!--End::Section-->
</div>
@endsection

@section('javascript')
@endsection









