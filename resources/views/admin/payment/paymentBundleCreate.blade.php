@extends('layouts.admin')


@section('stylesheet')
@endsection


@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!--Begin::Section-->
	<div class="m-content">
		<div class="row">

			<div class="col-md-12">
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									Create Payment Bunle
								</h3>
							</div>
						</div>
					</div>
					<div class="m-portlet__body">
						<!--begin::Section-->
						<div class="m-section">
							<div class="m-section__content">
								
								<div class="row">
									<div class="col-md-6">
										{!! Form::open(['method' => 'POST', 'url' => route('adminPaymentBundleStore') ]) !!}
										<div class="row">

											<input type="hidden" name="token" value="{{ $post_token }}">
											
											<div class="col-md-12">
												<div class="form-group">
													{!! Form::label('name', 'Name') !!}
													<span class="m--font-danger">*</span>
													<div class="m-input-icon m-input-icon--left">
														{!! Form::text('name', null, ['class' => 'form-control m-input', 'placeholder' => 'Payment Bunle name', 'required' => 'required']) !!}
														<span class="m-input-icon__icon m-input-icon__icon--left">
															<span><i class="fa fa-cc-stripe"></i></span>
														</span>
													</div>
												</div>
											</div>

											<div class="col-md-12">
												<div class="form-group">
													{!! Form::label('amount', 'Amounts') !!}
													<span class="m--font-danger">*</span>
													<div class="m-input-icon m-input-icon--left">
														{!! Form::number('amount', null, ['class' => 'form-control m-input', 'placeholder' => 'Amounts', 'required' => 'required', 'step' => '.01']) !!}
														<span class="m-input-icon__icon m-input-icon__icon--left">
															<span><i class="flaticon-coins"></i></span>
														</span>
													</div>
												</div>
											</div>

											<div class="col-md-12">
												<div class="form-group">
													{!! Form::label('currency', 'Currency') !!}
													<span class="m--font-danger">*</span>
													{!! Form::select('currency', ['USD' => 'United States Dollar', 'AUD' => 'Australian Dollar'], null, [
													'class' => 'form-control', 
													'placeholder' => '-Select currency-',
													'required' => 'required'
													]) !!}
												</div>
											</div>

											<div class="col-md-12">
												<div class="form-group">
													{!! Form::label('interval', 'Payment Frequency') !!}
													<span class="m--font-danger">*</span>
													{!! Form::select('interval', ['week' => 'Weekly', 'month' => 'Monthly', 'year' => 'Annually'], null, [
													'class' => 'form-control', 
													'placeholder' => '-Select payment frequency-',
													'required' => 'required'
													]) !!}
												</div>
											</div>

											<div class="col-md-12">
												<div class="form-group">
													{!! Form::label('description', 'Description') !!}
													<span class="m--font-danger">*</span>
													{!! Form::textarea('description', null, ['class' => 'summernote']) !!}
												</div>
											</div>

											<div class="col-sm-12 text-center mt-3">
												<button type="button" class="btn btn-outline-metal">Back</button>
												{{ Form::button('Submit', ['type' => 'submit', 'class' => 'btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning'] )  }}
											</div>
										</div>
										{!! Form::close() !!}
									</div>

									<div class="col-md-6">

										<table class="table m-table m-table--head-bg-brand" id="paymentRules" style="display: none;">
											<thead>
												<tr>
													<th>Target</th>
													<th>Count</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												
											</tbody>
										</table>

										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													{!! Form::label('count', 'Count') !!}
													{!! Form::number('count', null, ['class' => 'form-control m-input', 'placeholder' => 'X times per subscription', 'id' => 'rule_count']) !!}
												</div>
											</div>

											<div class="col-md-12">
												<div class="form-group">
													{!! Form::label('target', 'Target') !!}
													{!! Form::select('target', $targetList, null, [
													'class' => 'form-control', 
													'placeholder' => '-Select target-',
													'id' => 'rule_target'
													]) !!}
												</div>
											</div>

											<input type="hidden" id="payment_id" value="">

											<div class="col-md-12 text-center">
												<button class="btn btn-primary" id="rule_add">Add rule</button>
											</div>
										</div>

									</div>
								</div>

							</div>
						</div>
						<!--end::Section-->
					</div>
				</div>
    		</div>

    	</div>
	</div>
	<!--End::Section-->
</div>
@endsection

@section('javascript')
<script type="text/javascript">
	var getRuleAddUrl = "{{ route("adminPaymentBundleRuleStore", ['token' => $post_token]) }}";
	var getRuleDeleteUrl = "{{ route("adminPaymentBundleRuleDelete") }}";
</script>
<script src="{{ URL::asset('assets/js/pages/payment.js') }}" type="text/javascript"></script>
@endsection









